package com.SwipeData.authentication;

import java.util.Arrays;

import android.util.Log;
/* @author RAJESH KUMAR */
public class FeatureManager {
    
    private static final String	FMGR	= null;
		private double[] TimeSeries;
    private final int NumOfFeaturesPerSeries=14;
    
    public FeatureManager( double[] TSeries) {
                this.TimeSeries = TSeries; 
        }
    public double[] getTimeSeries()
    {
        double[] TimeSeriesCopy=TimeSeries.clone();
        return TimeSeriesCopy;
    }
     public int getNumberOfFeaturesPerSeries()
    {
      return NumOfFeaturesPerSeries;
    }
   
    public double min() {
    
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        return TimeSeriesCopy[0];
    }   
   
    public double max() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        return TimeSeriesCopy[TimeSeriesCopy.length-1];
    }  
    public double mean() {
        double SUM=0.0;
        for (int i =0; i<TimeSeries.length; i++)
        {
          SUM=SUM+TimeSeries[i];
        }
        return SUM/TimeSeries.length;
    }  
    public double std() {
    
        double MeanOfTimeSeries=mean();
        double SUM=0.0;
        for (int i =0; i<TimeSeries.length; i++)
        {
          SUM=SUM+(TimeSeries[i]-MeanOfTimeSeries)*(TimeSeries[i]-MeanOfTimeSeries);
        }
        double StdTimeSeries= Math.sqrt(SUM/(TimeSeries.length-1));
        return StdTimeSeries;
    }  
    
     public double fQuantile() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        double fQrt=TimeSeriesCopy[TimeSeriesCopy.length/4];
        return fQrt;
    }  
    public double sQuantile() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        double SQrt=TimeSeriesCopy[TimeSeriesCopy.length/2];
        return SQrt;
    }  
    public double thQuantile() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        double ThQrt=TimeSeriesCopy[TimeSeriesCopy.length/4*3];
        return ThQrt;
    }  
    public double energy() {
        double EnergyTimeSeries=0.0;
        double AbsValue;
        for (int i =0; i<TimeSeries.length; i++)
        {
          AbsValue=Math.abs(TimeSeries[i]);
          EnergyTimeSeries=EnergyTimeSeries+(AbsValue*AbsValue);
        }
        return EnergyTimeSeries;
    }  
    public double power() {
    
        double EnergyOfTimeSeries=energy();
        return EnergyOfTimeSeries/TimeSeries.length;
    }  
      
    public int noZeroCrosings() {
    
        int NOZC=0;
        for (int i =0; i<TimeSeries.length-1; i++)
        {   
            if((TimeSeries[i]*TimeSeries[i+1])<0)
            NOZC = NOZC+1;
        }
        return NOZC;
    }  
    public double [] findpeaks() { //Local Peakedness current>prev && current>next
        
        double PeakLocations[];
        int NOPeaks=0;
        for (int i =1; i<TimeSeries.length-1; i++)
        {   
          double currentElement=TimeSeries[i];
            if ((currentElement>TimeSeries[i-1])&&(currentElement>TimeSeries[i+1])){
                NOPeaks=NOPeaks+1; 
        
            }
        }
        
        PeakLocations= new double[NOPeaks];
        int index=0;
        for (int i =1; i<TimeSeries.length-1; i++)
        {   
          double currentElement=TimeSeries[i];
            if ((currentElement>TimeSeries[i-1])&&(currentElement>TimeSeries[i+1])){
                NOPeaks=NOPeaks+1; 
                PeakLocations[index++]=i;
            }
        }
        return PeakLocations;
               
    }
    public int nopeaks(){
    	double PeakLocations[]=findpeaks();
        return PeakLocations.length;
    }
  
    public double api() {
    	  double PeakLocations[]=findpeaks();
        double AveragePeakInterval, Diff=0.0;
        for(int i=1;i<PeakLocations.length;i++)
        {
          Diff=Diff+(PeakLocations[i]-PeakLocations[i-1]);
        }
        AveragePeakInterval=Diff/(PeakLocations.length-1); 
        return AveragePeakInterval;
    }
//We use the following (unbiased) formula to define skewness:
// skewness = [n / (n -1) (n - 2)] sum[(x_i - mean)^3] / std^3 
     public double skewness(){
      double N;
        N = TimeSeries.length;
      double SkewnessOfSeries;
      double Mean=mean();
      double Std=std();
      double SUM=0.0;
      for (int i =0; i<TimeSeries.length; i++)
      {
       SUM=SUM+Math.pow((TimeSeries[i]-Mean),3);
      }
      SkewnessOfSeries= (N/((N-1)*(N-2)))*SUM/(Math.pow(Std,3));
      return SkewnessOfSeries;
      
    }
  //   kurtosis = { [n(n+1) / (n -1)(n - 2)(n-3)] sum[(x_i - mean)^4] / std^4 } - [3(n-1)^2 / (n-2)(n-3)]
    
   public double kurtosis(){
      double N;
      N = TimeSeries.length;
      double KurtosisOfSeries;
      double MeanOfSeries=mean();
      double StdOfSeries=std();
      double SUM=0.0,Part1, Part2, Part3;
      for (int i =0; i<TimeSeries.length; i++)
      {
        SUM=SUM+Math.pow((TimeSeries[i]-MeanOfSeries),4);
      }
      Part1=(N*(N+1))/((N-1)*(N-2)*(N-3));
    
      Part2=SUM/Math.pow(StdOfSeries, 4);
    
      Part3=((Math.pow((N-1),2))*3)/((N-2)*(N-3));
 
      KurtosisOfSeries= Part1*Part2-Part3;
      
      return KurtosisOfSeries;
      
    }
  
   //ADD THIS FUNCTION
       
   public void smoothenSeries3Points()
   {
     for (int itr=1; itr< TimeSeries.length-1;itr++)
     {
        TimeSeries[itr]=(TimeSeries[itr-1]+TimeSeries[itr]+TimeSeries[itr+1])/3;
     }
   }
      //ADD THIS FUNCTION
   public void smoothenSeries5Points()
   {
     for (int itr=2; itr< TimeSeries.length-2;itr++)
     {
        TimeSeries[itr]=(TimeSeries[itr-2]+TimeSeries[itr-1]+TimeSeries[itr]+TimeSeries[itr+1]+TimeSeries[itr+2])/5;
     }
   }
      //ADD THIS FUNCTION
   public void smoothenSeries7Points()
   {
     for (int itr=3; itr< TimeSeries.length-3;itr++)
     {
        TimeSeries[itr]=(TimeSeries[itr-3]+TimeSeries[itr-2]+TimeSeries[itr-1]+TimeSeries[itr]+TimeSeries[itr+1]+TimeSeries[itr+2]+TimeSeries[itr+3])/5;
     }
   }
      //ADD THIS FUNCTION
   public void removeOutliersFromBeginAndEnd()
   {
  	 Log.i(FMGR, "Before Removal - Length of Array -" +TimeSeries.length);
       int Length=TimeSeries.length;
       int begin=Length/5;
       int end=(Length*4)/5;
       System.out.println("Removing "+begin+" Lines from the Begining and "+(Length-end)+" from the end");
       TimeSeries=Arrays.copyOfRange(TimeSeries, begin, Length);
       Log.i(FMGR, "After Removal - Length of Array -" +TimeSeries.length);
   }
   
   public void removeOnlyBeginingOutliers()
   {
  	   Log.i(FMGR, "Before Removal - Length of Array -" +TimeSeries.length);
       int Length=TimeSeries.length;
       int begin=Length/5;
       System.out.println("Removing "+begin+" Lines from the Begining ");
       TimeSeries=Arrays.copyOfRange(TimeSeries, begin, Length);
       Log.i(FMGR, "After Removal - Length of Array -" +TimeSeries.length);
   }
   
      //ADD THIS FUNCTION
   public int getLength()
   {
     return (TimeSeries.length);
   }
   
   public double[] getAllTheFeatures()
    {
     double []Features = new double[NumOfFeaturesPerSeries];
     int FCounter=0;
     Features[FCounter++]=api();
     Features[FCounter++]=energy();
     Features[FCounter++]=fQuantile();
     Features[FCounter++]=kurtosis();
     Features[FCounter++]=max();
     Features[FCounter++]=mean();
     Features[FCounter++]=min();
     Features[FCounter++]=nopeaks();
     Features[FCounter++]=noZeroCrosings();
     Features[FCounter++]=power();
     Features[FCounter++]=skewness();
     Features[FCounter++]=sQuantile();
     Features[FCounter++]=std();
     Features[FCounter]=thQuantile();
     return Features;
    }
   
  
}
