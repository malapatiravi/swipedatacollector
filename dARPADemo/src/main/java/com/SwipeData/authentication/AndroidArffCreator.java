package com.SwipeData.authentication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import com.SwipeData.generalobjects.AndroidFileManager;

import android.os.Environment;
import android.util.Log;

public class AndroidArffCreator extends AndroidFileManager
{
  //private String FileLocation = "";
	//private String Filename = "";
	private boolean RelationSet;
	private boolean AttributeSet;
	private int TotalAttribs;
	private String[] AttribsTypeList;
	private String TAG = "ARRF";
	File ImposterFile;
	private final String RelationTag 		= "@relation ";
	private final String AttributeTag 	= "@attribute ";
	private final String DataTag 				= "@data";
	private final String[] ValidDataTypes = {"numeric", "string"};
	AndroidArffCreator()
	{
		super(null);
		try
		{
			throw new Exception("File location and name must be provided");
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public AndroidArffCreator(AndroidFileManager  AFMObject)
	{
		super(AFMObject);
		ImposterFile =  new File(Environment.getExternalStorageDirectory(), "Imposter/GlobalImposter.txt");
		RelationSet = false;
		AttributeSet = false;
		TotalAttribs = 0;
	}
	
	public AndroidArffCreator(AndroidFileManager AFMObject, boolean Append)
	{
		super(AFMObject);
		ImposterFile =  new File(Environment.getExternalStorageDirectory(), "Imposter/GlobalImposter.txt");
		RelationSet = false;
		AttributeSet = false;
		TotalAttribs = 0;
	}
	
	
	public AndroidArffCreator(String Filename, String FolderLocation)
	{
		super(Filename, FolderLocation);
		RelationSet = false;
		AttributeSet = false;
		TotalAttribs = 0;
	}
	
	public AndroidArffCreator(String Filename, String FolderLocation, boolean Append)
	{
		super(Filename, FolderLocation, Append);
		RelationSet = false;
		AttributeSet = false;
		TotalAttribs = 0;
	}
	public boolean IsValidDataType(String DT)
	{
		if (Arrays.asList(ValidDataTypes).contains(DT))
			return true;
		else
			return false;
	}
	public void SetRelation(String RelationVal)
	{
		//String CurrentLine = RelationTag+EncloseInQuotes(RelationVal);
		String CurrentLine = RelationTag+"  "+RelationVal;
		this.SaveData(CurrentLine);
		RelationSet = true;
	}
	
	public void SetAttributes(String [][]Attribs)
	{
			try
			{
				if (!RelationSet) throw new Exception("Relation is not set!");
				if (Attribs.length < 1) 									throw new Exception("Attributes array is empty");
				if (Attribs[0].length != 2)						 		throw new Exception("Attributes variable must be <attribute-name> <datatype> pair");
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		TotalAttribs = Attribs.length;
		String CurrentLine = "";
		AttribsTypeList = new String[Attribs.length]; 
		this.SaveData("");
		for (int i =0; i < TotalAttribs; i++)
		{
			AttribsTypeList[i] = Attribs[i][1];
			try
			{
				if ((!IsValidDataType(AttribsTypeList[i])) && (Attribs[i][0]!="class")) throw new Exception(AttribsTypeList[i]+" is not valid data type. Use: "+Arrays.toString(ValidDataTypes));
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//CurrentLine = AttributeTag+EncloseInQuotes(Attribs[i][0])+" "+Attribs[i][1];
			CurrentLine = AttributeTag+Attribs[i][0]+" "+Attribs[i][1];
			this.SaveData(CurrentLine);
		}
		AttributeSet = true;
	}
	
	public void SetData(String[][] Data) throws Exception
	{
		Log.i(TAG, "Data[0].length = "+Data[0].length+" TotalAttribs ="+TotalAttribs);
		if (!AttributeSet) 											throw new Exception("Attributes are not set!");
		if (Data.length < 1) 										throw new Exception("Given data is empty");
		if (Data[0].length != (TotalAttribs)) 		throw new Exception("Total attributes in header does not match with provided in data section"+TotalAttribs);
		this.SaveData("");
		this.SaveData(DataTag);
		String CurrentLine;
		for (int i =0; i<Data.length; i++)
		{
			CurrentLine = "";
			for (int j=0; j<Data[i].length; j++)
			{
			//	CurrentLine += ((j==0)?"":",")+((AttribsTypeList[j]=="STRING")?EncloseInQuotes(Data[i][j]):Data[i][j]);
				CurrentLine += ((j==0)?"":",")+Data[i][j];
			}
			this.SaveData(CurrentLine);
		}
	}
	
	public void SetData(String[] Data1)
	{
		String[][] Data = new String[Data1.length][TotalAttribs];
		for (int i = 0; i < Data1.length; i++)
		{
			if (!Data[i].equals(null))
			{
				String[] CurrentLine = Data1[i].split(",");
				Log.i(TAG, "i = " + i +"TotalAttribs = "+TotalAttribs+" CurrentLine: "+Arrays.toString(CurrentLine));
				for (int j =0; j < CurrentLine.length; j++)
				{
					Data[i][j] = CurrentLine[j];
				}				
			}
		}
		try
		{
			this.SetData(Data);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// Overloaded SetData Function for Double Type Feature Matrix
	
	public void SetData(double[][] Data) throws Exception
	{
		//DecimalFormat doubleFormator = new DecimalFormat("0.000000");
		Log.i(TAG, "Data[0].length = "+Data[0].length+" TotalAttribs ="+TotalAttribs+" number of rows "+Data.length);
		if (!AttributeSet) 											throw new Exception("Attributes are not set!");
		if (Data.length < 1) 										throw new Exception("Given data is empty");
		if (Data[0].length != (TotalAttribs-1)) 		throw new Exception("Total attributes in header does not match with provided in data section");
		this.SaveData("");
		this.SaveData(DataTag);
		String CurrentLine;
		for (int i =0; i<Data.length; i++)
		{
			CurrentLine = "";
			for (int j=0; j<Data[i].length; j++)
			{
				CurrentLine += ((j==0)?"":",")+String.valueOf(Data[i][j]);;
				//CurrentLine += ((j==0)?"":",")+doubleFormator.format(Data[i][j]);;
				
			}
			this.SaveData(CurrentLine+",Genuine");
		}
	}
	
	// Added code to Append the Imposters run time
	
	public void appendImposter() throws IOException
	{
		BufferedReader ImposterReader = new BufferedReader(new FileReader(ImposterFile));
		 Log.i(TAG, "Appending Imposters From the global Imposter File viz. : "+ImposterFile.getAbsolutePath());
    String CurrentLine;
    while((CurrentLine = ImposterReader.readLine()) != null) {
    	this.SaveData(CurrentLine);
    }
    ImposterReader.close();
		
	}
	
	public boolean IsRelationSet()
	{
		return RelationSet;
	}
	
	public boolean IsAttributeSet()
	{
		return AttributeSet;
	}
	@Override
	public void Close()
	{
		this.CloseWriter();
	}
}
