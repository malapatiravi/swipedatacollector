package com.SwipeData.authentication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;
import android.util.Log;

/**
 *
 * @author RAJESH
 */
public class WekaClassifier {
    public File TrainingFile;
    public File TestingFile;
    private String TAG = "SClass";
    //private int Threshold = 45;
    
    private double PercentageInAccuracy;
    private double PercentageAccuracy;
    
    public WekaClassifier(File TrainingFile, File TestingFile) throws FileNotFoundException
    {
    
      this.TrainingFile=TrainingFile;  
      this.TestingFile=TestingFile;
      Log.i(TAG, "Training file: "+TrainingFile.getAbsolutePath()+", Testing file: "+TestingFile.getAbsolutePath());
     
    }
    
    public void ClassifyThroughNaiveBayes() throws Exception 
    {
    	
    	Instances Trainer;
      Instances Tester;
      
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));

      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully  ");
   
      Trainer.setClassIndex(Trainer.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
  
      Log.i(TAG, "Uploading Testing File : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
      Log.i(TAG, TestingFile.getAbsolutePath()+" Testing File Uploaded Successfully  ");
      
   
   // Setting Class Attribute Index for Testing Arff
      Tester.setClassIndex(Tester.numAttributes()-1);
     
   // Instantiating Classifier Object
      weka.classifiers.Classifier ClassifierObject = new NaiveBayes();
   // 
      ClassifierObject.buildClassifier(Trainer);
      Evaluation Evaluator = new Evaluation(Trainer);
      Evaluator.evaluateModel(ClassifierObject, Tester);
      
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();
      
      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);


    }
    
    public boolean getTheDecisionUsingNaiveBayes() throws Exception 
    {
      // Classifying Through Naive Bayes and Setting up the Score Percentage Accuracy and Percentage Inaccuracy 	
     	ClassifyThroughNaiveBayes();
    	Log.i(TAG, "Correctly classified Using Naive Bayes with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy);
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    public boolean getTheDecisionUsingNaiveBayesForSensors() throws Exception 
    {
      // Classifying Through Naive Bayes and Setting up the Score Percentage Accuracy and Percentage Inaccuracy 	
     	ClassifyThroughNaiveBayes();
    	Log.i(TAG, "Correctly classified Using Naive Bayes with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy);
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    public void ClassifyThroughLogisticRegression() throws Exception
    {
    	
    	Instances Trainer;
      Instances Tester;
      
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));

      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully  ");
      
      // Setting Class Attribute Index for Traning Arff
      Trainer.setClassIndex(Trainer.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
     
      Log.i(TAG, "Uploading Testing File.... : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
      Log.i(TAG, TestingFile.getAbsolutePath()+" Testing File Uploaded Successfully...  ");
      
   // Setting Class Attribute Index for Testing Arff
      Tester.setClassIndex(Tester.numAttributes()-1);
      
   // Instantiating Classifier Object
      
      weka.classifiers.functions.Logistic ClassifierObject = new weka.classifiers.functions.Logistic();
      ClassifierObject.setOptions(weka.core.Utils.splitOptions("Logistic -R 0.1 -M -1"));
   // Build Classifier  
      ClassifierObject.buildClassifier(Trainer);
      Evaluation Evaluator = new Evaluation(Trainer);
      Evaluator.evaluateModel(ClassifierObject, Tester);
      
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();

    	
    }
    
    
    public boolean getTheDecisionUsingLogisticRegression() throws Exception 
    {
    	// Clasifying through Logistic Regression to set the values in Percentage Accuracy and Inaccuracy
    	ClassifyThroughLogisticRegression();
      
      Log.i(TAG, "Correctly classified Using LogisticRegression() "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
      
      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
      
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    public boolean getTheDecisionUsingLogisticRegressionForSensors() throws Exception 
    {
    	// Clasifying through Logistic Regression to set the values in Percentage Accuracy and Inaccuracy
    	ClassifyThroughLogisticRegression();
      
      Log.i(TAG, "Correctly classified Using LogisticRegression() "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
      
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    public void ClassifyThroughSVM() throws Exception
    {
    	Instances Trainer;
      Instances Tester;
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));

      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully...  ");
      
      // Setting Class Attribute Index for Traning Arff
      Trainer.setClassIndex(Trainer.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));

      Log.i(TAG, "Uploading Testing File.... : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
      Log.i(TAG, TestingFile.getAbsolutePath()+" Testing File Uploaded Successfully...  ");
     
   // Setting Class Attribute Index for Testing Arff
      Tester.setClassIndex(Tester.numAttributes()-1);
      
   // Instantiating Classifier Object
      
      weka.classifiers.functions.SMO ClassifierObject = new weka.classifiers.functions.SMO();
      //
      ClassifierObject.setOptions(weka.core.Utils.splitOptions("-C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -M -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01\""));
      ClassifierObject.buildClassifier(Trainer);
      Evaluation Evaluator = new Evaluation(Trainer);
      Evaluator.evaluateModel(ClassifierObject, Tester);
      
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();

      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
  
    }
    
    public boolean getTheDecisionUsingSVM() throws Exception 
    {
   // Clasifying through SVM to set the values in Percentage Accuracy and Inaccuracy
    	ClassifyThroughSVM();
      Log.i(TAG, "Correctly classified through SVM with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy+"%");
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    public boolean getTheDecisionUsingSVMForSensors() throws Exception 
    {
   // Clasifying through SVM to set the values in Percentage Accuracy and Inaccuracy
    	ClassifyThroughSVM();
      Log.i(TAG, "Correctly classified through SVM with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy+"%");
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    
    public void ClassifyThroughRandomForest() throws Exception
    {
    	Instances Trainer;
      Instances Tester;
     
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));
          
      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully  ");
      
      // Setting Class Attribute Index for Traning Arff
      Trainer.setClassIndex(Trainer.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
     
      Log.i(TAG, "Uploading Testing File.... : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
      Log.i(TAG, TestingFile.getAbsolutePath()+" Testing File Uploaded Successfully...  ");
     
   // Setting Class Attribute Index for Testing Arff
      Tester.setClassIndex(Tester.numAttributes()-1);
      
   // Instantiating Classifier Object
      
      weka.classifiers.trees.RandomForest ClassifierObject = new weka.classifiers.trees.RandomForest();
      ClassifierObject.setOptions(weka.core.Utils.splitOptions("-I 1000 -K 0 -S 1"));
      ClassifierObject.buildClassifier(Trainer);
     
      Evaluation Evaluator = new Evaluation(Trainer);
      Evaluator.evaluateModel(ClassifierObject, Tester);
      
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();

      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
 
    }
    
    public boolean getTheDecisionUsingRandomForest() throws Exception 
    {
    	// Clasifying through RandomForest() to set the values in Percentage Accuracy and Inaccuracy
    		ClassifyThroughRandomForest();
      
    		Log.i(TAG, "Correctly classified using RandomForest() with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy);
    		return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    public boolean getTheDecisionUsingRandomForestForSensors() throws Exception 
    {
    	// Clasifying through RandomForest() to set the values in Percentage Accuracy and Inaccuracy
    		ClassifyThroughRandomForest();
      
    		Log.i(TAG, "Correctly classified using RandomForest() with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy);
    		return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
    public double getClassificationScore()
    {
      	return PercentageAccuracy;
    }
    
   
}
