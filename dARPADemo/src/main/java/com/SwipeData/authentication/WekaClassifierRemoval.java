package com.SwipeData.authentication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.SwipeData.generalobjects.AndroidFileManager;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;
import weka.core.Utils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import android.util.Log;

/**
 *
 * @author RAJESH
 */
public class WekaClassifierRemoval {
    public File TrainingFile;
    public File TestingFile;
    private AndroidFileManager ProbabilitiesFile;
    private String TAG = "SClass";
    //private int Threshold = 45;
    
    private double PercentageInAccuracy;
    private double PercentageAccuracy;
    private int IndicesOFArrayToBeRemoved[];
    private double Threshold;
    private double [] ProbablitiesBayes;
    private double [] ProbablitiesSVM;
    private double [] ProbablitiesLR;
    private double [] ProbablitiesRF;
    private boolean finalDecision;
    private int finalVote=0;
    
    public WekaClassifierRemoval(File TrainingFile, File TestingFile,AndroidFileManager ProbFile) throws FileNotFoundException
    {
    
      this.TrainingFile=TrainingFile;  
      this.TestingFile=TestingFile;
      this.ProbabilitiesFile=new AndroidFileManager(ProbFile.GetCurrentFolder(), "ProbabilitiesFile.txt");
      Log.i(TAG, "Training file: "+TrainingFile.getAbsolutePath()+", Testing file: "+TestingFile.getAbsolutePath());
      Threshold=0.80;
      
     
    }
    public WekaClassifierRemoval(File TrainingFile, File TestingFile, String path)
    {
        this.TrainingFile=TrainingFile;
        this.TestingFile=TestingFile;
        this.ProbabilitiesFile=new AndroidFileManager(path, "ProbabilitiesFile.txt");
        Log.i(TAG, "Training file: "+TrainingFile.getAbsolutePath()+", Testing file: "+TestingFile.getAbsolutePath());
        Threshold=0.80;
    }
    public WekaClassifierRemoval(AndroidFileManager ProbFile) throws FileNotFoundException
    {
    
      this.TrainingFile=new File(ProbFile.GetFullPath(), "Training.arff");//TrainingFile;  
      this.TestingFile=new File(ProbFile.GetFullPath(), "Testing.arff");//TrainingFile;  
      this.ProbabilitiesFile=new AndroidFileManager(ProbFile.GetCurrentFolder(), "ProbabilitiesFile.txt");
      Log.i(TAG, "Training file in AFM loaded constructor: "+TrainingFile.getAbsolutePath()+", Testing file: "+TestingFile.getAbsolutePath());
      Log.i(TAG, "Full path is: "+ProbFile.GetFullPath());
      Threshold=0.80;
    
    }
    public WekaClassifierRemoval(File TrainingFile, File TestingFile) throws FileNotFoundException
    {
  
      this.TrainingFile=TrainingFile;  
      this.TestingFile=TestingFile;
      Log.i(TAG, "Training file: "+TrainingFile.getAbsolutePath()+", Testing file: "+TestingFile.getAbsolutePath());
      Threshold=0.80;

    }
    
    
//*******Setting Feature Indexes*******************************    
    public void setFeatureIndexesToBeRemoved(int Indices[])
    {
    	IndicesOFArrayToBeRemoved = new int[Indices.length];
    	for(int i=0;i<Indices.length;i++)
    	{
    		
    		IndicesOFArrayToBeRemoved[i]=Indices[i];
    		
    	}
    	Log.i(TAG, "Set tHe Attribute Indices to be removed are ------");
    	for(int i=0;i<Indices.length;i++)
    	{
    	 Log.i(TAG, String.valueOf(Indices[i]));
    	}
    }
    public void setThreshold(double Th)
    {
    	Threshold=Th;
    	
    }

    //*********************NAIVE BAYES*******************
    public void ClassifyThroughNaiveBayes() throws Exception 
    {
    	
    	Instances Trainer;
      Instances Tester;
      
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));

      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully  ");
 //     Trainer.setClassIndex(Trainer.numAttributes()-1);
   
      //*********************************************************
      Instances       TrainerAfterRemoval;
      Remove          RemoveObject;
      RemoveObject = new Remove();
      RemoveObject.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObject.setInvertSelection(false);
      RemoveObject.setInputFormat(Trainer);
      TrainerAfterRemoval = Filter.useFilter(Trainer, RemoveObject);
      // Setting Class Attribute Index for Traning Arff
      TrainerAfterRemoval.setClassIndex(TrainerAfterRemoval.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
  
      Log.i(TAG, "Uploading Testing File : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
  
      Instances       TesterAfterRemoval;
      Remove          RemoveObjectTest;
      RemoveObjectTest = new Remove();
      RemoveObjectTest.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObjectTest.setInvertSelection(false);
      RemoveObjectTest.setInputFormat(Tester);
      TesterAfterRemoval = Filter.useFilter(Tester, RemoveObjectTest);

   // Setting Class Attribute Index for Testing Arff
    TesterAfterRemoval.setClassIndex(TesterAfterRemoval.numAttributes()-1);
    //  Tester.setClassIndex(Tester.numAttributes()-1);
     
   // Instantiating Classifier Object
     weka.classifiers.Classifier ClassifierObject = new NaiveBayes();
    
     ClassifierObject.buildClassifier(TrainerAfterRemoval);
     Evaluation Evaluator = new Evaluation(TrainerAfterRemoval);
     Evaluator.evaluateModel(ClassifierObject, TesterAfterRemoval);
     double[] Distribution;
     ProbablitiesBayes= new double[TesterAfterRemoval.numInstances()];
      System.out.println("# Inside Bayes- Actual - Predicted - Distribution....#");
      for (int i = 0; i < TesterAfterRemoval.numInstances(); i++)
      {
      	double pred = ClassifierObject.classifyInstance(TesterAfterRemoval.instance(i));
      	Distribution = ClassifierObject.distributionForInstance(TesterAfterRemoval.instance(i));
      	ProbablitiesBayes[i]=Distribution[0];
      	System.out.print((i+1) + " - ");
      	System.out.print(TesterAfterRemoval.instance(i).toString(TesterAfterRemoval.classIndex()) + " - ");
      	System.out.print(TesterAfterRemoval.classAttribute().value((int) pred) + " - ");
      	System.out.println(Utils.arrayToString(Distribution));
      	
      }
      AndroidFileManager BayesProbabilitiesFile=new AndroidFileManager(ProbabilitiesFile.GetCurrentFolder(), "BayesProbabilitiesFile.txt");
     // BayesProbabilitiesFile.SaveData("NaiveBayes Results");
      for(int i=0;i<ProbablitiesBayes.length;i++)
      	BayesProbabilitiesFile.SaveData(String.valueOf(ProbablitiesBayes[i]));

//        ClassifierObject.buildClassifier(Trainer);
//        Evaluation Evaluator = new Evaluation(Trainer);
 //       Evaluator.evaluateModel(ClassifierObject, Tester);
//     
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();
      
      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);


    }
    
    public boolean getTheDecisionUsingNaiveBayes() throws Exception 
    {
      // Classifying Through Naive Bayes and Setting up the Score Percentage Accuracy and Percentage Inaccuracy 	
     	ClassifyThroughNaiveBayes();
    	Log.i(TAG, "Correctly classified Using Naive Bayes with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy);
      return PercentageAccuracy>9*PercentageInAccuracy;
    }
    
    public double [] getProbablities() throws Exception
    {
    	
    	ClassifyThroughNaiveBayes();
    	return ProbablitiesBayes;
    }
   
    //********************Logistic Regression **************************
    public void ClassifyThroughLogisticRegression() throws Exception
    {
    	
    	Instances Trainer;
      Instances Tester;
      
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));

      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully  ");
 //     Trainer.setClassIndex(Trainer.numAttributes()-1);
      Instances       TrainerAfterRemoval;
      Remove          RemoveObject;
      RemoveObject = new Remove();
      RemoveObject.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObject.setInvertSelection(false);
      RemoveObject.setInputFormat(Trainer);
      TrainerAfterRemoval = Filter.useFilter(Trainer, RemoveObject);
 // Setting Class Attribute Index for Traning Arff
      TrainerAfterRemoval.setClassIndex(TrainerAfterRemoval.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
  
      Log.i(TAG, "Uploading Testing File : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
      Log.i(TAG, TestingFile.getAbsolutePath()+" Testing File Uploaded Successfully  ");
 
      Instances       TesterAfterRemoval;
      Remove          RemoveObjectTest;
      RemoveObjectTest = new Remove();
      RemoveObjectTest.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObjectTest.setInvertSelection(false);
      RemoveObjectTest.setInputFormat(Tester);
      TesterAfterRemoval = Filter.useFilter(Tester, RemoveObjectTest);
 
   // Setting Class Attribute Index for Testing Arff
      TesterAfterRemoval.setClassIndex(TesterAfterRemoval.numAttributes()-1);
    //  Tester.setClassIndex(Tester.numAttributes()-1);
      weka.classifiers.functions.Logistic ClassifierObject = new weka.classifiers.functions.Logistic();
      ClassifierObject.setOptions(weka.core.Utils.splitOptions("Logistic -R 0.1 -M -1"));
   // Instantiating Classifier Object
   
     ClassifierObject.buildClassifier(TrainerAfterRemoval);
     Evaluation Evaluator = new Evaluation(TrainerAfterRemoval);
     Evaluator.evaluateModel(ClassifierObject, TesterAfterRemoval);
     double[] Distribution;
     ProbablitiesLR= new double[TesterAfterRemoval.numInstances()];
      System.out.println("# Inside LR- Actual - Predicted - Distribution....#");
      for (int i = 0; i < TesterAfterRemoval.numInstances(); i++)
      {
      	double pred = ClassifierObject.classifyInstance(TesterAfterRemoval.instance(i));
      	Distribution = ClassifierObject.distributionForInstance(TesterAfterRemoval.instance(i));
      	ProbablitiesLR[i]=Distribution[0];
      	System.out.print((i+1) + " - ");
      	System.out.print(TesterAfterRemoval.instance(i).toString(TesterAfterRemoval.classIndex()) + " - ");
      	System.out.print(TesterAfterRemoval.classAttribute().value((int) pred) + " - ");
      	System.out.println(Utils.arrayToString(Distribution));
      	
      }
      AndroidFileManager LRProbabilitiesFile=new AndroidFileManager(ProbabilitiesFile.GetCurrentFolder(), "LRProbabilitiesFile.txt");
     for(int i=0;i<ProbablitiesLR.length;i++)
    	 LRProbabilitiesFile.SaveData(String.valueOf(ProbablitiesLR[i]));

//        ClassifierObject.buildClassifier(Trainer);
//        Evaluation Evaluator = new Evaluation(Trainer);
 //       Evaluator.evaluateModel(ClassifierObject, Tester);
//     
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();
  }
 
  public boolean getTheDecisionUsingLogisticRegression() throws Exception 
    {
    	// Clasifying through Logistic Regression to set the values in Percentage Accuracy and Inaccuracy
    	ClassifyThroughLogisticRegression();
      
      Log.i(TAG, "Correctly classified Using LogisticRegression() "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
      
      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
      
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
   
//**************************SVM********************************    
    
    public void ClassifyThroughSVM() throws Exception
    {
    	Instances Trainer;
      Instances Tester;
      
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));

      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully  ");
 //     Trainer.setClassIndex(Trainer.numAttributes()-1);
   
      //*********************************************************
      Instances       TrainerAfterRemoval;
      Remove          RemoveObject;
      RemoveObject = new Remove();
      RemoveObject.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObject.setInvertSelection(false);
      RemoveObject.setInputFormat(Trainer);
      TrainerAfterRemoval = Filter.useFilter(Trainer, RemoveObject);
           
      //*********************************************************
      // Setting Class Attribute Index for Traning Arff
      TrainerAfterRemoval.setClassIndex(TrainerAfterRemoval.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
  
      Log.i(TAG, "Uploading Testing File : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
      Log.i(TAG, TestingFile.getAbsolutePath()+" Testing File Uploaded Successfully  ");
        //*********************************************************
      Instances       TesterAfterRemoval;
      Remove          RemoveObjectTest;
      RemoveObjectTest = new Remove();
      RemoveObjectTest.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObjectTest.setInvertSelection(false);
      RemoveObjectTest.setInputFormat(Tester);
      TesterAfterRemoval = Filter.useFilter(Tester, RemoveObjectTest);
     
   // Setting Class Attribute Index for Testing Arff
      TesterAfterRemoval.setClassIndex(TesterAfterRemoval.numAttributes()-1);
    //  Tester.setClassIndex(Tester.numAttributes()-1);
      weka.classifiers.functions.SMO ClassifierObject = new weka.classifiers.functions.SMO();
      ClassifierObject.setOptions(weka.core.Utils.splitOptions("-C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -M -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01\""));
   // Instantiating Classifier Object
  
     ClassifierObject.buildClassifier(TrainerAfterRemoval);
     Evaluation Evaluator = new Evaluation(TrainerAfterRemoval);
     Evaluator.evaluateModel(ClassifierObject, TesterAfterRemoval);
     double[] Distribution;
     ProbablitiesSVM= new double[TesterAfterRemoval.numInstances()];
      System.out.println("# Inside SVM- Actual - Predicted - Distribution....#");
      for (int i = 0; i < TesterAfterRemoval.numInstances(); i++)
      {
      	double pred = ClassifierObject.classifyInstance(TesterAfterRemoval.instance(i));
      	Distribution = ClassifierObject.distributionForInstance(TesterAfterRemoval.instance(i));
      	ProbablitiesSVM[i]=Distribution[0];
      	System.out.print((i+1) + " - ");
      	System.out.print(TesterAfterRemoval.instance(i).toString(TesterAfterRemoval.classIndex()) + " - ");
      	System.out.print(TesterAfterRemoval.classAttribute().value((int) pred) + " - ");
      	System.out.println(Utils.arrayToString(Distribution));
      	
      }
      AndroidFileManager SVMProbabilitiesFile=new AndroidFileManager(ProbabilitiesFile.GetCurrentFolder(), "SVMProbabilitiesFile.txt");
     // SVMProbabilitiesFile.SaveData("SVM Results------");
      for(int i=0;i<ProbablitiesSVM.length;i++)
      	SVMProbabilitiesFile.SaveData(String.valueOf(ProbablitiesSVM[i]));

//        ClassifierObject.buildClassifier(Trainer);
//        Evaluation Evaluator = new Evaluation(Trainer);
 //       Evaluator.evaluateModel(ClassifierObject, Tester);
//     
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();
      
      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);
  
    }
    
    public boolean getTheDecisionUsingSVM() throws Exception 
    {
   // Clasifying through SVM to set the values in Percentage Accuracy and Inaccuracy
    	ClassifyThroughSVM();
      Log.i(TAG, "Correctly classified through SVM with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy+"%");
      return PercentageAccuracy>2*PercentageInAccuracy;
    }
    
   
    public void ClassifyThroughRandomForest() throws Exception
    {
    	Instances Trainer;
      Instances Tester;
      
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));

      Log.i(TAG, "Uploading Traning File : "+TrainingFile.getAbsolutePath());
      Trainer = new Instances(TrainingReader);
      Log.i(TAG, TrainingFile.getAbsolutePath()+" Traning File Uploaded Successfully  ");
 //     Trainer.setClassIndex(Trainer.numAttributes()-1);
   
      //*********************************************************
      Instances       TrainerAfterRemoval;
      Remove          RemoveObject;
      RemoveObject = new Remove();
      RemoveObject.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObject.setInvertSelection(false);
      RemoveObject.setInputFormat(Trainer);
      TrainerAfterRemoval = Filter.useFilter(Trainer, RemoveObject);
           
      //*********************************************************
      // Setting Class Attribute Index for Traning Arff
      TrainerAfterRemoval.setClassIndex(TrainerAfterRemoval.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
  
      Log.i(TAG, "Uploading Testing File : "+TestingFile.getAbsolutePath());
      Tester = new Instances(TestingReader);
      Log.i(TAG, TestingFile.getAbsolutePath()+" Testing File Uploaded Successfully  ");
        //*********************************************************
      Instances       TesterAfterRemoval;
      Remove          RemoveObjectTest;
      RemoveObjectTest = new Remove();
      RemoveObjectTest.setAttributeIndicesArray(IndicesOFArrayToBeRemoved);
      RemoveObjectTest.setInvertSelection(false);
      RemoveObjectTest.setInputFormat(Tester);
      TesterAfterRemoval = Filter.useFilter(Tester, RemoveObjectTest);
     
   // Setting Class Attribute Index for Testing Arff
      TesterAfterRemoval.setClassIndex(TesterAfterRemoval.numAttributes()-1);
    //  Tester.setClassIndex(Tester.numAttributes()-1);
    weka.classifiers.trees.RandomForest ClassifierObject = new weka.classifiers.trees.RandomForest();
    ClassifierObject.setOptions(weka.core.Utils.splitOptions("-I 1000 -K 0 -S 1"));
   // Instantiating Classifier Object
  
     ClassifierObject.buildClassifier(TrainerAfterRemoval);
     Evaluation Evaluator = new Evaluation(TrainerAfterRemoval);
     Evaluator.evaluateModel(ClassifierObject, TesterAfterRemoval);
     double[] Distribution;
     ProbablitiesRF= new double[TesterAfterRemoval.numInstances()];
      System.out.println("# Inside Random Forest- Actual - Predicted - Distribution....#");
      for (int i = 0; i < TesterAfterRemoval.numInstances(); i++)
      {
      	double pred = ClassifierObject.classifyInstance(TesterAfterRemoval.instance(i));
      	Distribution = ClassifierObject.distributionForInstance(TesterAfterRemoval.instance(i));
      	ProbablitiesRF[i]=Distribution[0];
      	System.out.print((i+1) + " - ");
      	System.out.print(TesterAfterRemoval.instance(i).toString(TesterAfterRemoval.classIndex()) + " - ");
      	System.out.print(TesterAfterRemoval.classAttribute().value((int) pred) + " - ");
      	System.out.println(Utils.arrayToString(Distribution));
      }
      AndroidFileManager RFProbabilitiesFile=new AndroidFileManager(ProbabilitiesFile.GetCurrentFolder(), "RFProbabilitiesFile.txt");
     // RFProbabilitiesFile.SaveData("Random Forest Results------");
      for(int i=0;i<ProbablitiesRF.length;i++)
      	RFProbabilitiesFile.SaveData(String.valueOf(ProbablitiesRF[i]));

//        ClassifierObject.buildClassifier(Trainer);
//        Evaluation Evaluator = new Evaluation(Trainer);
 //       Evaluator.evaluateModel(ClassifierObject, Tester);
//     
      PercentageAccuracy=Evaluator.pctCorrect();
      PercentageInAccuracy=Evaluator.pctIncorrect();
      
      Log.i(TAG, "Correctly classified "+PercentageAccuracy+" And Incorrectly Clasified "+PercentageInAccuracy);

 
    }
    
    public boolean getTheDecisionUsingRandomForest() throws Exception 
    {
    	// Clasifying through RandomForest() to set the values in Percentage Accuracy and Inaccuracy
    		ClassifyThroughRandomForest();
      
    		Log.i(TAG, "Correctly classified using RandomForest() with "+PercentageAccuracy+"% And Incorrectly Clasified "+PercentageInAccuracy);
    		return PercentageAccuracy>2*PercentageInAccuracy;
    }
   
    public double getClassificationScore()
    {
      	return PercentageAccuracy;
    }
    
    public void initAllClassifiersProbabilities() throws Exception
    {
    	ClassifyThroughNaiveBayes();
    	ClassifyThroughLogisticRegression();
    	ClassifyThroughSVM();
    	ClassifyThroughRandomForest();
    	
    }
//    public boolean getTheCombinedDecision() throws Exception
//    {
//    	
//    	initAllClassifiersProbabilities();
//    	//getTheDecisionUsingNaiveBayes();
//    	double CurrentScore=0.0;
//    	int Decision1,Decision2,Decision3,Decision4;
//    	for (int i=0;i<ProbablitiesBayes.length;i++)
//    	{
//    		if(ProbablitiesBayes[i]>Threshold)
//    			CurrentScore++;
//    	}
//    	if(CurrentScore>(ProbablitiesBayes.length*0.66)) Decision1=1;
//    	else Decision1=0;
//    	
//    	Log.i(TAG, "----Scores from Bayes----:"+(CurrentScore/(double)ProbablitiesBayes.length));
//     	
//    	
//    	//getTheDecisionUsingLogisticRegression();
//    	CurrentScore=0;
//     	for (int i=0;i<ProbablitiesLR.length;i++)
//    	{
//    		if(ProbablitiesLR[i]>Threshold)
//    			CurrentScore++;
//    	}
//     	if(CurrentScore>(ProbablitiesLR.length*0.66)) Decision2=1;
//    	else Decision2=0;
//     	Log.i(TAG, "----Scores from LR----:"+(CurrentScore/(double)ProbablitiesLR.length));
//    	
//     //	getTheDecisionUsingRandomForest();
//     	CurrentScore=0;
//     	for (int i=0;i<ProbablitiesRF.length;i++)
//    	{
//    		if(ProbablitiesRF[i]>Threshold)
//    			CurrentScore++;
//    	}     	
//    	if(CurrentScore>(ProbablitiesRF.length*0.66)) Decision3=1;
//    	else Decision3=0;
//    	Log.i(TAG, "----Scores from RF----:"+(CurrentScore/(double)ProbablitiesRF.length));
//     	
//     //	getTheDecisionUsingSVM();
//     	CurrentScore=0;
//     	for (int i=0;i<ProbablitiesSVM.length;i++)
//    	{
//    		if(ProbablitiesSVM[i]>Threshold)
//    			CurrentScore++;
//    	}     	
//    	if(CurrentScore>(ProbablitiesSVM.length*0.66)) Decision4=1;
//    	else Decision4=0;
//    	Log.i(TAG, "----Scores from SVM----:"+(CurrentScore/(double)ProbablitiesSVM.length));
//   	
//     	int CombinedDecision=Decision1+Decision2+Decision3+Decision4;
//     	
//     	if(CombinedDecision>=3)finalDecision=true;
//     	else finalDecision=false;
//     	
//     	return finalDecision;
//    	
//    }
//    
    public boolean getFinalDecision() throws Exception
    {
    	finalVote=0;
    	ClassifyThroughNaiveBayes();
    	if(PercentageAccuracy>Threshold) finalVote++;
    	ClassifyThroughLogisticRegression();
    	if(PercentageAccuracy>Threshold) finalVote++;
    	ClassifyThroughSVM();
    	if(PercentageAccuracy>Threshold) finalVote++;
    	ClassifyThroughRandomForest();
    	if(PercentageAccuracy>Threshold) finalVote++;
    	
    	Log.i(TAG, "Final Vote --"+finalVote);
    	
    	if(finalVote>3) finalDecision=true;
    	else finalDecision=false;
    	
    	return finalDecision;
    }
    
    public double[] GetAllScores()
    {
    	double[] AllScores = new double[4];
    	try
			{
                getTheDecisionUsingNaiveBayes();
	    	    AllScores[0] = PercentageAccuracy;
	    	    getTheDecisionUsingLogisticRegression();
	    	    AllScores[1] = PercentageAccuracy;
	    	    getTheDecisionUsingRandomForest();
	    	    AllScores[2] = PercentageAccuracy;
	    	    getTheDecisionUsingSVM();
	    	    AllScores[3] = PercentageAccuracy;
			}
        catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	return AllScores;
    }
    
   
}
