package com.SwipeData.authentication;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import com.SwipeData.generalobjects.AndroidFileManager;
import com.SwipeData.generalobjects.FileWriterObject;

import android.util.Log;

/**
 *
 * @author RAJESH
 */
public class SensorFileManager {
    private final String	TAG	= "SFM";
		public File FileName;
    public String FileType;
    private String DataFolder = "";
    public SensorFileManager(File FN, String DF)
    {
    	this.DataFolder = DF;
    	this.FileName= new File(DataFolder,FN.getName());
    	Log.i(TAG, "Data folder is in sensor file manager is = "+DF+" and filename="+FileName.getAbsolutePath());
    	//new File(Environment.getExternalStorageDirectory(), DataFolder);
      
    }
    
    public SensorFileManager(AndroidFileManager AFM)
    {
    	this.DataFolder = AFM.GetCurrentFolder();
    	this.FileName= new File(AFM.GetAbsoluteFilename());
    	Log.i(TAG, "Data folder is in sensor file manager is = "+FileName);
    	//new File(Environment.getExternalStorageDirectory(), DataFolder);
      
    }
    public static boolean isDouble(String Str)  
    {  
        try  
        {  
         @SuppressWarnings("unused")
				double DBL = Double.parseDouble(Str);  
        }  
        catch(NumberFormatException nfe)  
        {  
         return false;  
        }  
        return true;  
    }

    @SuppressWarnings("resource")
		public double[] txtReadWindow(int column, int from, int windowSize) throws FileNotFoundException, IOException
     {
        int LineNumber=0, ElementIndex, ArrIndex=0;        
        double ElementArray[] = new double[windowSize+1];
        BufferedReader bufRdr;
        bufRdr = new BufferedReader(new FileReader(FileName));
        String Line;
        //read each line of text file
        String Token;
        while((Line = bufRdr.readLine()) != null)
           {
               LineNumber++;
               if(LineNumber>=from && LineNumber<=(from+windowSize))
               {
                 StringTokenizer st = new StringTokenizer(Line,",");
                 ElementIndex=0;
                 while (st.hasMoreTokens())
                 {
                     //get next token and store it in the array if it is double and the desired column
                     Token =st.nextToken();
                     ElementIndex++;
                     if(isDouble(Token)&& ElementIndex==column)
                     ElementArray[ArrIndex++]=Double.parseDouble(Token);
                }
               }
               else if (LineNumber>(from+windowSize)) break;
               
         } 
    
         return ElementArray;
      }
  
     public int getNumberOfTotalSamples() throws FileNotFoundException, IOException
     {
        int LineNumber=0;      
        @SuppressWarnings("resource")
				BufferedReader bufRdr = new BufferedReader(new FileReader(FileName));
        while(bufRdr.readLine()!= null)
             {
                LineNumber++;
             } 
        return LineNumber;
     }
   /* write feature matrix to file 
      public void writeMatrixToFile(double FVMatrix[][]) throws FileNotFoundException, IOException 
      {
        String StrToWrite;
        int NRows=FVMatrix.length;
        int NCols=FVMatrix[0].length;
        System.out.println("FileManager--Size of the Feature Matrix - ROws: "+NRows+ "and Columns"+NCols);
        System.out.println("FileName -"+FileName);
        FileWriter fstream;
        BufferedWriter Writer=null;
        try{  
              fstream = new FileWriter(FileName, false);
              Writer = new BufferedWriter(fstream);
           }
        catch(IOException e)
         {
           System.out.println("Exception: "+e);
         }
      
        for (int RowItr=0; RowItr<NRows ; RowItr++)
           {
               StrToWrite=""; 
               for(int ColItr=0;ColItr<NCols;ColItr++)
                {
                   StrToWrite += String.valueOf(FVMatrix[RowItr][ColItr]+(((ColItr<NCols-1))?",":""));
                  
                }
           //    System.out.println("Line To Write "+StrToWrite);
               Writer.write(StrToWrite);
               Writer.write("\n");
             }
            Writer.close();
       
   }
      *************/
      public void writeMatrixIntoFile(double[][] fvmatrix)
      {
           String Str = "";
           int NRow=fvmatrix.length;
           int NCol=fvmatrix[0].length;
//           float[][] FloatFeatureMatrix= new float[NRow][NCol];
//           for(int i=0;i<NRow;i++)
//           {
//          	 for(int j=0;j<NCol;j++)
//          	 {
//          		 
//          		 FloatFeatureMatrix[i][j]= (float)fvmatrix[i][j];
//          	 }
//          	 
//          	 
//           }
//           
           Log.i(TAG,"Feature Matrix with Rows = "+NRow+", NCols="+NCol+"Are Being Written to the Feature File"+FileName.getAbsolutePath());
           FileWriterObject FOBJ = new FileWriterObject(FileName);
        //   DecimalFormat doubleFormator = new DecimalFormat("0.000000");
           for (int i=0; i<NRow ; i++)
           {
             for(int j=0;j<NCol;j++)
             {
               Str += String.valueOf(fvmatrix[i][j]+(((j<NCol-1))?",":""));
       //       Str += ""+doubleFormator.format(fvmatrix[i][j])+(((j<NCol-1))?",":"");
            	
             }
             FOBJ.SaveData(Str);
           	Str = "";
           }
           FOBJ.CloseWriter();
      }
      
    
  
}
