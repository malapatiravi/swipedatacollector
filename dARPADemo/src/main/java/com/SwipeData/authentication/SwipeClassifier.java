package com.SwipeData.authentication;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

import android.os.Environment;
import android.util.Log;

import com.SwipeData.generalobjects.AndroidFileManager;
import com.SwipeData.generalobjects.ExcelReader;

public class SwipeClassifier
{
	private String TrainerFilename = "";
	private String TesterFilename = "";
	private String TrainerRootPath = "";
	private String TesterRootPath = "";
	private ExcelReader TrainerTemplate;
	private ExcelReader TesterTemplate;
	private File TesterFile;
	private File TrainerFile;
	private String TAG="Classifier";
	private float Z_THRESHOLD = 1.96f;
	private float TrainedData[][];
	private float TestingData[][];
	private int ZScore = 0;
	private int Z_Total_Threshold = 14;
	private int Majority = 0;
	private float EuclideanDistance = 0;
	private float E_THRESHOLD = 0.0f;
	private float ManhattanDistance = 0;
	private float M_THRESHOLD = 0.0f;
	private float Correlation = 0;
	private float C_THRESHOLD = 0.0f;
	private AndroidFileManager GlobalAFM;
	WekaClassifierRemoval WC;

	
	SwipeClassifier() throws Exception
	{
		throw new Exception("Trainer and tester file names are required");
	}
	
	public SwipeClassifier(String TrainerPath, String TrainerFile1, String TesterPath, String TesterFile1)
	{
		TrainerRootPath = TrainerPath;
		TesterRootPath = TesterPath;
		TesterFilename = TesterFile1;
		TrainerFilename = TrainerFile1;
		InitializeFiles();
	}

	public SwipeClassifier(File TrFile, File TeFile)
	{
		TesterFile = TeFile;
		TrainerFile = TrFile;
		LoadToWeka();
	}
	
	public SwipeClassifier(AndroidFileManager GAFM)
	{
		GlobalAFM = GAFM;
		TesterFile = new File(GAFM.GetFullPath(), "Testing.arff");
		LoadAFMToWeka();
	}

	public void LoadToWeka()
	{
		try
		{
			//WC = null;
			WC = new WekaClassifierRemoval(TrainerFile, TesterFile);
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void LoadAFMToWeka()
	{
		try
		{
			//WC = null;
			WC = new WekaClassifierRemoval(GlobalAFM);
			int UnwantedFeatures[] = {10,15,20,21,22,23};
			WC.setFeatureIndexesToBeRemoved(UnwantedFeatures);
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean GetTheDecisionUsingNaiveBayes()
	{
		try
		{
			return WC.getTheDecisionUsingNaiveBayes();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public double[] GetAllScores()
	{
		return WC.GetAllScores();
	}
	
	public double GetScoreFromWeka()
	{
		return WC.getClassificationScore();
	}
  public boolean GetTheDecisionUsingLogisticRegression() 
  {
		try
		{
			return WC.getTheDecisionUsingLogisticRegression();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
  }
  
  public boolean GetTheDecisionUsingSVM()
  {
		try
		{
			return WC.getTheDecisionUsingSVM();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
  }
  
  public boolean GetTheDecisionUsingRandomForest()
  {
		try
		{
			return WC.getTheDecisionUsingRandomForest();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
  }
  
  public double GetWekaClassificationScore()
  {
  	return WC.getClassificationScore();
  }
  
	private void InitializeFiles()
	{
		File TrainerRoot =  new File(Environment.getExternalStorageDirectory(), TrainerRootPath);
		File TesterRoot =  new File(Environment.getExternalStorageDirectory(), TesterRootPath);
		TesterFile = new File(TesterRoot, TesterFilename);
		TrainerFile = new File(TrainerRoot, TrainerFilename);
		try
		{
			TrainerTemplate 	= new ExcelReader(TrainerFilename, TrainerRootPath, 0, 0);
			TesterTemplate 		= new ExcelReader(TesterFilename,   TesterRootPath, 0, 0);
		}
		catch (Exception e)	{	e.printStackTrace();}
	}

	private void GetDataFromTemplates()
	{
		int TRTRows = TrainerTemplate.GetTotalRows();
		int TRTColumns = TrainerTemplate.GetTotalColumns();
		int TETRows = TesterTemplate.GetTotalRows();
		int TETColumns = TesterTemplate.GetTotalColumns();
		
		TrainedData = new float[TRTRows][TRTColumns];
		TestingData = new float[TETRows][TETColumns];

		TrainedData = TrainerTemplate.GetFloatData();
		TestingData = TesterTemplate.GetFloatData();

	}
	public void RefreshData()
	{
		ReInitialize();
		GetDataFromTemplates();
		NormalizeData();
	}

	public int GetZScore()
	{
		RefreshData();
		CalculateZScore();
		Log.i(TAG, "Z Score = "+ZScore);
		return ZScore;
	}
	
	public float GetEScore()
	{
		RefreshData();
		CalculateEuclidean();
		return EuclideanDistance;
	}
	
	public int GetMajority()
	{
		Majority = 0;
		RefreshData();
		CalculateEuclidean();
		CalculateManhattan();
		CalculateCorrelation();
		if (EuclideanDistance>=E_THRESHOLD) Majority++;
		if (ManhattanDistance>=M_THRESHOLD) Majority++;
		if (Correlation>=C_THRESHOLD) 			Majority++;
		return Majority;		
	}
	
	public float[] GetTrio()
	{
		float[] Trio = new float[3];
		RefreshData();
		CalculateEuclidean();
		CalculateManhattan();
		CalculateCorrelation();
		if (EuclideanDistance>=E_THRESHOLD) Majority++;
		if (ManhattanDistance>=M_THRESHOLD) Majority++;
		if (Correlation>=C_THRESHOLD) 			Majority++;
		Trio[0] = EuclideanDistance;
		Trio[1] = ManhattanDistance;
		Trio[2] = Correlation;
		return Trio;		
	}
	public boolean IsGenuine()
	{
		if (ZScore>=Z_Total_Threshold)	return true;
		else 														return false;
	}
	
	public void CalculateEuclidean()
	{
		int TRTRows = TrainerTemplate.GetTotalRows();
		int TRTColumns = TrainerTemplate.GetTotalColumns();
		int TETRows = TesterTemplate.GetTotalRows();
		float TrMeanVals[] = new float[TRTColumns];
		float TeMeanVals[] = new float[TRTColumns];
		//float TrStdDev[] = new float[TRTColumns];
		//float TeStdDev[] = new float[TRTColumns];
		float[] EValue = new float[TRTColumns];
		float TotalE = 0.0f;
		for (int i = 0; i<TRTColumns; i++)
		{
			TrMeanVals[i] = GetColumnMean(TrainedData, i, TRTRows);
			TeMeanVals[i] = GetColumnMean(TestingData, i, TETRows);
			EValue[i] = (float)Math.pow((TrMeanVals[i]-TeMeanVals[i]), 2.0);//+(Math.pow(TeMeanVals[i], 2.0)));//  ((Math.pow((TrMeanVals[i]), 2)) ));
			TotalE+=EValue[i];
			//TrStdDev[i] = GetColumnStdDev(TrainedData, i, TRTRows, TrMeanVals[i]);
			//TeStdDev[i] = GetColumnStdDev(TestingData, i, TETRows, TeMeanVals[i]);
			//if (Math.abs(EValue[i])<E_THRESHOLD)	Counter++;
			//Log.i(TAG, "E value at "+i+" = "+EValue[i]);
		}
		TotalE = (float) Math.sqrt(TotalE);
		Log.i(TAG, "Euclidean distance = "+TotalE);
		EuclideanDistance = TotalE;		
	}
	
	public void CalculateManhattan()
	{
		int TRTRows = TrainerTemplate.GetTotalRows();
		int TRTColumns = TrainerTemplate.GetTotalColumns();
		int TETRows = TesterTemplate.GetTotalRows();
		float TrMeanVals[] = new float[TRTColumns];
		float TeMeanVals[] = new float[TRTColumns];
		//float TrStdDev[] = new float[TRTColumns];
		//float TeStdDev[] = new float[TRTColumns];
		float[] MValue = new float[TRTColumns];
		float TotalM = 0.0f;
		for (int i = 0; i<TRTColumns; i++)
		{
			TrMeanVals[i] = GetColumnMean(TrainedData, i, TRTRows);
			TeMeanVals[i] = GetColumnMean(TestingData, i, TETRows);
			MValue[i] = Math.abs(TrMeanVals[i]-TeMeanVals[i]);//  ((Math.pow((TrMeanVals[i]), 2)) ));
			TotalM+=MValue[i];
			//TrStdDev[i] = GetColumnStdDev(TrainedData, i, TRTRows, TrMeanVals[i]);
			//TeStdDev[i] = GetColumnStdDev(TestingData, i, TETRows, TeMeanVals[i]);
			//if (Math.abs(EValue[i])<E_THRESHOLD)	Counter++;
			//Log.i(TAG, "M value at "+i+" = "+MValue[i]);
		}
		Log.i(TAG, "Total manhattan distance = "+TotalM);
		ManhattanDistance = TotalM;		
	}

	public void CalculateCorrelation()
	{
		int TRTRows = TrainerTemplate.GetTotalRows();
		int TRTColumns = TrainerTemplate.GetTotalColumns();
		int TETRows = TesterTemplate.GetTotalRows();
		float TrMeanVals[] = new float[TRTColumns];
		float TeMeanVals[] = new float[TRTColumns];
		for (int i = 0; i<TRTColumns; i++)
		{
			TrMeanVals[i] = GetColumnMean(TrainedData, i, TRTRows);
			TeMeanVals[i] = GetColumnMean(TestingData, i, TETRows);
		}
		float XBar = GetMean(TrMeanVals);
		float YBar = GetMean(TeMeanVals);
		float Numerator = 0.0f;
		float DXSum = 0.0f;
		float DYSum = 0.0f;
		for (int i = 0; i<TRTColumns; i++)
		{
			Numerator += ((TrMeanVals[i]-XBar)*(TeMeanVals[i]-YBar));
			DXSum += (Math.pow((TrMeanVals[i]-XBar), 2));
			DYSum += (Math.pow((TeMeanVals[i]-YBar), 2));
		}
		
		float CorrVal = (float) (Numerator*1.0f/(Math.sqrt(DXSum*DYSum)));
		Log.i(TAG, "Correlation = "+CorrVal);
		Correlation = CorrVal;		
	}
	
	private float GetMean(float[] Arr)
	{
		float MeanVal = 0.0f;
		float Summation = 0.0f;
		for (int i =0; i<Arr.length; i++)
		{
			Summation += Arr[i];
		}
		MeanVal = Summation*1.0f/Arr.length;
		return MeanVal;
	}

	private void NormalizeData()
	{
		int TRTRows = TrainerTemplate.GetTotalRows();
		int TRTColumns = TrainerTemplate.GetTotalColumns();
		int TETRows = TesterTemplate.GetTotalRows();
		int TETColumns = TesterTemplate.GetTotalColumns();
		for (int i =0; i<TRTColumns; i++)
		{
			float[] CurrentColumn = new float[TRTRows];
			for (int j=0; j<TRTRows; j++)	{	CurrentColumn[j] = TrainedData[j][i];	}
			float Min = GetMin(CurrentColumn);
			float Max = GetMax(CurrentColumn);
			if (Min == Max) Max++;
			for (int j=0; j<TRTRows; j++)	{	TrainedData[j][i] = (CurrentColumn[j]-Min)/(Max-Min);	}
		}
		for (int i =0; i<TETColumns; i++)
		{
			float[] CurrentColumn = new float[TETRows];
			for (int j=0; j<TETRows; j++)	{	CurrentColumn[j] = TestingData[j][i];	}
			float Min = GetMin(CurrentColumn);
			float Max = GetMax(CurrentColumn);
			if (Min == Max) Max++;
			for (int j=0; j<TETRows; j++)	{	TestingData[j][i] = (CurrentColumn[j]-Min)/(Max-Min);	}
		}
	}
	
	private float GetMin(float[] Arr)
	{
		Arrays.sort(Arr);
		return Arr[0];
	}
	private float GetMax(float[] Arr)
	{
		Arrays.sort(Arr);
		return Arr[Arr.length-1];
	}
	public void CalculateZScore()
	{
		int TRTRows = TrainerTemplate.GetTotalRows();
		int TRTColumns = TrainerTemplate.GetTotalColumns();
		int TETRows = TesterTemplate.GetTotalRows();
		int TETColumns = TesterTemplate.GetTotalColumns();

		float TrMeanVals[] = new float[TRTColumns];
		float TeMeanVals[] = new float[TRTColumns];
		float TrStdDev[] = new float[TRTColumns];
		float TeStdDev[] = new float[TRTColumns];
		float[] Z_Value = new float[TRTColumns];
		
		int Counter = 0;
		for (int i = 0; i<TRTColumns; i++)
		{
			
			TrMeanVals[i] = GetColumnMean(TrainedData, i, TRTRows);
			TeMeanVals[i] = GetColumnMean(TestingData, i, TETRows);
			TrStdDev[i] = GetColumnStdDev(TrainedData, i, TRTRows, TrMeanVals[i]);
			TeStdDev[i] = GetColumnStdDev(TestingData, i, TETRows, TeMeanVals[i]);
			float Sigma1ByN1 = (float) ((Math.pow(TrStdDev[i], 2))/TRTRows);
			float Sigma2ByN2 = (float) ((Math.pow(TeStdDev[i], 2))/TETRows);
			Z_Value[i] = (float) ((TrMeanVals[i]-TeMeanVals[i]) / (Math.pow((Sigma1ByN1+Sigma2ByN2), 0.5)));
			if (Math.abs(Z_Value[i])<Z_THRESHOLD)
				Counter++;
			//Log.i(TAG, "Z value at "+i+" = "+Z_Value[i]);
		}
		ZScore = Counter;
	}
	private void CloseFiles()
	{
		TesterFile = null;
		TrainerFile = null;
		TrainerTemplate = null;
		TesterTemplate = null;
	}
	public boolean TesterFileExists()
	{
		Log.i(TAG, "Tester file full path: "+TesterFile.getAbsolutePath());
		return TesterFile.exists();
	}

	public boolean TrainerFileExists()
	{
		return TrainerFile.exists();
	}

	private void ReInitialize()
	{
		CloseFiles();
		InitializeFiles();
	}
	
	private float GetColumnMean(float[][] trainedData, int ColumnNumber, int TotalRows)
	{
		float MeanVal = 0.0f;
		float Summation = 0.0f;
		for (int i=0; i<TotalRows; i++)
		{
			Summation += trainedData[i][ColumnNumber];
		}
		MeanVal = Summation*1.0f/TotalRows;
		return MeanVal;
	}
	
	private float GetColumnStdDev(float Arr[][], int ColumnNumber, int TotalRows, float MeanValue)
	{
		
		float StdDev = 0.0f;
		float Summation = 0.0f;
		for (int i=0; i<TotalRows; i++)
		{
			Summation += Math.pow((Arr[i][ColumnNumber]-MeanValue), 2);
			//Log.i(TAG, Arr[i][ColumnNumber]+"=Arr[i][ColumnNumber] "+MeanValue+" = Meanvalue, "+Summation+" = Summation");
		}
		Summation = Summation/TotalRows;
		//Log.i(TAG, Summation+" = Summation");
		
		StdDev = (float) Math.pow(Summation, 0.5);
		return StdDev;
	}
}
