package com.SwipeData.authentication;

import java.util.ArrayList;
import java.util.Arrays;
/* @author RAJESH KUMAR */
public class SensorsFeatures {
    
    public double[] TimeSeries;
    
    public SensorsFeatures( final double[] TSeries) {
                this.TimeSeries = TSeries; 
        }
    
    public double[] getTimeSeries()
    {
        double[] TimeSeriesCopy=TimeSeries.clone();
        return TimeSeriesCopy;
    }
    public double min() {
    
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        return TimeSeriesCopy[0];
    }   
   
    public double max() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        return TimeSeriesCopy[TimeSeriesCopy.length-1];
    }  
    public double mean() {
        double SUM=0.0;
        for (int i =0; i<TimeSeries.length; i++)
        {
          SUM=SUM+TimeSeries[i];
        }
        return SUM/TimeSeries.length;
    }  
    public double std() {
    
        double MeanOfTimeSeries=mean();
        double SUM=0.0;
        for (int i =0; i<TimeSeries.length; i++)
        {
          SUM=SUM+(TimeSeries[i]-MeanOfTimeSeries)*(TimeSeries[i]-MeanOfTimeSeries);
        }
        double StdTimeSeries= Math.sqrt(SUM/(TimeSeries.length-1));
        return StdTimeSeries;
    }  
    
     public double fQuantile() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        double fQrt=TimeSeriesCopy[TimeSeriesCopy.length/4];
        return fQrt;
    }  
    public double sQuantile() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        double SQrt=TimeSeriesCopy[TimeSeriesCopy.length/2];
        return SQrt;
    }  
    public double thQuantile() {
        double[] TimeSeriesCopy=TimeSeries.clone();
        Arrays.sort(TimeSeriesCopy);
        double ThQrt=TimeSeriesCopy[TimeSeriesCopy.length/4*3];
        return ThQrt;
    }  
    public double energy() {
        double EnergyTimeSeries=0.0;
        double AbsValue;
        for (int i =0; i<TimeSeries.length; i++)
        {
          AbsValue=Math.abs(TimeSeries[i]);
          EnergyTimeSeries=EnergyTimeSeries+(AbsValue*AbsValue);
        }
        return EnergyTimeSeries;
    }  
    public double power() {
    
        double EnergyOfTimeSeries=energy();
        return EnergyOfTimeSeries/TimeSeries.length;
    }  
      
    public int noZeroCrosings() {
    
        int NOZC=0;
        for (int i =0; i<TimeSeries.length-1; i++)
        {   
            if((TimeSeries[i]*TimeSeries[i+1])<0)
            NOZC = NOZC+1;
        }
        return NOZC;
    }  
    
    //ADD THIS FUNCTION
    
    public void smoothenSeries3Points()
    {
      for (int itr=1; itr< TimeSeries.length-1;itr++)
      {
         TimeSeries[itr]=(TimeSeries[itr-1]+TimeSeries[itr]+TimeSeries[itr+1])/3;
      }
    }
       //ADD THIS FUNCTION
    public void smoothenSeries5Points()
    {
      for (int itr=2; itr< TimeSeries.length-2;itr++)
      {
         TimeSeries[itr]=(TimeSeries[itr-2]+TimeSeries[itr-1]+TimeSeries[itr]+TimeSeries[itr+1]+TimeSeries[itr+2])/5;
      }
    }
       //ADD THIS FUNCTION
    public void smoothenSeries7Points()
    {
      for (int itr=3; itr< TimeSeries.length-3;itr++)
      {
         TimeSeries[itr]=(TimeSeries[itr-3]+TimeSeries[itr-2]+TimeSeries[itr-1]+TimeSeries[itr]+TimeSeries[itr+1]+TimeSeries[itr+2]+TimeSeries[itr+3])/5;
      }
    }
       //ADD THIS FUNCTION
    public void removeBeginEndOutliers()
    {
       // System.out.println("Before Outlier REMOVAL"+TimeSeries.length);
        TimeSeries=Arrays.copyOfRange(TimeSeries, 50, TimeSeries.length-80);
        //System.out.println("AFTER Outlier REMOVAL"+TimeSeries.length);
    }
    
       //ADD THIS FUNCTION
    public int length()
    {
      return (TimeSeries.length);
    }
    public ArrayList<Integer> findpeaks() { //Local Peakedness current>prev && current>next
        
        ArrayList<Integer> PeakLocations = new ArrayList<Integer>();
        int NOPeaks=0;
        for (int i =1; i<TimeSeries.length-1; i++)
        {   
          double currentElement=TimeSeries[i];
            if ((currentElement>TimeSeries[i-1])&&(currentElement>TimeSeries[i+1])){
                NOPeaks=NOPeaks+1; 
                PeakLocations.add(i);
            }
        }
        return PeakLocations;
               
    }
    public int nopeaks(){
      ArrayList<Integer> Locations=findpeaks();
        return Locations.size();
    }
  
    public double api() {
        ArrayList<Integer> Locations=findpeaks();
        double AveragePeakInterval, Diff=0.0;
        for(int i=1;i<Locations.size();i++)
        {
          Diff=Diff+(Locations.get(i)-Locations.get(i-1));
        }
        AveragePeakInterval=Diff/(Locations.size()-1); 
        return AveragePeakInterval;
    }
//We use the following (unbiased) formula to define skewness:
// skewness = [n / (n -1) (n - 2)] sum[(x_i - mean)^3] / std^3 
     public double skewness(){
      double N;
        N = TimeSeries.length;
      double SkewnessOfSeries;
      double Mean=mean();
      double Std=std();
      double SUM=0.0;
      for (int i =0; i<TimeSeries.length; i++)
      {
       SUM=SUM+Math.pow((TimeSeries[i]-Mean),3);
      }
      SkewnessOfSeries= (N/((N-1)*(N-2)))*SUM/(Math.pow(Std,3));
      return SkewnessOfSeries;
      
    }
  //   kurtosis = { [n(n+1) / (n -1)(n - 2)(n-3)] sum[(x_i - mean)^4] / std^4 } - [3(n-1)^2 / (n-2)(n-3)]
    
   public double kurtosis(){
      double N;
      N = TimeSeries.length;
      double KurtosisOfSeries;
      double MeanOfSeries=mean();
      double StdOfSeries=std();
      double SUM=0.0,Part1, Part2, Part3;
      for (int i =0; i<TimeSeries.length; i++)
      {
        SUM=SUM+Math.pow((TimeSeries[i]-MeanOfSeries),4);
      }
      Part1=(N*(N+1))/((N-1)*(N-2)*(N-3));
    
      Part2=SUM/Math.pow(StdOfSeries, 4);
    
      Part3=((Math.pow((N-1),2))*3)/((N-2)*(N-3));
 
      KurtosisOfSeries= Part1*Part2-Part3;
      
      return KurtosisOfSeries;
      
    }
   
    public ArrayList<Double> getAllTheFeatures()
    {
     ArrayList<Double> Features = new ArrayList<Double>();
     Features.add(api());
     Features.add(energy());
     Features.add(fQuantile());
     Features.add(kurtosis());
     Features.add(max());
     Features.add(mean());
     Features.add(min());
     Features.add((double)nopeaks());
     Features.add((double)noZeroCrosings());
     Features.add(power());
     Features.add(skewness());
     Features.add(sQuantile());
     Features.add(std());
     Features.add(thQuantile());
     return Features;
    }
    
//    public static void main(String[] args) 
//    {
//        double [] TSeriesX={-0.6703765,2.0685902,9.921572,0.0474706,-0.115484364,-0.9607817,-0.35043433,-0.27359867,0.5239525,0.10095213,0.5072818,0.45239422,0.2406809,-0.14660765,-0.1160644};
//        Features FeatureX =new Features(TSeriesX);
//        System.out.println("api :" +FeatureX.api());
//        System.out.println("Kurtosis :" +FeatureX.kurtosis());
//       
//      
//    }
}
