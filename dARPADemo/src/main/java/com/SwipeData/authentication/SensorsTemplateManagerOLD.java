/* TemplateCreator  */
/*** @author RAJESH */
package com.SwipeData.authentication;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import com.SwipeData.generalobjects.ArffCreator;

import android.os.Environment;
import android.util.Log;
public class SensorsTemplateManagerOLD {
private static final String	TAG	= null;
private File RawDataFile;
private File FeatureFile;
private File ArffFile;
boolean Training;
private final int WindowSize,SlidingInterval;
private double FeatureMatrix[][];
private String DataFolder = "";
private File root;
private double XSeries[],YSeries[],ZSeries[];
private final int TotalFeatures=56;
private String Username = "";
private String [][]AttributesList={
		{"apiX", "numeric"}, {"energyX", "numeric"} ,{"fQuantileX", "numeric"} ,{"kurtosisX", "numeric"} ,{"maxX", "numeric"} ,{"meanX", "numeric"},{"minX", "numeric"},
		{"nopeaksX", "numeric"} ,{"noZeroCrosingsX", "numeric"} ,{"powerX", "numeric"} ,{"skewnessX", "numeric"} ,{"sQuantileX", "numeric"},{"stdX", "numeric"},{"thQuantileX", "numeric"},
		{"apiY", "numeric"}, {"energyY", "numeric"} ,{"fQuantileY", "numeric"} ,{"kurtosisY", "numeric"} ,{"maxY", "numeric"} ,{"meanY", "numeric"},{"minY", "numeric"},
		{"nopeaksY", "numeric"} ,{"noZeroCrosingsY", "numeric"} ,{"powerY", "numeric"} ,{"skewnessY", "numeric"} ,{"sQuantileY", "numeric"},{"stdY", "numeric"},{"thQuantileY", "numeric"},
		{"apiZ", "numeric"}, {"energyZ", "numeric"} ,{"fQuantileZ", "numeric"} ,{"kurtosisZ", "numeric"} ,{"maxZ", "numeric"} ,{"meanZ", "numeric"},{"minZ", "numeric"},
		{"nopeaksZ", "numeric"} ,{"noZeroCrosingsZ", "numeric"} ,{"powerZ", "numeric"} ,{"skewnessZ", "numeric"} ,{"sQuantileZ", "numeric"},{"stdZ", "numeric"},{"thQuantileZ", "numeric"},
		{"apiM", "numeric"}, {"energyM", "numeric"} ,{"fQuantileM", "numeric"} ,{"kurtosisM", "numeric"} ,{"maxM", "numeric"} ,{"meanM", "numeric"},{"minM", "numeric"},
		{"nopeaksM", "numeric"} ,{"noZeroCrosingsM", "numeric"} ,{"powerM", "numeric"} ,{"skewnessM", "numeric"} ,{"sQuantileM", "numeric"},{"stdM", "numeric"},{"thQuantileM", "numeric"},
		
		{"class", "{Genuine,Imposter}"}};
//    
   public SensorsTemplateManagerOLD(File DataFile, String DF, String UN,boolean TrainingOrTest)
    {
  //	 Log.i(TAG, "Template manager is being created");
  	 this.RawDataFile=DataFile;
  	 this.Username = UN;
  	
  	 if(TrainingOrTest)
  	 {
  		 this.DataFolder = DF;
    	 root = new File(Environment.getExternalStorageDirectory(), DataFolder);
    	 this.Training=TrainingOrTest;
  		 FeatureFile= new File(root,"Sensors/"+Username+"/"+"TrainingFeatureFile.txt");
  	//	 Log.i(TAG,"Created a name for the Training Feature File  :"+ FeatureFile.getAbsolutePath());
  		 
  		 ArffFile=new File(root, "Sensors/"+Username+"/"+"Training.arff");
  //		 Log.i(TAG,"Created a name for the Training Feature File  :"+ ArffFile.getAbsolutePath() );
  	 }
  	 else
  	 {
  		 this.DataFolder = DF;
    	 root = new File(Environment.getExternalStorageDirectory(), DataFolder);
    	 this.Training=TrainingOrTest;
  		 FeatureFile= new File(root,"Sensors/"+Username+"/"+"TestingFeatureFile.txt");
  	//	 Log.i(TAG,"Created a name for the Training Feature File  :"+ FeatureFile.getAbsolutePath());
  		 
  		 ArffFile=new File(root, "Sensors/"+Username+"/"+"Testing.arff");
  	//	 Log.i(TAG,"Created a name for the Testing Feature File  :"+ ArffFile.getAbsolutePath() );
  	 }
     
  	 // Intializing the Size of Sliding Window and the Interval
     WindowSize=200;SlidingInterval=60;
        
    }
     
    public static double [] computeMagnitude(double[]X, double []Y, double []Z)
     {
      double M[]= new double[X.length];
      for (int itr=0;itr<X.length;itr++)
      {
          M[itr]=Math.sqrt(X[itr]*X[itr]+Y[itr]*Y[itr]+Z[itr]*Z[itr]);
      }
           return M; 
     }
    
    public void intializeTheDataSeriesForTraining() throws FileNotFoundException, IOException
    {
    	int xColIndex=1,yColIndex=2,zColIndex=3;
    	
    	SensorFileManager FileObject = new SensorFileManager(RawDataFile, DataFolder+"/Sensors/"+Username);
    	
    	int totalRowsInFile=FileObject.getNumberOfTotalSamples();
    	XSeries = FileObject.txtReadWindow(xColIndex, 1, totalRowsInFile);
    	YSeries = FileObject.txtReadWindow(yColIndex, 1, totalRowsInFile);
    	ZSeries = FileObject.txtReadWindow(zColIndex, 1, totalRowsInFile);
    	Log.i(TAG, "Size of the Series for Training Feature Extraction "+XSeries.length);
  
    }
    
    public void intializeTheDataSeriesForTesting() throws FileNotFoundException, IOException
    {
    	int xColIndex=1,yColIndex=2,zColIndex=3;
    	int NumberOfSamplesNeeded=1000;
    	SensorFileManager FileObject = new SensorFileManager(RawDataFile, DataFolder+"/Sensors/"+Username);
    	int totalRowsInFile=FileObject.getNumberOfTotalSamples();
      int StartingPoint=totalRowsInFile-NumberOfSamplesNeeded;
  
    	XSeries = FileObject.txtReadWindow(xColIndex, 1, totalRowsInFile);
    	YSeries = FileObject.txtReadWindow(yColIndex, 1, totalRowsInFile);
    	ZSeries = FileObject.txtReadWindow(zColIndex, 1, totalRowsInFile);
    	StartingPoint=XSeries.length-NumberOfSamplesNeeded;
    	XSeries=Arrays.copyOfRange(XSeries, StartingPoint,XSeries.length);
    	YSeries=Arrays.copyOfRange(YSeries, StartingPoint,YSeries.length);
    	ZSeries=Arrays.copyOfRange(ZSeries, StartingPoint,ZSeries.length);
    	
    	Log.i(TAG, "Size of the Series for Testing Feature Extraction "+XSeries.length);
    }

    public void extractFeatures() throws IOException
    {
        //Set the Column Index for X Y and Z according to the Data File
        int RowIndex=0,NumOfRows, FeatureCounter;
        int WindowCounter=1,StartWIndex,EndWIndex;
       // Log.i(TAG,"Etracting Feature from the File : "+RawDataFile.getAbsolutePath());
        if(Training)
        {
        	intializeTheDataSeriesForTraining();
        }
        else
        {
        	
        	intializeTheDataSeriesForTesting();
        }
        
        FeatureManager SeriesX = new FeatureManager(XSeries);
        FeatureManager SeriesY = new FeatureManager(YSeries);
        FeatureManager SeriesZ = new FeatureManager(ZSeries);
        if(Training)
        {
        	SeriesX.removeOutliersFromBeginAndEnd();
        	SeriesY.removeOutliersFromBeginAndEnd();
        	SeriesZ.removeOutliersFromBeginAndEnd();
        }
        else
        {
        	SeriesX.removeOnlyBeginingOutliers();
        	SeriesY.removeOnlyBeginingOutliers();
        	SeriesZ.removeOnlyBeginingOutliers();
        }
        
        double Magnitude[] = computeMagnitude(SeriesX.getTimeSeries(),SeriesY.getTimeSeries(),SeriesZ.getTimeSeries());
        FeatureManager SeriesM= new FeatureManager(Magnitude);
      
        SeriesX.smoothenSeries3Points();
        SeriesY.smoothenSeries3Points();
        SeriesZ.smoothenSeries3Points();
        SeriesM.smoothenSeries3Points();
        
        int totalRowsToBeProcessed=SeriesX.getLength();
        
        if(totalRowsToBeProcessed%SlidingInterval==0) NumOfRows=totalRowsToBeProcessed/SlidingInterval;
        else NumOfRows=(totalRowsToBeProcessed/SlidingInterval)+1;
        
        FeatureMatrix= new double [NumOfRows][TotalFeatures];
       // System.out.println("Feature Matrix Defined ----With the following Size ");
      //  System.out.println("TemplateManager(Extract Feature)--Size of the Feature Matrix - ROws: "+FeatureMatrix.length+ "and Columns"+FeatureMatrix[0].length);
       
        for ( StartWIndex=0;StartWIndex<totalRowsToBeProcessed;StartWIndex=StartWIndex+SlidingInterval)
        { 
              // Determining endWIndex and making sure that last window contains all
              // remaining rows in case there is less than 200 rows
              if ((StartWIndex+WindowSize)<totalRowsToBeProcessed) EndWIndex=StartWIndex+WindowSize-1;
              else EndWIndex=totalRowsToBeProcessed;
              FeatureCounter=0;
              // Displaying the current WindowNo, its startIndex and EndIndex
             // System.out.println("Extract Features for the Window = " + WindowCounter);  
             // System.out.println("startWIndex = "+ StartWIndex + " endWIndex = " + EndWIndex); 
              int NOEWindow=EndWIndex-StartWIndex;
            //  System.out.println("Number Of Elements in Window:"+NOEWindow);
              double WindowX[] = new double[NOEWindow];
              double WindowY[] = new double[NOEWindow];
              double WindowZ[] = new double[NOEWindow];
              double WindowM[] = new double[NOEWindow];
              
              for (int index=StartWIndex, itr=0;index<EndWIndex;)
                {
                    WindowX[itr]=SeriesX.getTimeSeries()[index];
                    WindowY[itr]=SeriesY.getTimeSeries()[index];
                    WindowZ[itr]=SeriesZ.getTimeSeries()[index];
                    WindowM[itr++]=SeriesM.getTimeSeries()[index++];
                }
              FeatureManager FeaturesFromX = new FeatureManager(WindowX);
              FeatureManager FeaturesFromY = new FeatureManager(WindowY);
              FeatureManager FeaturesFromZ = new FeatureManager(WindowZ);
              FeatureManager FeaturesFromM = new FeatureManager(WindowM);
            
              for (int XItr=0;XItr<FeaturesFromX.getNumberOfFeaturesPerSeries();XItr++)
              {
                 FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromX.getAllTheFeatures()[XItr];
              }
                           
              for (int YItr=0;YItr<FeaturesFromY.getNumberOfFeaturesPerSeries();YItr++)
              {
                 FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromY.getAllTheFeatures()[YItr];
              }
             
           
              for (int ZItr=0;ZItr<FeaturesFromZ.getNumberOfFeaturesPerSeries();ZItr++)
              {
                 FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromZ.getAllTheFeatures()[ZItr];
              }
             
            
              for (int MItr=0;MItr<FeaturesFromM.getNumberOfFeaturesPerSeries();MItr++)
              {
                 FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromM.getAllTheFeatures()[MItr];
                 
              }
              RowIndex++;
              WindowCounter=WindowCounter+1;
        }
  
      
   }

   public void normalizeFeatures() throws IOException
    {
        int NumberOfRows;
        NumberOfRows=FeatureMatrix.length;
        double TempArray[]= new double[NumberOfRows];
        double Min, Max, Differences;
        int NumberOfFeaturesToBeNormalized=55;
       for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
        {
          for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
          {
            TempArray[VectorItr]=FeatureMatrix[VectorItr][FeatureItr];
          }
            Arrays.sort(TempArray);
            Min=TempArray[0];Max=TempArray[TempArray.length-1];
            Differences=Max-Min;
           
                
          for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
          {
            
              if(Differences==0)
                FeatureMatrix[VectorItr][FeatureItr]= FeatureMatrix[VectorItr][FeatureItr]-Min;
              else 
                FeatureMatrix[VectorItr][FeatureItr]= (FeatureMatrix[VectorItr][FeatureItr]-Min)/Differences;
          }
                      
        }
       
       for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
       {
      	 for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
         {
      		 // if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]) || FeatureMatrix[VectorItr][FeatureItr]==0.0)
      		 if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]))
      		  	{
      		  	   FeatureMatrix[VectorItr][FeatureItr]=0.0025;
      		  	   Log.i(TAG, "Found NaN or 0.0 here And replacing with 0.0025 : "+FeatureMatrix[VectorItr][FeatureItr]);
      		  	}
      		  
      		 
         }
       }
     
  } 
   
  //REPLACE THIS AS WELL 
   public void createFeatureFile() throws IOException 
   {  
     
     extractFeatures(); //Extracting Features and Intializing FeatureMatrix
     Log.i(TAG,"NormaliZing the Features After Creating Feature Matrix");
     normalizeFeatures(); //Normalize the features 
 //    Log.i(TAG,"Writing to File  "+FeatureFile.getAbsolutePath() +" and Columns="+FeatureMatrix[0].length+" Rows="+FeatureMatrix.length);
     SensorFileManager FileWriter = new SensorFileManager(FeatureFile, DataFolder+"/Sensors/"+Username);
  	 FileWriter.writeMatrixIntoFile(FeatureMatrix);
 //    Log.i(TAG, "Feature File "+FeatureFile.getAbsolutePath()+ "is Created succesfully ");
  }
   
   public void createArffFile() throws Exception 
   {
  	 //Creating the feature matrix from the raw data taking 400 samples each time.
  	 createFeatureFile();
  	// ArffCreator ArffCreatorObject = new ArffCreator(ArffFile, Training);
  	 ArffCreator ArffCreatorObject = new ArffCreator(ArffFile, false);
  	 ArffCreatorObject.SetRelation(ArffFile.getName().replace(".", ""));
  	
  	 ArffCreatorObject.SetAttributes(AttributesList);
  	 ArffCreatorObject.SetData(FeatureMatrix);
  	 Log.i(TAG, "Creating arff file here: "+ArffFile.getAbsolutePath());
  	 if (Training)
  	 {
  		 // Append the Imposters
  		
  		 ArffCreatorObject.appendImposter();
  		 
  	 }
  	 
   }
}
