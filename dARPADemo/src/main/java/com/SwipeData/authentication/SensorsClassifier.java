package com.SwipeData.authentication;


import java.io.IOException;
import java.util.ArrayList;


public class SensorsClassifier {
    public String TrainingTemplateFile;
    public String TestingTemplateFile;
    private int FinalScore=0;
    private int Threshold = 45;
    
    public SensorsClassifier(String TestFileName, String TrainFileName)
    {
      TestingTemplateFile=TestFileName;   
      TrainingTemplateFile=TrainFileName;
    //  Log.i(TAG, "Testing file is: "+TestingTemplateFile+" and training is: "+TrainingTemplateFile);
    }
    public int calcluateScore() throws IOException
    {
      int Score=0;
      double ZScoreThreshold=1.96;
      double Numerator, Denominator;
      SensorsDataFile TraingFileObj = new SensorsDataFile(TrainingTemplateFile);
      SensorsDataFile TestingFileObj = new SensorsDataFile(TestingTemplateFile);
      ArrayList<Double> TrainMeanList,TrainStdList,TrainNumList,ZScore= new ArrayList<Double>();
      ArrayList<Double> TestMeanList,TestStdList,TestNumList;
      TrainMeanList=TraingFileObj.txtRead(1);
      TrainStdList=TraingFileObj.txtRead(2);
      TrainNumList=TraingFileObj.txtRead(3);
      TestMeanList=TestingFileObj.txtRead(1);
      TestStdList=TestingFileObj.txtRead(2);
      TestNumList=TestingFileObj.txtRead(3);
      for (int itr=0; itr<TrainMeanList.size();itr++)
      {
         Numerator=TrainMeanList.get(itr)- TestMeanList.get(itr);
         Denominator=Math.sqrt((TrainStdList.get(itr)*TrainStdList.get(itr))/TrainNumList.get(itr)+(TestStdList.get(itr)*TestStdList.get(itr))/TestNumList.get(itr));
         ZScore.add(Numerator/Denominator);
      }
      
      for (int itr=0; itr<TrainMeanList.size();itr++)
      {
         //Check The Z Score Confidence 
          if(ZScore.get(itr)>=-ZScoreThreshold && ZScore.get(itr)<=ZScoreThreshold )
              Score++;
      }
      return Score;
   }
 
   public boolean AuthenticationDecision() throws IOException
    {
        FinalScore=calcluateScore();
        System.out.println("Score of "+TestingTemplateFile.substring(0, TestingTemplateFile.length()-4)+ " Against to "+TrainingTemplateFile.substring(0, TrainingTemplateFile.length()-4)+" is : "+FinalScore);
        return FinalScore>Threshold;
    
    }
   
   public int GetFinalScore()
   {
  	 try
  	 {
  		 FinalScore=calcluateScore();
  	 } 
  	 catch (IOException e)	{	e.printStackTrace();}
  	 return FinalScore;
   }
}
