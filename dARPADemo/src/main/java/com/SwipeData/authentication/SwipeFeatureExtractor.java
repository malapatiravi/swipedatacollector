package com.SwipeData.authentication;


import java.util.ArrayList;
import java.util.Arrays;
import com.SwipeData.generalobjects.ArffCreator;
import com.SwipeData.generalobjects.ExcelReader;
import com.SwipeData.generalobjects.FileWriterObject;

import android.util.Log;

public class SwipeFeatureExtractor
{

	private String DataFilename = "";
	private String DataFileLocation = "";
	private int NumberOfFeatures = 0;
	private ExcelReader ExcelFile;
	private int TotalRows;
	private int TotalActualRows;
	private int TotalColumns;
	private String[][] CompleteData; 
	private String TAG = "FE";
	private Stroke AllStrokes;
	private SwipeFeatures FeatureCollection;
	private int IndexOfActionType = 1;
	private int IndexOfX = 3;
	private int IndexOfY = 4;
	private int IndexOfPressure  = 5;
	private int IndexOfArea = 6;
	private int IndexOfTime = 7;
	private String[][] FinalData;
	private int DIST_THRESHOLD = 3;//To remove long press outliers
	private boolean[] IgnoreLine;
	private String[][] AllFeatureLevelValues;
	SwipeFeatureExtractor()
	{
		
	}
	public SwipeFeatureExtractor(int Features, String Filename, String FileLocation)
	{
		NumberOfFeatures = Features;
		DataFilename = Filename;
		DataFileLocation = FileLocation;
	//	Log.i(TAG, "Creating CVS reader object");
		try
		{
//			Log.i(TAG, "CVS reader object creating with"+DataFileLocation+" and filename="+DataFilename);
			ExcelFile = new ExcelReader(DataFilename, DataFileLocation, 0, IndexOfTime);
			TotalRows = ExcelFile.GetTotalRows();
			//	Log.i(TAG, "Total rows: "+TotalRows);
			TotalColumns = ExcelFile.GetTotalColumns();
			//	Log.i(TAG, "Total rows: "+TotalColumns);
			CompleteData = new String[TotalRows][TotalColumns];
			CompleteData = ExcelFile.GetData();
			ExcelFile.CloseFile();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		ArrayList< ArrayList<String> > TemporaryData = RemoveOutliers();
		CopyToFinalData(TemporaryData);
		SaveFinalData();
		//		PrintArrayList(TemporaryData);
//		RemoveOutliers();
//		TotalActualRows = CountProperLines();
		Log.i(TAG, "TotalAcutalRows = "+TotalActualRows);
//		TotalActualRows = RemoveLongPressOutliers();
//		Log.i(TAG, "TotalAcutalRows = "+TotalActualRows);
//		IgnoreLine = new boolean[TotalActualRows];
//		RemoveLongPressOutliers();
//		Arrays.deepToString(TemporaryData);
		
		Log.i(TAG, "Total Strokes = "+GetTotalStrokes());
//		AllStrokes = new Stroke(GetTotalStrokes(), GetMaxLinesPerStroke());
		AllStrokes = new Stroke();
//		int[] Samp = CountEachSamples();
		SaveStrokeData();
//		CountNumberOfStrokes(Samp);
		ExtractFeatures();
		int TotalFeatureRows = FeatureCollection.GetNumberOfStrokes();
		AllFeatureLevelValues = new String[TotalFeatureRows][NumberOfFeatures];
		AllFeatureLevelValues = FeatureValsToArry();
		//SaveAsArrf();
	}
	

	void CopyToFinalData(ArrayList< ArrayList<String> > Temp)
	{
		FinalData = new String[Temp.size()][Temp.get(0).size()];
		for (int i =0; i<Temp.size(); i++)
		{
			for (int j =0; j < Temp.get(0).size(); j++)
			{
				FinalData[i][j] = Temp.get(i).get(j);
			}
			//Log.i(TAG, "Final data at "+i+" : "+Arrays.toString(FinalData[i]));
		}
	}
	void PrintArrayList(ArrayList< ArrayList<String> > Temp)
	{
		String PrintVal = "";
		for (int i =0; i<Temp.size(); i++)
		{
			for (int j =0; j < Temp.get(0).size(); j++)
			{
				PrintVal += Temp.get(i).get(j)+",";
			}
			Log.i(TAG, PrintVal);
			PrintVal = "";
		}
	}
	
	float GetDistance(float X1, float Y1, float X2, float Y2)
	{
		float Distance = 0.0f;
		Distance = (float) Math.sqrt(Math.pow(X2-X1, 2)+(Math.pow(Y2-Y1, 2)));
		return Distance;
	}
	
	ArrayList< ArrayList<String> > RemoveOutliers()
	{
		ArrayList< ArrayList<String> > TemporaryData = new ArrayList< ArrayList<String>>();
		float CurrentX = 0.0f;
		float CurrentY = 0.0f;
		float PrevX = Float.parseFloat(CompleteData[0][IndexOfX]);
		float PrevY = Float.parseFloat(CompleteData[0][IndexOfY]);
		int ActionType = 0;
		int RowCounter = 0;
		boolean HaveInitPoint = false;
		for (int i =0; i<(TotalRows-1); i++)
		{
			if (!(ActionType==7 || ActionType==9 || ActionType==10))
			{
				CurrentX = Float.parseFloat(CompleteData[i+1][IndexOfX]);
				CurrentY = Float.parseFloat(CompleteData[i+1][IndexOfY]);
//				Log.i(TAG, "CurrentX = "+CurrentX+", CurrentY = "+CurrentY+" PrevX="+PrevX+" PrevY="+PrevY+", ActionType="+ActionType);
				ActionType = Integer.parseInt(CompleteData[i+1][IndexOfActionType]);
				
				if (ActionType==1)
				{
					HaveInitPoint = false;
//					Log.i(TAG, "RowCounter = "+RowCounter+"i = "+i);
					TemporaryData.add(RowCounter, new ArrayList<String>());
					for (int j =0; j<TotalColumns; j++) TemporaryData.get(RowCounter).add(CompleteData[i+1][j]);
//					PrevX = Float.parseFloat(CompleteData[i+2][IndexOfX]);
					PrevX = (((i+2)>(TotalRows-1))?0:Float.parseFloat(CompleteData[i+2][IndexOfX]));
					PrevY = (((i+2)>(TotalRows-1))?0:Float.parseFloat(CompleteData[i+2][IndexOfY]));
					i++;
					RowCounter++;
//					Log.i(TAG, "RowCounter = "+RowCounter+", size = "+TemporaryData.size()+" data ="+Arrays.toString(TemporaryData.get(RowCounter-1).toArray()));
				}
				else if ((GetDistance(CurrentX, CurrentY, PrevX, PrevY) > DIST_THRESHOLD))
				{
					if (!HaveInitPoint)
					{
						HaveInitPoint = true;
						CompleteData[i][IndexOfActionType]="0";						
					}
//					Log.i(TAG, "Top RowCounter = "+RowCounter+", i = "+i);
					TemporaryData.add(RowCounter, new ArrayList<String>());
					for (int j =0; j<TotalColumns; j++) 
					{
						TemporaryData.get(RowCounter).add(CompleteData[i][j]);
					}
					PrevX = CurrentX;
					PrevY = CurrentY;
					RowCounter++;
//					Log.i(TAG, "RowCounter = "+RowCounter+", size = "+TemporaryData.size()+" data ="+Arrays.toString(TemporaryData.get(RowCounter-1).toArray()));
				}
			}
		}
		TotalActualRows = RowCounter;
		return TemporaryData;
	}
	int GetMaxLinesPerStroke()
	{
		int MaxLines = 0;
		int CurrentTotalLines = 0;
		for (int i = 0; i<TotalActualRows; i++)
		{
			CurrentTotalLines++;
			if (Integer.parseInt(CompleteData[i][IndexOfActionType])==1)
			{
				if (MaxLines < CurrentTotalLines)
					MaxLines = CurrentTotalLines+1;
				CurrentTotalLines = 0;
			}
		}
		Log.i(TAG, "Max line per stroke is ="+MaxLines);
		return MaxLines;
	}
	int CountProperLines()
	{
//		Log.i(TAG, Arrays.deepToString(CompleteData));
		int ProperRows = 0;
		for (int i = 0; i < TotalRows; i++)
		{
			//Log.i(TAG, Arrays.toString(CompleteData[i])+" and i = "+i + " and total rows="+TotalRows);
			if (!(Integer.parseInt(CompleteData[i][IndexOfActionType])==7 || Integer.parseInt(CompleteData[i][IndexOfActionType])==9 || Integer.parseInt(CompleteData[i][IndexOfActionType])==10))
			{
				ProperRows++;
			}
		}		
		return ProperRows;
	}
	
	int RemoveLongPressOutliers()
	{
		float PrevX = 0.0f;
		float PrevY = 0.0f;
		int NewSize = 0;
		for (int i = 0; i < TotalActualRows; i++)
		{
			float XVal = Float.parseFloat(CompleteData[i][IndexOfX]);
			float YVal = Float.parseFloat(CompleteData[i][IndexOfY]);
			float Distance = (float) Math.sqrt(Math.pow(XVal-PrevX, 2)+(Math.pow(YVal-PrevY, 2)));
			//Log.i(TAG, "Distance = "+Distance+", XVal="+XVal+", YVal="+YVal+", PrevX="+PrevX+", PrevY="+PrevY);
			if ((Distance > DIST_THRESHOLD) || (Integer.parseInt(CompleteData[i][IndexOfActionType])==1) || (Integer.parseInt(CompleteData[i][IndexOfActionType])==0))
			{
				NewSize++;
				PrevX = XVal;
				PrevY = YVal;
			}
		}
		return NewSize;
	}
	
	void SaveFinalData()
	{
		FileWriterObject FilteredData = new FileWriterObject("FilteredData.txt", DataFileLocation);
		for (int i=0; i<TotalActualRows; i++)
		{
			FilteredData.SaveData(Arrays.toString(FinalData[i]));
		}
		FilteredData.CloseWriter();
		
	}
	private String[][] GetFeatureAndTypeRelation()
	{
		String[][] Attribs  = new String[NumberOfFeatures][2];
		Attribs[0][0]  = "BeginX"; 							Attribs[0][1]  = "NUMERIC";
		Attribs[1][0]  = "EndX"; 								Attribs[1][1]  = "NUMERIC";
		Attribs[2][0]  = "BeginY"; 							Attribs[2][1]  = "NUMERIC";
		Attribs[3][0]  = "EndY"; 								Attribs[3][1]  = "NUMERIC";
		Attribs[4][0]  = "Distance"; 						Attribs[4][1]  = "NUMERIC";
		Attribs[5][0]  = "AvgDist";							Attribs[5][1]  = "NUMERIC";
		Attribs[6][0]  = "Angle1"; 							Attribs[6][1]  = "NUMERIC";
		Attribs[7][0]  = "Time"; 								Attribs[7][1]  = "NUMERIC";
		Attribs[8][0]  = "MeanEV"; 							Attribs[8][1]  = "NUMERIC";
		Attribs[9][0]  = "StdDevEV"; 						Attribs[9][1]  = "NUMERIC";
		Attribs[10][0] = "QuantileEV25"; 				Attribs[10][1] = "NUMERIC";
		Attribs[11][0] = "QuantileEV50"; 				Attribs[11][1] = "NUMERIC";
		Attribs[12][0] = "QuantileEV75"; 				Attribs[12][1] = "NUMERIC";
		Attribs[13][0] = "MeanEA"; 							Attribs[13][1] = "NUMERIC";
		Attribs[14][0] = "StdDevEA"; 						Attribs[14][1] = "NUMERIC";
		Attribs[15][0] = "QuantileEA25"; 				Attribs[15][1] = "NUMERIC";
		Attribs[16][0] = "QuantileEA50"; 				Attribs[16][1] = "NUMERIC";
		Attribs[17][0] = "QuantileEA75"; 				Attribs[17][1] = "NUMERIC";
		Attribs[18][0] = "MeanPressure"; 				Attribs[18][1] = "NUMERIC";
		Attribs[19][0] = "PressureStdDev"; 			Attribs[19][1] = "NUMERIC";
		Attribs[20][0] = "PressureQuantile25"; 	Attribs[20][1] = "NUMERIC";
		Attribs[21][0] = "PressureQuantile50"; 	Attribs[21][1] = "NUMERIC";
		Attribs[22][0] = "PressureQuantile75"; 	Attribs[22][1] = "NUMERIC";
//		Attribs[23][0] = "class"; 							Attribs[23][1] = "{Genuine,Imposter}";
		return Attribs;
	}
	void SaveAsArrf()
	{
		ArffCreator ArrfFileObj = new ArffCreator("ArrfData.arrf", DataFileLocation);
		String Rel = "Complete Swipe Data";
		ArrfFileObj.SetRelation(Rel);
		String[][] Attribs  = new String[NumberOfFeatures][2];
		Attribs = GetFeatureAndTypeRelation();
		try
		{
			ArrfFileObj.SetAttributes(Attribs);
			ArrfFileObj.SetData(AllFeatureLevelValues);
		}
		catch (Exception e)	{	e.printStackTrace();}
		ArrfFileObj.Close();
	}
	void RemoveInitialOutliers()
	{
		int NewDataSize = 0;
		float PrevX = 0.0f;
		float PrevY = 0.0f;
		float XVal = 0.0f;
		float YVal = 0.0f;
		float Distance = 0.0f;
		boolean SavedCurrentPoints = false;
		String[] LastData = new String[TotalColumns];
		for (int j = 0; j < TotalColumns; j++)	LastData[j] = CompleteData[0][j];
		FinalData = new String[TotalActualRows][TotalColumns];
		for (int i = 0; i < TotalRows; i++)
		{
			if (!(Integer.parseInt(CompleteData[i][IndexOfActionType])==7 || Integer.parseInt(CompleteData[i][IndexOfActionType])==9 || Integer.parseInt(CompleteData[i][IndexOfActionType])==10))
			{
				XVal = Float.parseFloat(CompleteData[i][IndexOfX]);
				YVal = Float.parseFloat(CompleteData[i][IndexOfY]);
				Distance = (float) Math.sqrt(Math.pow(XVal-PrevX, 2)+(Math.pow(YVal-PrevY, 2)));
				if ((Distance > DIST_THRESHOLD) || (Integer.parseInt(CompleteData[i][IndexOfActionType])==1) || (Integer.parseInt(CompleteData[i][IndexOfActionType])==0))
				{
					//Log.i(TAG, Arrays.toString(CompleteData[i])+" i="+NewDataSize);
					for (int j = 0; j < TotalColumns; j++)	
					{
						FinalData[NewDataSize][j] = (i==0)?LastData[j]:CompleteData[i-1][j];
						LastData[j] = CompleteData[i][j];//FinalData[NewDataSize][j] = CompleteData[i][j];
					}
					NewDataSize++;
					PrevX = XVal;
					PrevY = YVal;
				}
			}
		}
		//Log.i(TAG, "Total considered rows = " + NewDataSize+" out of "+TotalRows);
	}
	
	int GetTotalStrokes()
	{
		int TotalStrokes = 0;
		boolean StrokeClosed = true;
		for (int i = 0; i < TotalActualRows; i++)
		{
			//Log.i(TAG, "i = "+i+" data = "+Arrays.toString(FinalData[i]));
			if (Integer.parseInt(FinalData[i][IndexOfActionType])==0)
			{
				StrokeClosed = false;
			}
			else if (Integer.parseInt(FinalData[i][IndexOfActionType])==1)
			{
				TotalStrokes++;
				StrokeClosed = true;
			}
		}
		if (!StrokeClosed)
			TotalStrokes++;
		return TotalStrokes;
	}
	public SwipeFeatures GetFeatureVar()
	{
		return FeatureCollection;
	}
	
	public int GetTotalStrokesFromObject()
	{
		return AllStrokes.GetTotalStrokes();
	}
	private void SaveStrokeData()
	{
		int Count = 0;
		int CurrentLines = 0;
		for (int i = 0; i < TotalActualRows; i++)
		{
			float[] KValues = new float[5];
			KValues[0] = Float.parseFloat(FinalData[i][IndexOfX]);
			KValues[1] = Float.parseFloat(FinalData[i][IndexOfY]);
			KValues[2] = Float.parseFloat(FinalData[i][IndexOfPressure]);
			KValues[3] = Float.parseFloat(FinalData[i][IndexOfArea]);
			KValues[4] = Float.parseFloat(FinalData[i][IndexOfTime]);
			//Log.i(TAG, "Kvalues = "+Arrays.toString(KValues)+" and i ="+i);
			AllStrokes.StoreValue(Count, CurrentLines, KValues);
//			Log.i(TAG, "Count = "+Count+" CurrentLines ="+CurrentLines+" and storedValues = "+AllStrokes.GetKValueAt(Count, CurrentLines, 0)+", "+AllStrokes.GetKValueAt(Count, CurrentLines, 1)+", "+AllStrokes.GetKValueAt(Count, CurrentLines, 2)+", "+AllStrokes.GetKValueAt(Count, CurrentLines, 3)+", "+AllStrokes.GetKValueAt(Count, CurrentLines, 4));
			CurrentLines++;
			if ((Integer.parseInt(FinalData[i][IndexOfActionType])==1) || (i==(TotalActualRows-1)))
			{
				CurrentLines = 0;
				Count++;
			}
		}
	}
/*	
	private void CountNumberOfStrokes(int[] Samp)
	{
		int StrokeCounter = 0;
		int Count = 0;
		int Stnum = 0;
			Count = 0;
			Stnum = 0;
			int Start = Samp[i];
			while (Start+Count < Samp[i+1])
			{
				String X1 = CompleteData[Start+Count][1];
				int Encount = 0; //Number Of Lines In a Stroke
				while ((!X1.equals("1")) && (Start+Count < Samp[i+1]))
				{
//					Log.i(TAG, "X1 Data: "+X1+" and Start = "+Start+" and Count = "+Count+" i+1 sample = "+Samp[i+1]);
					float[] KValue = new float[5];
					for (int j = 0; j < 5; j++)
					{
						try
						{
							KValue[j] = Float.parseFloat(CompleteData[Start+Count][j+3]);							
						}
						catch(Exception e)
						{
							KValue[j] = 0;
						}
						if (Start+Count >= TotalRows)
							break;
						X1 = CompleteData[Start+Count][1];
					}
//					Log.i(TAG, "i+1: "+(i+1)+", Stnum+1 = "+(Stnum+1)+", Encount = "+Encount+", k0="+KValue[0]+", k1="+KValue[1]+", k2="+KValue[2]+", k3="+KValue[3]+", k4="+KValue[4]);
					AllStrokes.StoreValue(Stnum, Encount, KValue);//[StrokeCounter] = new Stroke(i+1, Stnum+1, Encount, KValue);
					Encount++;					
					Count++;
					if (Start+Count > TotalRows)
						break;
				}
				AllStrokes.SetLinesPerStroke(Stnum, Encount-1);
				Stnum++;
				StrokeCounter++;
			}
			//			Log.i(TAG, "Strokes per user: "+StrokePerUser[i]+"Total strokes: "+StrokeCounter);
	}
	
	private int[] CountEachSamples()
	{
		int TotalExpectedSamples = (int)(TotalRows/3);
		int[] SampleCounter = new int[TotalExpectedSamples];
		int SampleNumber = 1;
		int Count = 0;
		String DataVal = CompleteData[Count][0];
		SampleCounter[0] = 0;
		while (Count < TotalRows)
		{
			//Log.i(TAG,"Count="+Count);
			if (!DataVal.equals(CompleteData[Count][0]))
			{
				SampleCounter[SampleNumber] = Count;
				SampleNumber++;// = SampleNumber+1;
				DataVal = CompleteData[Count][0];
			}
			Count++;
			if (Count >= TotalRows)
			{
				SampleCounter[SampleNumber] = Count-1;
			}
		}
		return SampleCounter;
	}
	*/
	private void ExtractFeatures()
	{
//		Log.i(TAG, "Extracting features"+" TotalStrokes="+AllStrokes.GetTotalStrokes()+" Number of features="+NumberOfFeatures);
//		AllStrokes.Print();
		FeatureCollection = new SwipeFeatures(AllStrokes.GetTotalStrokes(), NumberOfFeatures);
			for (int j = 0; j < AllStrokes.GetTotalStrokes(); j++)
			{
//				Log.i(TAG, "Inside j="+j);
				if (AllStrokes.StrokeExists(j, 0, 0))
				{
					//Log.i(TAG, "AllStrokes.GetLinesInStroke(i, j) = "+AllStrokes.GetLinesPerStroke(j));
					float[][] OneStroke = new float[AllStrokes.GetLinesPerStroke(j)][5];
					for (int k = 0; k < AllStrokes.GetLinesPerStroke(j); k++)
					{
						//Log.i(TAG, "Inside k="+k);
						if (AllStrokes.StrokeExists(j, k, 0))
						{
							for (int l = 0; l < 5; l++)
							{
								OneStroke[k][l] = AllStrokes.GetKValueAt(j, k, l);
								//Log.i(TAG, "OneStroke[k][l]="+OneStroke[k][l]+" where k="+k+" and l="+l+" and j ="+j+" AllStrokes.GetKValueAt(j, k, l)"+AllStrokes.GetKValueAt(j, k, l));
							}
						}
						else
						{
							Log.i(TAG, "Stroke does not exist!");
							break;
						}
					}
					FeatureCollection.SetSingleFeature(GetFeature(OneStroke), j);
				}
				else
				{
					Log.i(TAG, "Stroke does not exists");					
				}
	//				if (Stroke)
			}
	}
	
	private String[][] FeatureValsToArry()
	{
		int TotalFeatureRows = FeatureCollection.GetNumberOfStrokes();
		String[][] Vals = new String[TotalFeatureRows][NumberOfFeatures];
		for (int j=0; j<GetFeatureVar().GetNumberOfStrokes(); j++)
		{
			for (int k=0; k<NumberOfFeatures; k++)
			{
				Vals[j][k] = Float.toString(GetFeatureVar().GetValueAt(j, k));
			}
		}		
		return Vals;
	}
	int BnFCounter = 0;
	private SwipeSingleFeature GetFeature(float[][] OneStroke)
	{
	//	Log.i(TAG, Arrays.deepToString(OneStroke));
		int A = OneStroke.length;
		//Log.i(TAG, "A = "+A);
//		int B = OneStroke[0].length;
		int Type = -1;
		SwipeSingleFeature out = new SwipeSingleFeature(NumberOfFeatures+1);
		if ((Max(OneStroke, -1, 0)-Min(OneStroke, -1,0)) > (Max(OneStroke, -1, 1)-Min(OneStroke, -1, 1)))
			Type=1; //Horizontal stroke
		else
			Type = 2; //Vertical Stroke
		out.SetFeatureAt(NumberOfFeatures, Type);
		
		if (A<=4) //ShortStrokes filtered
		{
			out.SetFeatureAt(NumberOfFeatures, 3);
			return out;
		}
		/*Log.i(TAG, "Stroke(1,type) = "+OneStroke[0][Type-1]+", Stroke(1,type) = "+OneStroke[1][Type-1]);
		if (OneStroke[0][Type-1]<OneStroke[1][Type-1])
		{
			for (int i = 1; i<A-1; i++)
			{
				if (OneStroke[i][Type-1]>OneStroke[i+1][Type-1])
				{
					out.SetFeatureAt(NumberOfFeatures, 3);
					Log.i(TAG, "Back and forth top: "+BnFCounter);
					BnFCounter++;
					return out;
				}
			}
		}
		else
		{
			for (int i = 1; i<A-1; i++)
			{
				if (OneStroke[i][Type-1]<OneStroke[i+1][Type-1])
				{
					out.SetFeatureAt(NumberOfFeatures, 3);
					Log.i(TAG, "Back and forth bottom: "+BnFCounter);
					BnFCounter++;
					return out;
				}
			}		
		}
		*/
//		Log.i(TAG, "Not back n forth:");
		int fc = 0;
		//Back and forth movement in single stroke is also filtered out
	
		if (out.GetFeatureAt(NumberOfFeatures)==1)
		{
			if (OneStroke[0][0]<OneStroke[A-1][0])
			{
				//Log.i(TAG, "First 4 from 1st");
				out.SetFeatureAt(fc, OneStroke[0][0]); fc++; //fc=0 set
				out.SetFeatureAt(fc, OneStroke[0][1]); fc++; //fc=1 set
				out.SetFeatureAt(fc, OneStroke[A-1][0]); fc++;//fc=2 set
				out.SetFeatureAt(fc, OneStroke[A-1][1]); fc++;//fc=3 set
			}
			else
			{
				//Log.i(TAG, "First 4 from 2nd");
				out.SetFeatureAt(fc, OneStroke[A-1][0]); fc++;
				out.SetFeatureAt(fc, OneStroke[A-1][1]); fc++;				
				out.SetFeatureAt(fc, OneStroke[0][0]); fc++;
				out.SetFeatureAt(fc, OneStroke[0][1]); fc++;
			}
		}
		else
		{
			if (OneStroke[0][1]<OneStroke[A-1][1])
			{
				//Log.i(TAG, "First 4 from 3rd");
				out.SetFeatureAt(fc, OneStroke[0][0]); fc++;
				out.SetFeatureAt(fc, OneStroke[0][1]); fc++;
				out.SetFeatureAt(fc, OneStroke[A-1][0]); fc++;
				out.SetFeatureAt(fc, OneStroke[A-1][1]); fc++;
			}
			else
			{
				//Log.i(TAG, "First 4 from 4th");
				out.SetFeatureAt(fc, OneStroke[A-1][0]); fc++;
				out.SetFeatureAt(fc, OneStroke[A-1][1]); fc++;				
				out.SetFeatureAt(fc, OneStroke[0][0]); fc++;
				out.SetFeatureAt(fc, OneStroke[0][1]); fc++;
			}
			
		}
		//Distance
		float DistanceVal = (float) Math.sqrt((Math.pow((OneStroke[0][0]-OneStroke[A-1][0]), 2)) + (Math.pow((OneStroke[0][1]-OneStroke[A-1][1]), 2)));
		out.SetFeatureAt(fc, DistanceVal); fc++; //fc=4 set
		float Val = 0;
//		out.SetFeatureAt(fc, (float) Math.sqrt((Math.pow((OneStroke[0][0]-OneStroke[A-1][0]), 2)) + (Math.pow((OneStroke[0][1]-OneStroke[A-1][1]), 2))));
		for (int i = 0; i < A-2; i++)
		{
			Val += (float)Math.sqrt((Math.pow((OneStroke[i][0]-OneStroke[i+1][0]), 2)) + (Math.pow((OneStroke[i][0]-OneStroke[i+1][2]), 1)));
		}
		out.SetFeatureAt(fc, Val); fc++;//fc=5 set
		
		//Angle
		if (out.GetFeatureAt(NumberOfFeatures)==1)
		{
			out.SetFeatureAt(fc, (OneStroke[0][1]-OneStroke[A-1][1])/(OneStroke[0][0]-OneStroke[A-1][0]));
		}
		else
		{
			out.SetFeatureAt(fc, (OneStroke[0][0]-OneStroke[A-1][0])/(OneStroke[0][1]-OneStroke[A-1][1]));			
		}
		fc++; //fc=6 set

		//Time length
		out.SetFeatureAt(fc, OneStroke[A-1][4]-OneStroke[0][4]); fc++;//fc=7 set
		//xv
		float[] XV = new float[A];
		for (int i = 0; i < (A-1); i++)
		{
			XV[i] = Math.abs((OneStroke[i+1][0]-OneStroke[i][0])/(OneStroke[i+1][4]-OneStroke[i][4]));
			//Log.i(TAG, "Starting ev. OneStroke="+Arrays.toString(OneStroke[i])+" and i+1 = "+Arrays.toString(OneStroke[i+1])+"XV[i]="+XV[i]);
		}
		float[] YV = new float[A];
		for (int i = 0; i < (A-1); i++)
			YV[i] = Math.abs((OneStroke[i+1][1]-OneStroke[i][1])/(OneStroke[i+1][4]-OneStroke[i][4]));

		//ev velocity
		float[] EV = new float[A];
		for (int i = 0; i < (A-1); i++)
			EV[i] = (float) Math.sqrt((float)(Math.pow(XV[i], 2)+Math.pow(YV[i], 2)));
		out.SetFeatureAt(fc, Mean(EV));fc++;//fc=8 set
		out.SetFeatureAt(fc, StdDev(EV));fc++;//fc=9 set
		out.SetFeatureAt(fc, GetQuantile(EV, 0.25f)); fc++;//fc=10 set
		out.SetFeatureAt(fc, GetQuantile(EV, 0.50f)); fc++;//fc=11 set
		out.SetFeatureAt(fc, GetQuantile(EV, 0.75f)); fc++;//fc=12 set
		//Log.i(TAG, "Finished with ev");
		
		float[] XA = new float[A-1];
		for (int i = 0; i < (A-2); i++)
			XA[i] = (XV[i+1]-XV[i])/((OneStroke[i+2][4]-OneStroke[i][4])/2);

		float[] YA = new float[A-1];
		for (int i = 0; i < (A-2); i++)
			YA[i] = (YV[i+1]-YV[i])/((OneStroke[i+2][4]-OneStroke[i][4])/2);

		//ea acceleration
		float[] EA = new float[A-1];
		for (int i = 0; i < (A-2); i++)
			EA[i] = (EV[i+1]-EV[i])/((OneStroke[i+2][4]-OneStroke[i][4])/2);;

		out.SetFeatureAt(fc, Mean(EA));fc++;//fc=13 set
		out.SetFeatureAt(fc, StdDev(EA));fc++;//fc=14 set
		out.SetFeatureAt(fc, GetQuantile(EA, 0.25f)); fc++;//fc=15 set
		out.SetFeatureAt(fc, GetQuantile(EA, 0.50f)); fc++;//fc=16 set
		out.SetFeatureAt(fc, GetQuantile(EA, 0.75f)); fc++;//fc=17 set

		//Log.i(TAG, "Finished with ea");
		
		//Pressure
		out.SetFeatureAt(fc, Mean(OneStroke, -1, 2)); fc++;//fc=18 set
		out.SetFeatureAt(fc, StdDev(OneStroke, -1, 2)); fc++;//fc=19 set
		out.SetFeatureAt(fc, GetQuantile(OneStroke, -1, 2, 0.25f)); fc++;//fc=20 set
		out.SetFeatureAt(fc, GetQuantile(OneStroke, -1, 2, 0.50f)); fc++;//fc=21 set
		out.SetFeatureAt(fc, GetQuantile(OneStroke, -1, 2, 0.75f)); fc++;//fc=22 set
		return out;
	}

	private float GetQuantile(float[] Arr, float QuantilePosition)
	{
		float QuantileVal = 0.0f;
		int n = Arr.length;
		Arrays.sort(Arr);
		float Position = n*QuantilePosition;
		if (Position>Math.round(Position) && Position<(Math.round(Position)+1))
			QuantileVal = (float) ((Arr[Math.round(Position)] + Arr[Math.round(Position)+1])/2.0);
		else
			QuantileVal = Arr[Math.round(Position)]*1.0f;
		return QuantileVal;
	}

	private float StdDev(float[] Arr)
	{
		float StdDevVal = 0.0f;
		float Summation = 0.0f;
		int n = Arr.length;
		float MeanVal = Mean(Arr);
		for (int i = 0; i < n; i++)
			Summation += Math.pow((Arr[i] - MeanVal), 2);
		StdDevVal = (float)Math.sqrt((Summation/n*1.0f));
		return StdDevVal;
	}
	float Mean(float[] Arr)
	{
		float MeanVal = 0.0f;
		int ArrLength = Arr.length;
		for (int i = 0; i < ArrLength; i++)
			MeanVal += Arr[i];
		MeanVal = MeanVal*1.0f/ArrLength;
		return MeanVal;
	}

	
	private float GetQuantile(float[][] Arr, int Index1, int Index2, float QuantilePosition)
	{
		float QuantileVal = 0.0f;
		if ((Index1<0 && Index2 < 0) || (Index1>=0 && Index2 >= 0))
			return 0.0f;
		float[] TempArr;
		int n = (Index1<0)?Arr.length:Arr[0].length;
		TempArr = new float[n];
		if (Index1<0)
		{
			for (int i =0; i<n; i++) TempArr[i] = Arr[i][Index2];
		}
		else
		{
			for (int i =0; i<n; i++) TempArr[i] = Arr[Index1][i];			
		}
		Arrays.sort(TempArr);
		float Position = n*QuantilePosition;
		if (Position>Math.round(Position) && Position<(Math.round(Position)+1))
		{
			QuantileVal = (TempArr[Math.round(Position)] + TempArr[Math.round(Position)+1])/2.0f;
			/*
			if (Index1<0)
				QuantileVal = (float) ((Arr[Math.round(Position)][Index2] + Arr[Math.round(Position)+1][Index2])/2.0);				
			else
				QuantileVal = (float) ((Arr[Index1][Math.round(Position)] + Arr[Index1][Math.round(Position)+1])/2.0);
			*/
		}
		else
		{
			QuantileVal = TempArr[Math.round(Position)]*1.0f;
			/*
			if (Index1<0)
				QuantileVal = Arr[Math.round(Position)][Index2]*1.0f;
			else
				QuantileVal = Arr[Index1][Math.round(Position)]*1.0f;
			*/
		}
		return QuantileVal;
	}

	private float StdDev(float[][] Arr, int Index1, int Index2)
	{
		if ((Index1<0 && Index2 < 0) || (Index1>=0 && Index2 >= 0))
			return 0.0f;
		float StdDevVal = 0.0f;
		float Summation = 0.0f;
		int n = Arr.length;
		float MeanVal = Mean(Arr, Index1, Index2);
		for (int i = 0; i < n; i++)
		{
			if (Index1<0)
				Summation += Math.pow((Arr[i][Index2] - MeanVal), 2);
			else
				Summation += Math.pow((Arr[Index1][i] - MeanVal), 2);				
		}
		StdDevVal = (float)Math.sqrt((Summation/n*1.0f));
		return StdDevVal;
	}
	private float Mean(float[][] Arr, int Index1, int Index2)
	{
		if ((Index1<0 && Index2 < 0) || (Index1>=0 && Index2 >= 0))
			return 0.0f;
		float MeanVal = 0.0f;
		int ArrLength = Arr.length;
		for (int i = 0; i < ArrLength; i++)
		{
			if (Index1<0)
				MeanVal += Arr[i][Index2];
			else
				MeanVal += Arr[Index1][i];			
		}
		MeanVal = MeanVal*1.0f/ArrLength;
		return MeanVal;
	}

	
	
	private float Max(float[][] Arr, int Index1, int Index2) //If one if the index is -1, with other index constant, index with -1 will be searched in one dimension
	{
		float MaxValue = 0;
		if (Index1<0 && Index2<0)
		{
			for (int i = 0; i<Arr.length; i++)
			{
				for (int j = 0; j < Arr[0].length; j++)
				{
					if (Arr[Index1][Index2] > MaxValue)
						MaxValue = Arr[Index1][Index2];					
				}
			}
		}
		else if (Index1<0)
		{
			for (int i = 0; i<Arr.length; i++)
			{
				if (Arr[i][Index2] > MaxValue)
					MaxValue = Arr[i][Index2];
			}
		}
		else if (Index2<0)
		{
			for (int i = 0; i<Arr[0].length; i++)
			{
				if (Arr[Index1][i] > MaxValue)
					MaxValue = Arr[Index1][i];
			}
		}
//		Log.i(TAG, "MaxValue is ="+MaxValue+"at Index1="+Index1+"Index2="+Index2);
		return MaxValue;
	}
	private float Min(float[][] Arr, int Index1, int Index2) //If one if the index is -1, with other index constant, index with -1 will be searched in one dimension
	{
		float MinValue = 1920;
		if (Index1<0 && Index2<0)
		{
			for (int i = 0; i<Arr.length; i++)
			{
				for (int j = 0; j < Arr[0].length; j++)
				{
					if (Arr[Index1][Index2] < MinValue)
						MinValue = Arr[Index1][Index2];					
				}
			}
		}
		else if (Index1<0)
		{
			for (int i = 0; i<Arr.length; i++)
			{
				if (Arr[i][Index2] < MinValue)
					MinValue = Arr[i][Index2];
			}
		}
		else if (Index2<0)
		{
			for (int i = 0; i<Arr[0].length; i++)
			{
				if (Arr[Index1][i] < MinValue)
					MinValue = Arr[Index1][i];
			}
		}
//		Log.i(TAG, "MinValue is ="+MinValue+"at Index1="+Index1+"Index2="+Index2);
		return MinValue;
	}

}