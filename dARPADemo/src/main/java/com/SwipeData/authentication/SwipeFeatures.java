package com.SwipeData.authentication;

public class SwipeFeatures
{
	private SwipeSingleFeature[] FeatureCollection;
	private int TotalDimensions = 0;
	private int NumberOfStrokes = 0;
	
	SwipeFeatures(int NumberOfStrokes1, int TotalDimensions1)
	{
		TotalDimensions = TotalDimensions1;
		NumberOfStrokes = NumberOfStrokes1;
		FeatureCollection = new SwipeSingleFeature[NumberOfStrokes];
		for (int j=0; j<NumberOfStrokes; j++)
		{
			FeatureCollection[j] = new SwipeSingleFeature(TotalDimensions);
		}
	}
	
	public void SetSingleFeature(SwipeSingleFeature F1, int StrokeNumber)
	{
		FeatureCollection[StrokeNumber] = F1;
	}
	
	public void SetSingleFeatureValue(int StrokeNumber, int FeatureID1, float Value)
	{
		FeatureCollection[StrokeNumber].SetFeatureAt(FeatureID1, Value);
	}

	public float GetValueAt(int StrokeNumber, int DimensionNumber)
	{
		return FeatureCollection[StrokeNumber].GetFeatureAt(DimensionNumber);
	}

	public int GetTotalDimesions()
	{
		return TotalDimensions;
	}
	
	public int GetNumberOfStrokes()
	{
		return NumberOfStrokes;
	}

}
