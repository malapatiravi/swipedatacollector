package com.SwipeData.authentication;


import java.util.Arrays;
import java.util.ArrayList;
import android.util.Log;

public class Stroke
{
	private String TAG="StrokeClass";
	private int TotalStrokes = 0;
	private ArrayList<ArrayList<ArrayList<Float>>> Data1;
	Stroke()
	{
		Data1 					= new ArrayList<ArrayList<ArrayList<Float>>>(); //new float[TotalStrokes][TotalEncount][5];
		TotalStrokes 		= 0;
	}
	
	void StoreValue(int StrokesID, int LinesCount, float[] K1)
	{
		if (StrokesID>TotalStrokes)
			StrokesID=TotalStrokes-1;
		if (StrokesID==TotalStrokes)
		{
			Data1.add(StrokesID, new ArrayList<ArrayList<Float> >());
			TotalStrokes++;
		}
		if (LinesCount == Data1.get(StrokesID).size())
			Data1.get(StrokesID).add(LinesCount, new ArrayList<Float>());
		//Log.i(TAG, "StrokeID = "+StrokesID+" Lines count ="+LinesCount);
		for(int i = 0; i < 5; i++) Data1.get(StrokesID).get(LinesCount).add(i, K1[i]);
		//String TempData = "";
		//for(int i = 0; i < 5; i++) TempData+=Data1.get(StrokesID).get(LinesCount).get(i)+",";
		//Log.i(TAG, TempData);
	}
	void SetTotalStrokes(int Value)
	{
		TotalStrokes = Value;
	}

	void Print()
	{
		Log.i(TAG, Arrays.deepToString(Data1.toArray()));
	}


	int GetTotalStrokes()
	{
		return TotalStrokes;
	}

	int GetLinesPerStroke(int StrokeID)
	{
		return Data1.get(StrokeID).size();
	}

	int GetDataSize()
	{
		return 5;
	}
	
	float GetKValueAt(int StrokeID, int TotalEncount, int KIndex)
	{
		//String TempData = "Bottom: ";
		//for(int i = 0; i < 5; i++) TempData+=Data1.get(StrokeID).get(TotalEncount).get(i)+",";
		//Log.i(TAG, TempData);
		return Data1.get(StrokeID).get(TotalEncount).get(KIndex);
	}
	
	boolean StrokeExists(int StrokesID, int LineNumber, int TotalVariables)
	{
		//Log.i(TAG, "TotalVariables = "+TotalVariables+" StrokesID = "+StrokesID+", GetTotalStrokes() ="+GetTotalStrokes()+" and length = "+Data1.get(StrokesID).size()+" and LineNumber = "+LineNumber);
		if (TotalVariables >= 5 || StrokesID >= GetTotalStrokes() || Data1.get(StrokesID).size() < LineNumber) return false;
		return true;
	}
}