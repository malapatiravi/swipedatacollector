package com.SwipeData.authentication;

public class SwipeSingleFeature
{
	//1=boolean
	//2=
	private String TAG = "FeaturesClass";
	private float[] FeatureValues;

	SwipeSingleFeature(int FeatureNumbers)
	{
		FeatureValues = new float[FeatureNumbers];
		for (int i = 0; i<FeatureNumbers; i++)
			FeatureValues[i] = 0.0f;
	}
	
	float GetFeatureAt(int FeatureID)
	{
		return FeatureValues[FeatureID];
	}
	
	void SetFeatureAt(int FeatureID, float FeatureValue)
	{
		FeatureValues[FeatureID] = (float) (FeatureValue*1.0);
		//Log.i(TAG, "FeatureID = "+FeatureID+" and Value = "+FeatureValue+" has been set!");
	}
}
