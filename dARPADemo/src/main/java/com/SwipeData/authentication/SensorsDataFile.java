package com.SwipeData.authentication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.os.Environment;

/**
 *
 * @author RaviMalz
 */
public class SensorsDataFile {
    public String FileName;
    public String FileType;
    private String DataFolder = "Authentication/Accelerometer/User1";
    public SensorsDataFile(String FName)
    {
    	 new File(Environment.getExternalStorageDirectory(), DataFolder);
       FileName=FName;
       if(FName.contains(".txt"))FileType="txt";
       else if (FName.contains(".xls")) FileType="xls";
       else FileType=null;
    }
    public static boolean isDouble(String Str)  
    {  
        try  
        {  
         double DBL = Double.parseDouble(Str);  
        }  
        catch(NumberFormatException nfe)  
        {  
         return false;  
        }  
        return true;  
    }
    public ArrayList<Double> txtRead(int column) throws FileNotFoundException, IOException
     {
        int ElementIndex;        
        ArrayList<Double> ElementArray = new ArrayList<Double>();
        File file = new File(FileName);
        BufferedReader bufRdr = new BufferedReader(new FileReader(file));
        
        String Line;
           //read each line of text file
             String Token;
             while((Line = bufRdr.readLine()) != null)
             {
                 StringTokenizer st = new StringTokenizer(Line,",");
                 ElementIndex=0;
                 while (st.hasMoreTokens())
                 {
                     //get next token and store it in the array if it is double and the desired column
                     Token =st.nextToken();
                     ElementIndex++;
                     if(isDouble(Token)&& ElementIndex==column)
                     ElementArray.add(Double.parseDouble(Token));
                }
         } 
    
         return ElementArray;
      }
    
    public double[] txtReadWindow(int column, int from, int windowSize) throws FileNotFoundException, IOException
     {
        int LineNumber=0, ElementIndex, ArrIndex=0;        
        double ElementArray[] = new double[windowSize+1];
        File File = new File(FileName);
        BufferedReader bufRdr;
        bufRdr = new BufferedReader(new FileReader(File));
        String Line;
        //read each line of text file
        String Token;
        while((Line = bufRdr.readLine()) != null)
           {
               LineNumber++;
               if(LineNumber>=from && LineNumber<=(from+windowSize))
               {
                 StringTokenizer st = new StringTokenizer(Line,",");
                 ElementIndex=0;
                 while (st.hasMoreTokens())
                 {
                     //get next token and store it in the array if it is double and the desired column
                     Token =st.nextToken();
                     ElementIndex++;
                     if(isDouble(Token)&& ElementIndex==column)
                     ElementArray[ArrIndex++]=Double.parseDouble(Token);
                }
               }
               else if (LineNumber>(from+windowSize)) break;
               
         } 
    
         return ElementArray;
      }
  
     public int getNOAReadings() throws FileNotFoundException, IOException
     {
        int LineNumber=0;      
        File File = new File(FileName);
        BufferedReader bufRdr = new BufferedReader(new FileReader(File));
        while(bufRdr.readLine()!= null)
             {
                LineNumber++;
             } 
        return LineNumber;
      }
   // write feature matrix to file 
      public void writeMatrixToFile(double FVMatrix[][]) throws FileNotFoundException 
     {
        String Str;
        int NRow=FVMatrix.length;
        int NCol=FVMatrix[1].length;
        PrintWriter PR;
        PR = new PrintWriter(FileName);
        for (int i=0; i<NRow ; i++)
           {
                for(int j=0;j<NCol;j++)
                {
                  Str = String.valueOf(FVMatrix[i][j]);
                  if(j<NCol-1) {
                      Str=Str.concat(",");
                  } else {
                  }
                  PR.print(Str);
                }
                PR.println();
           }
            PR.close();
     } 
   public void writeArrayListToFile(ArrayList<ArrayList<Double>> fvmatrix) throws FileNotFoundException 
   {
        String Str;
        int NRow=fvmatrix.size();
        int NCol=fvmatrix.get(1).size();
        PrintWriter PR;
        PR = new PrintWriter(FileName);
        for (int i=0; i<NRow ; i++)
           {
                for(int j=0;j<NCol;j++)
                {
                  Str = String.valueOf(fvmatrix.get(i).get(j));
                  if(j<NCol-1) Str=Str.concat(",");
                  PR.print(Str);
                }
                PR.println();
           }
            PR.close();
         } 
        
 
}
