package com.SwipeData.maininterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.SwipeData.authentication.SensorsTemplateManager;
import com.SwipeData.collector.R;
import com.SwipeData.generalobjects.AndroidFileManager;
import com.SwipeData.services.SensorsClass;
import com.SwipeData.services.SensorsAuthService;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class SensorsFragment extends Fragment
{

	private Button AuthBtn;
	private String TAG="SensorsFrag";
	private Intent SensorsService;
	private Intent SensorsAuthService;
	private RadioGroup DemoMode;
	private String MainURL = "http://10.79.54.131/DEMO/savetodb.php";
	private EditText UserID;
	List<String> DataFromMain;
	

	private int NumberOfFeatures = 23;
  private String CurrentFolder = "";
  private String CurrentFile = "";
  private String Folder = "DataFolder";
  private String CurrentMode = "Training";
	private String AuthorizedUser = "Trainer";
	private String TestingFilename = "Testing.txt";
	private String TemplateFilename = "Training.arff";
	private String DemoVal = "Sensors";
	private MainActivity CurrentActivity;
	private String UserIDVal = "NA";
  private TextView UpdateMsgBox;
  private TextView TotalLines;
  private int NumberOfRequiredLines=1000;
  
	private boolean StopSensors = false;

	private BroadcastReceiver AuthenticationMessageReceiver = new BroadcastReceiver() 
	{
    @Override
    public void onReceive(Context context, Intent intent) 
    {
    	if ((CurrentMode.equals("Testing")))
		{
			boolean Authenticated = intent.getBooleanExtra("IsSensorAuthenticated", false);
			String FinalScore = intent.getStringExtra("FinalScore");
			String UpdateMessage = intent.getStringExtra("UpdateMessage")+" with Final Score: "+FinalScore;
			String TotalLinesValue = intent.getStringExtra("TotalLines")+" Lines";
			Log.i(TAG,"Total Number of Lines in Testing Phase-"+TotalLinesValue);
			if (Authenticated)
			{
				AuthBtn.setBackgroundColor(Color.GREEN);
				AuthBtn.setText("Authenticated");
			}
			else
			{
				AuthBtn.setBackgroundColor(Color.RED);
				AuthBtn.setText("Restricted");
			}
			Log.i(TAG, "Received the message: Auth="+Authenticated+" and message = "+UpdateMessage);

				if (CurrentMode.equals("Testing"))
				{
				TotalLines.setText(TotalLinesValue);
				if (FinalScore!=null)
					UpdateMsgBox.setText(UpdateMessage+"\n");
				else
					UpdateMsgBox.setText("Testing has not started Yet..."+"\n");
				}
			//  ... react to local broadcast message""""
		}
    }
	};	
	
	private BroadcastReceiver LoggerMessageReceiver = new BroadcastReceiver() 
	{
    @Override
    public void onReceive(Context context, Intent intent) 
    {
        String TotalLinesValue = intent.getStringExtra("TotalTrainerLines");
        if ((TotalLinesValue!=null) && (CurrentMode.equals("Training")))
        {
        	TotalLines.setText(TotalLinesValue+" Lines");
          if (Integer.parseInt(TotalLinesValue) > NumberOfRequiredLines)
          {
          	AuthBtn.setBackgroundColor(Color.GREEN);
          	AuthBtn.setText("**Enough Data**");
          }
          else
          {
          	AuthBtn.setBackgroundColor(Color.RED);
          	AuthBtn.setText("**Data is not Enough**");        	
          }
        }
        else
        {
          Log.i(TAG, "Data null at the moment");        	
        }
  
    }
	};
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		CurrentActivity = (MainActivity) getActivity();
		final View rootView = inflater.inflate(R.layout.sensors, container, false);
		
		AuthBtn = (Button)rootView.findViewById(R.id.AuthNotif);
  	AuthBtn.setBackgroundColor(Color.RED);
  	UpdateMsgBox = (TextView)rootView.findViewById(R.id.UpdateMessageBox);
  	TotalLines = (TextView)rootView.findViewById(R.id.TotalLines);
  	TotalLines.setText("0 Lines");
    Button StartBtn = (Button) rootView.findViewById(R.id.StartButton);
    Button StopBtn = (Button) rootView.findViewById(R.id.StopButton);
    SensorsService = new Intent(getActivity(), SensorsClass.class);
		SensorsAuthService = new Intent(getActivity(), SensorsAuthService.class);
		
    StartBtn.setOnClickListener(new View.OnClickListener() 
    {
    	@Override
			public void onClick(View arg0) 
    	{
        UpdateMsgBox.setText("Collecting Training Data..Please Wait....\n");
        TotalLines.setText("Empty");
				UserID = (EditText)rootView.findViewById(R.id.UserID);
				if (UserID.length()<4)
				{
					Toast.makeText(getActivity().getBaseContext(), "UserID cannot be less than 4 characters!", Toast.LENGTH_SHORT).show();
				}
				else
				{
					
					UserIDVal = UserID.getText().toString();
					Log.i(TAG,"User id = "+UserIDVal);
					DemoMode = (RadioGroup)rootView.findViewById(R.id.DemoMode);
					int SelectedMode= DemoMode.getCheckedRadioButtonId();
					RadioButton ModeRVal = (RadioButton)rootView.findViewById(SelectedMode);
					String ModeVal = ModeRVal.getText().toString();
					SensorsService.putExtra("DemoMode", ModeVal);
					SensorsService.putExtra("DemoFor", DemoVal);
					SensorsService.putExtra("UserID", UserIDVal);
					AuthorizedUser = UserIDVal;
					SensorsService.putExtra("Folder", Folder);
					CurrentMode = ModeVal;

					
					//root =  new File(Environment.getExternalStorageDirectory(), Folder);
					CurrentFile = ModeVal+".txt";
					CurrentFolder = Folder+"/"+DemoVal+"/"+UserIDVal;
					Log.i(TAG, "Current file = "+CurrentFile+" and folder is="+CurrentFolder);
					SensorsService.putExtra("CurrentURL", MainURL);
					Toast.makeText(getActivity().getBaseContext(), "Starting sensors service", Toast.LENGTH_LONG).show(); 

						Log.i(TAG, "Data is collecting for sensors with "+ModeVal+" and message is = ");
					if (ModeVal.equals("Testing"))
					{
	        	AuthBtn.setBackgroundColor(Color.RED);
	        	AuthBtn.setText("Restricted"); 
						LocalBroadcastManager.getInstance(CurrentActivity).registerReceiver(AuthenticationMessageReceiver, new IntentFilter("AuthenticationBroadcast"));
						Log.i(TAG, "Checking testing"+ModeVal);
						File root =  new File(Environment.getExternalStorageDirectory(), (Folder+"/"+DemoVal+"/"+AuthorizedUser));
						boolean HaveTemplateFile = false;
						if (!root.exists()) 
						{
							Log.i(TAG, "Root does not exists: "+root.getAbsolutePath());
							Toast.makeText(getActivity().getBaseContext(), "There is no training data available", Toast.LENGTH_LONG).show(); 
							HaveTemplateFile = false;
						}
						else
						{
							File DataFile = new File(root, TemplateFilename);							
							if (!DataFile.exists())
							{
								Log.i(TAG, "There is no file");
								HaveTemplateFile = false;							
							}
							else
							{
								try
								{
									BufferedReader BufferedFile = new BufferedReader(new FileReader(DataFile));
									BufferedFile.close();
									
									HaveTemplateFile = true;
									Log.i(TAG, "File is there");
								} catch (IOException e)	
								{	
									e.printStackTrace();
									HaveTemplateFile = false;
									Log.i(TAG, "File is not there");
								}							
							}
							if (HaveTemplateFile)
							{
								Log.i(TAG, "Starting authentication module"+ModeVal);
								SensorsAuthService.putExtra("Username", UserIDVal);
								SensorsAuthService.putExtra("DemoFor", DemoVal);
								SensorsAuthService.putExtra("Filename", TestingFilename);
								SensorsAuthService.putExtra("Folder", Folder);
								SensorsAuthService.putExtra("AuthorizedUser", AuthorizedUser);
								SensorsAuthService.putExtra("TemplateFilename", TemplateFilename);
								SensorsAuthService.putExtra("NumberOfFeatures", NumberOfFeatures);
								if (getActivity().startService(SensorsService) == null)	Log.i(TAG, "No service");
								else 
								{
									Log.i(TAG, "Yes service");
									if (!CheckStopMessageThread.isAlive())
										CheckStopMessageThread.start();
								}

								if (getActivity().startService(SensorsAuthService) == null)	Log.i(TAG, "No service");
								else Log.i(TAG, "Yes service");
							}
							else
							{
								Log.i(TAG, "Cannot start the authentication because there is no trained data");							
							}
						}
					}
					else
					{
	        	AuthBtn.setBackgroundColor(Color.RED);
	        	AuthBtn.setText("Data is not enough"); 
						LocalBroadcastManager.getInstance(CurrentActivity).registerReceiver(LoggerMessageReceiver, new IntentFilter("LoggerMessages"));
						if (getActivity().startService(SensorsService) == null)	Log.i(TAG, "No service");
						else 
						{
							Log.i(TAG, "Yes service");
							if (!CheckStopMessageThread.isAlive())
								CheckStopMessageThread.start();
						}
					}
				}

    	}
    });
    StopBtn.setOnClickListener(new View.OnClickListener() 
    {
    	@Override
			public void onClick(View arg0) 
    	{
    		try
				{
					int SelectedMode= DemoMode.getCheckedRadioButtonId();
					RadioButton ModeRVal = (RadioButton)rootView.findViewById(SelectedMode);
					String ModeVal = ModeRVal.getText().toString();
                    ModeVal.equals("Training");
                    {
                        UpdateMsgBox.setText("Training Started..Please Wait....\n");
                    }

					StopLogging();
				} catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    	}
    });
		return rootView;
	}
	
	public void ReceiveData(List<String> DataFromMain1)
	{
		DataFromMain = DataFromMain1;
	}

	
	private void DoTraining() throws Exception
	{
		AndroidFileManager TrainingFile = new AndroidFileManager(Folder+"/"+DemoVal+"/"+UserIDVal, "Training.txt");
    //File TrainingFile = new File(root, "/"+DemoVal+"/"+UserIDVal+"/Training.txt");
    Log.i(TAG, "Current Path is: "+TrainingFile.GetAbsoluteFilename());

        Toast.makeText(getActivity().getBaseContext(), "Wait for 30 sec", Toast.LENGTH_LONG).show();

    SensorsTemplateManager TraningObject= new SensorsTemplateManager(TrainingFile,true);
    // Creating the Training.Arff File 
    Log.i(TAG, "Data Colelction Stopped .....Creating the Training Arff from the raw data file--"+TrainingFile.GetAbsoluteFilename());
		TraningObject.createArffFileForTraining();
        Toast.makeText(getActivity().getBaseContext(), "Training in progress", Toast.LENGTH_LONG).show();
		AuthBtn.setBackgroundColor(Color.RED);
  	AuthBtn.setText("**Training in Progress**");
	}
	
	Thread CheckStopMessageThread = new Thread()
	{
	    @Override
    public void run() 
	  {
			while(true) 
			{
				boolean Temp = CurrentActivity.GotStopCommand();
				if (StopSensors != Temp)
				{
					Log.i(TAG, "Value is "+(CurrentActivity.GotStopCommand())+" and activity is: "+CurrentActivity.toString());
					try
					{
						StopLogging();
					} catch (Exception e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					StopSensors = false;
				}
			}
    }
	};
	
	private void StopLogging() throws Exception
	{
		CurrentActivity.stopService(SensorsService);		
		CurrentActivity.stopService(SensorsAuthService);
		Log.i(TAG, "Current Mode is = "+CurrentMode);
		if (CurrentMode.equals("Training"))
		{
            Toast.makeText(getActivity().getBaseContext(), "Training Started", Toast.LENGTH_LONG).show();
			DoTraining();
			AuthBtn.setBackgroundColor(Color.RED);
    	    AuthBtn.setText("Training in Progress...");
            Toast.makeText(getActivity().getBaseContext(), "Training Ended", Toast.LENGTH_LONG).show();
            UpdateMsgBox.setText("Training Ended..Start Testing....\n");

		}
	}
}
