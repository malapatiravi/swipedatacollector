package com.SwipeData.maininterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.SwipeData.authentication.SwipeFeatureExtractor;
import com.SwipeData.collector.R;
import com.SwipeData.generalobjects.FileWriterObject;
import com.SwipeData.services.NAOAuthenticationService;
import com.SwipeData.services.OneToOneSenderClass;
import com.SwipeData.services.TouchClass;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class NAOFragment extends Fragment
{
	private Button AuthBtn;
	private String TAG="NAOFrag";
	private Intent TouchService;
	private Intent OneToOneSender;
	private Intent AuthService;
	private RadioGroup DemoMode;
	private String MainURL = "http://10.79.54.131/DEMO/savetodb.php";
	private String OneToOneProcessingURL = "http://10.79.54.131/DEMO/nao2/OneToOne.php";
	private String SwipeProcessingURL	 = "http://10.79.54.131/DEMO/nao/StrokePerStroke.php";
	private EditText UserID;
	private SwipeFeatureExtractor TrainingFE;
	List<String> DataFromMain;
	

	private int NumberOfFeatures = 23;
	  private String CurrentFolder = "";
	  private String CurrentFile = "";
	  private String Folder = "DataFolder";
	  private String CurrentMode = "Training";
	private String AuthorizedUser = "Suzzett";
	private String TestingFilename = "Testing.txt";
	private String TemplateFilename = "TrainingTemplate.txt";
	private String DemoVal = "NAO";
	private MainActivity CurrentActivity;
	private boolean StopSwipe = false;

	
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() 
	{
    @Override
    public void onReceive(Context context, Intent intent) 
    {
        boolean Authenticated = intent.getBooleanExtra("IsAuthenticated", false);
        if (Authenticated)
        {
        	AuthBtn.setBackgroundColor(Color.GREEN);
        	AuthBtn.setText("Authenticated");
        }
        else
        {
        	AuthBtn.setBackgroundColor(Color.RED);
        	AuthBtn.setText("Restricted");        	
        }
        //  ... react to local broadcast message
    }
	};	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		CurrentActivity = (MainActivity) getActivity();



		final View rootView = inflater.inflate(R.layout.nao, container, false);
		LocalBroadcastManager.getInstance(CurrentActivity).registerReceiver(mMessageReceiver, new IntentFilter("NAOAS"));
		
		AuthBtn = (Button)rootView.findViewById(R.id.AuthNotif);
  	AuthBtn.setBackgroundColor(Color.RED);
    Button StartBtn = (Button) rootView.findViewById(R.id.StartButton);
    Button StopBtn = (Button) rootView.findViewById(R.id.StopButton);
    TouchService = new Intent(getActivity(), TouchClass.class);
    OneToOneSender = new Intent(getActivity(), OneToOneSenderClass.class);
		AuthService = new Intent(getActivity(), NAOAuthenticationService.class);

    StartBtn.setOnClickListener(new View.OnClickListener() 
    {
    	@Override
			public void onClick(View arg0) 
    	{
				UserID = (EditText)rootView.findViewById(R.id.UserID);
				String UserIDVal = UserID.getText().toString();
				if (UserID.length()<4)
				{
					Toast.makeText(getActivity().getBaseContext(), "UserID cannot less than 4 characters!", Toast.LENGTH_SHORT).show();
				}
				else
				{
					
					DemoMode = (RadioGroup)rootView.findViewById(R.id.DemoMode);
					int SelectedMode= DemoMode.getCheckedRadioButtonId();
					RadioButton ModeRVal = (RadioButton)rootView.findViewById(SelectedMode);
					String ModeVal = ModeRVal.getText().toString();
					TouchService.putExtra("DemoMode", ModeVal);
					TouchService.putExtra("DemoFor", DemoVal);
					TouchService.putExtra("UserID", UserIDVal);
					OneToOneSender.putExtra("DemoMode", ModeVal);
					OneToOneSender.putExtra("DemoFor", DemoVal);
					OneToOneSender.putExtra("UserID", UserIDVal);
					CurrentFile = ModeVal+".txt";
					CurrentFolder = Folder+"/"+DemoVal+"/"+UserIDVal;
					Log.i(TAG, "Current file = "+CurrentFile+" and folder is="+CurrentFolder);
					AuthorizedUser = UserIDVal;
					OneToOneSender.putExtra("OneToOneURL", OneToOneProcessingURL);


						Log.i(TAG, "Data is collecting for NAO with "+ModeVal+" and message is = ");
					if (ModeVal.equals("TestingXXX"))
					{
						TouchService.putExtra("CurrentURL", MainURL);
						CurrentMode = "Testing";
						Log.i(TAG, "Checking testing"+ModeVal);
						File root =  new File(Environment.getExternalStorageDirectory(), (Folder+"/"+DemoVal+"/"+AuthorizedUser));
						boolean HaveTemplateFile = false;
						if (!root.exists()) 
						{
							Log.i(TAG, "Root does not exists");
							Toast.makeText(getActivity().getBaseContext(), "There is no training data available", Toast.LENGTH_LONG).show(); 
							HaveTemplateFile = false;
						}
						else
						{
							File DataFile = new File(root, TemplateFilename);							
							if (!DataFile.exists())
							{
								Log.i(TAG, "There is no file");
								HaveTemplateFile = false;							
							}
							else
							{
								try
								{
									BufferedReader BufferedFile = new BufferedReader(new FileReader(DataFile));
									BufferedFile.close();
									
									HaveTemplateFile = true;
									Log.i(TAG, "File is there");
								} catch (IOException e)	
								{	
									e.printStackTrace();
									HaveTemplateFile = false;
									Log.i(TAG, "File is not there");
								}							
							}
							if (HaveTemplateFile)
							{
								Log.i(TAG, "Starting authentication module"+ModeVal);
								AuthService.putExtra("Username", UserIDVal);
								AuthService.putExtra("DemoFor", DemoVal);
								AuthService.putExtra("Filename", TestingFilename);
								AuthService.putExtra("Folder", Folder);
								AuthService.putExtra("AuthorizedUser", AuthorizedUser);
								AuthService.putExtra("TemplateFilename", TemplateFilename);
								AuthService.putExtra("NumberOfFeatures", NumberOfFeatures);
								//if (getActivity().startService(AuthService) == null)	Log.i(TAG, "No sensors service");
								//else Log.i(TAG, "Yes service");
								if (getActivity().startService(TouchService) == null)	Log.i(TAG, "No authentication service");
								else 
								{
									Log.i(TAG, "Yes service");
									if (!CheckStopMessageThread.isAlive())
										CheckStopMessageThread.start();
								}
							}
							else
							{
								Log.i(TAG, "Cannot start the authentication because there is no trained data");							
							}
						}
					}
					else if (ModeVal.equals("Training") || ModeVal.equals("Testing"))
					{
						if (ModeVal.equals("Training"))
						{
							TouchService.putExtra("CurrentURL", SwipeProcessingURL);
							CurrentMode = "Training";
						}
						else if (ModeVal.equals("Testing"))
						{
							TouchService.putExtra("CurrentURL", MainURL);
							CurrentMode = "Training";
						}
						if (getActivity().startService(TouchService) == null)	Log.i(TAG, "No service");
						else 
						{
							Log.i(TAG, "Yes service");
							if (!CheckStopMessageThread.isAlive())
								CheckStopMessageThread.start();
						}
					}
					else if (ModeVal.equals("One To One"))
					{
						CurrentMode = "OneToOne";
						if (getActivity().startService(OneToOneSender) == null)	Log.i(TAG, "No service");
						else 
						{
							Log.i(TAG, "Yes service");
							if (!CheckStopMessageThread.isAlive())
								CheckStopMessageThread.start();
						}
						Toast.makeText(getActivity().getBaseContext(), "Starting one to one", Toast.LENGTH_LONG).show();
						
					}
					else
					{
						Toast.makeText(getActivity().getBaseContext(), "Some error occured", Toast.LENGTH_LONG).show(); 						
					}
				}

    	}
    });
    StopBtn.setOnClickListener(new View.OnClickListener() 
    {
    	@Override
			public void onClick(View arg0) 
    	{
    		if (CurrentMode.equals("Training"))
    		{
					DoTraining();
    		}
				Log.i(TAG, "Stopping data collection "+CurrentMode);
				getActivity().stopService(TouchService);
				getActivity().stopService(OneToOneSender);
				getActivity().stopService(AuthService);
    	}
    });
    
    

		return rootView;
	}
	
	public void ReceiveData(List<String> DataFromMain1)
	{
		DataFromMain = DataFromMain1;
	}
	private void DoTraining()
	{
		TrainingFE = new SwipeFeatureExtractor(NumberOfFeatures, CurrentFile, CurrentFolder);
		Log.i(TAG, "Finished training");
		CreateTrainedTemplate();
	}
	
	Thread CheckStopMessageThread = new Thread()
	{
	    @Override
    public void run() 
	  {
			while(true) 
			{
				boolean Temp = CurrentActivity.GotStopCommand();
				if (StopSwipe != Temp)
				{
					Log.i(TAG, "Value is "+(CurrentActivity.GotStopCommand())+" and activity is: "+CurrentActivity.toString());	
					StopLogging();
					StopSwipe = false;
				}
			}
    }
	};
	
	private void StopLogging()
	{
		CurrentActivity.stopService(TouchService);		
		CurrentActivity.stopService(OneToOneSender);
		CurrentActivity.stopService(AuthService);
		Log.i(TAG, "Current Mode is = "+CurrentMode);
		if (CurrentMode.equals("Training"))
			DoTraining();
	}
	private void CreateTrainedTemplate()
	{
		String TemplateFilename = "TrainingTemplate.txt";
		String FeatureData = "";
		FileWriterObject TrainedTemplate = new FileWriterObject(TemplateFilename, CurrentFolder);
			for (int j=0; j<TrainingFE.GetFeatureVar().GetNumberOfStrokes(); j++)
			{
				for (int k=0; k<NumberOfFeatures; k++)
				{
					FeatureData = FeatureData+(k==0?"":",")+TrainingFE.GetFeatureVar().GetValueAt(j, k);
//					Log.i(TAG,"i="+i+", j="+j+", k="+k+", TrainingFE.GetFeatureVar().GetValueAt(i, j, k)="+TrainingFE.GetFeatureVar().GetValueAt(i, j, k));
				}
				TrainedTemplate.SaveData(FeatureData);
				FeatureData="";
			}
		TrainedTemplate.CloseWriter();
		Log.i(TAG,"Finished creating template file");
		
	}
	
	public void CustomKeyDown()
	{
		
	}
}
