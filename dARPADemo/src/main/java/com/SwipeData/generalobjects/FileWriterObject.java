package com.SwipeData.generalobjects;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Environment;

@SuppressWarnings("unused")
public class FileWriterObject
{
  private String DataFolder = "UnnamedProject";
  private File root;// = new File(Environment.getExternalStorageDirectory(), DataFolder);
	private String Filename="Untitled.txt";
	private File DataFile;
  private BufferedWriter BufferedFile;

	private String TAG = "FWO";
	public FileWriterObject()
	{

	}
	public FileWriterObject(File FObject)
	{
		DataFile = FObject;
    try
		{
			BufferedFile = new BufferedWriter(new FileWriter(DataFile, true));
		} catch (IOException e)	{	e.printStackTrace();}
	}	
	
	public FileWriterObject(File FObject, boolean Append)
	{
		DataFile = FObject;
    try
		{
			BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
		} catch (IOException e)	{	e.printStackTrace();}
	}	
	
	public FileWriterObject(String Filename1, String Folder)
	{
		DataFolder = Folder;
		Filename = Filename1;
		root = new File(Environment.getExternalStorageDirectory(), DataFolder);
  	if (!root.exists()) { root.mkdirs(); }
  	DataFile = new File(root, Filename);
    try
		{
			BufferedFile = new BufferedWriter(new FileWriter(DataFile, true));
		} catch (IOException e)	{	e.printStackTrace();}
    //Log.i(TAG, "File is opened: "+DataFile.getAbsolutePath().toString());
	}
	
	public FileWriterObject(String Filename1, String Folder, boolean Append)
	{
		DataFolder = Folder;
		Filename = Filename1;
		root = new File(Environment.getExternalStorageDirectory(), DataFolder);
  	if (!root.exists()) { root.mkdirs(); }
  	DataFile = new File(root, Filename);
    try
		{
			BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
		} catch (IOException e)	{	e.printStackTrace();}
	}
	
	public FileWriterObject(String CompleteFilename, boolean Append)
	{
		DataFolder = "";
		Filename = CompleteFilename;
		root = new File(Environment.getExternalStorageDirectory(), DataFolder);
  	if (!root.exists()) { root.mkdirs(); }
  	DataFile = new File(root, Filename);
    try
		{
			BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
		} catch (IOException e)	{	e.printStackTrace();}
	}
	
	protected void CreateFileWriterObject()
	{
		
	}
	
	protected void CreateFileWriterObject(File FO)
	{
		DataFile = FO;
    try
		{
			BufferedFile = new BufferedWriter(new FileWriter(DataFile, true));
		} catch (IOException e)	{	e.printStackTrace();}
	}
	protected void CreateFileWriterObject(File FO, boolean Append)
	{
		DataFile = FO;
    try
		{
			BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
		} catch (IOException e)	{	e.printStackTrace();}
	}
	public void SaveData(String Data)
	{
		try
		{
			BufferedFile.write(Data);
			BufferedFile.newLine();
			BufferedFile.flush();
		} catch (IOException e)	{	e.printStackTrace();}
	}
	
	public void CloseWriter()
	{
		try
		{
			BufferedFile.close();
		} catch (IOException e)	{	e.printStackTrace();}
	}
}
