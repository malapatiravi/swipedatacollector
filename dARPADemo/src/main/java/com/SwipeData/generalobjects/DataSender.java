package com.SwipeData.generalobjects;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import android.os.AsyncTask;
import android.util.Log;

public class DataSender
{
    private String ServerURL = "http://10.79.54.131/demo/mobile/savetodb.php";
    private HttpURLConnection connection;
    private URL ServerURLObj= null;
    private String TAG = "DataSender";
    
    public DataSender(String UserServerURL)
	{
		ServerURL = UserServerURL;
		try 
		{
			ServerURLObj = new URL(ServerURL);
			connection = (HttpURLConnection) ServerURLObj.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestMethod("POST");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Log.i("Sender:", "Successfully started");
	}
	
	public void SendData(final String DataToServer)
	{
		MyUploadUser task=new MyUploadUser(DataToServer, ServerURL);
		task.execute(ServerURL);
		Log.i("In Send Data", "In Send Data for first time");
		/*Thread SendTaskToServer = new Thread (new Runnable()
		{
			@Override
			public void run()
			{
				String Response = null;
				OutputStreamWriter request = null;
				String SentDataToServer = "DataFromMobile="+DataToServer;
				try {
					DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
					wr.writeBytes(SentDataToServer);
					wr.flush();
					wr.close();

					//-request = new OutputStreamWriter(connection.getOutputStream());
					//-request.write(SentDataToServer);
					//-request.flush();
					//-request.close();
					String ServerLine = "";
					InputStreamReader isr = new InputStreamReader(connection.getInputStream());
					BufferedReader reader = new BufferedReader(isr);
					StringBuilder sb = new StringBuilder();
					while((ServerLine = reader.readLine()) != null)
					{
						sb.append(ServerLine+"\n");
					}
					Response = sb.toString();
					isr.close();
					reader.close();
					Log.i(TAG, Response);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				*//*
				ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();        
		        postParameters.add(new BasicNameValuePair("DataFromMobile", DataToServer));
		        String response = null;
		        String result;
		        try 
		        {
		        	response = CustomHttpClient.executeHttpPost( ServerURL, postParameters);
		        	result = response.toString();
		        	Log.i("SERVER:", "Successfully inserted"+result);
		        }
		        catch(Exception e)
		        {
		        	Log.e("Error","Error sending data to server" + e.toString());
		        }
		        *//*
			}
		});
		SendTaskToServer.start();*/
	}
//Author :Ravi newly introduced function for transmitting data

	public class MyUploadUser extends AsyncTask<String, String, String>
	{
		public String json_m;
		public String url_s;
		public MyUploadUser(String _json, String _url)
		{
			//String SentDataToServer = "DataFromMobile="+DataToServer;
			json_m="DataFromMobile="+_json;
			url_s=_url;
			Log.i("Url is:", ""+url_s);

		}
		@Override
		protected String doInBackground(String... strings) {
			String response_string="";
			try
			{
				URL url = new URL(url_s);
				HttpURLConnection postConn=(HttpURLConnection)url.openConnection();

				// postConn.setDoInput(true);
				postConn.setRequestMethod("POST");
				//postConn.setRequestProperty("X-Access-Token","5d26f33f30a7418c");

				postConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
				postConn.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(postConn.getOutputStream());
				//wr.write(URLEncoder.encode(item.toString(),"UTF-8"));
				Log.i("data here",""+ URLEncoder.encode(json_m,"UTF-8"));
				//wr.writeUTF(URLEncoder.encode(json_m.toString(),"UTF-8"));
				wr.writeBytes(json_m);

				wr.flush();
				wr.close();
				int responseCode = postConn.getResponseCode();
				/*if(responseCode!=200)
				{
					return null;
				}*/
				Log.i("Resonse code is","Response code"+responseCode);
				BufferedReader in = new BufferedReader(new InputStreamReader(postConn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				response_string=response.toString();
				System.out.println("Response String is"+response_string);

			}
			catch(Exception e)
			{
				Log.i("Http", "Http Exception"+e);
			}
			return response_string;
		}

		@Override
		protected void onPostExecute(String jsonString) {
			super.onPostExecute(jsonString);
			if(jsonString!=null)
			{

			}

			else
				return;

		}
	}
}

