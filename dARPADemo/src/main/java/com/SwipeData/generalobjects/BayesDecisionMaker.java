package com.SwipeData.generalobjects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;
    
/**
 *
 * @author RAJESH
 */
public class BayesDecisionMaker{
    File TrainingFile;
    File TestingFile;
    double AcceptanceThreshold;
    BayesDecisionMaker(File TrainingFile, File TestingFile)
    {
       this.TrainingFile=TrainingFile;
       this.TestingFile=TestingFile;
       AcceptanceThreshold=60.0;
    }
  //  public Instances runClassifiers(String TrainingFile,String TestingFile) throws FileNotFoundException, IOException, Exception
    public boolean getTheDecision() throws FileNotFoundException, IOException, Exception
    {
   
    	Instances Trainer;
      Instances Tester;
      BufferedReader TrainingReader;
      TrainingReader = new BufferedReader(new FileReader(TrainingFile));
      Trainer = new Instances(TrainingReader);
     
      // Setting Class Attribute Index for Traning Arff
      Trainer.setClassIndex(Trainer.numAttributes()-1);
      
      BufferedReader TestingReader;
      TestingReader = new BufferedReader(new FileReader(TestingFile));
      Trainer = new Instances(TrainingReader);
      Tester = new Instances(TestingReader);
     
   // Setting Class Attribute Index for Testing Arff
      Tester.setClassIndex(Tester.numAttributes()-1);
      
   // Instantiating Classifier Object
      weka.classifiers.Classifier ClassifierObject = new NaiveBayes();
  // 
      ClassifierObject.buildClassifier(Trainer);
      Evaluation Evaluator = new Evaluation(Trainer);
      Evaluator.evaluateModel(ClassifierObject, Tester);
      double GenuineRate=Evaluator.truePositiveRate(0);
      double ImposterRate=Evaluator.truePositiveRate(1);
      double Correct=Evaluator.truePositiveRate(0);
      double Incorrect=Evaluator.truePositiveRate(1);
     
      System.out.println("Percentage of Genuine Instances"+GenuineRate+"Percentage of Imposters "+ImposterRate);
      
      // return GenuineRate>ImposterRate;
      return Correct>Incorrect;
    }
   
   
 
}
