package com.SwipeData.generalobjects;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author RAJESH
 */
public class Directory {
    
    private String FullPath;
    public ArrayList<File> DataFilesList = new ArrayList<File>();
    public Directory(String Path)
    {
      FullPath=Path;
      retrieveAllDataFiles(FullPath,DataFilesList);
    }
    public void retrieveAllDataFiles(String Path, ArrayList<File> DFList) 
    {
            File Directory = new File(Path);
           // get all the files from a directory
             File[] FilesList = Directory.listFiles();
             for (File CurrentFile : FilesList) 
             {
                if (CurrentFile.isFile()) 
                {
                     DFList.add(CurrentFile);
                } 
                else if (CurrentFile.isDirectory()) 
                {
                     retrieveAllDataFiles(CurrentFile.getAbsolutePath(), DFList);
                }
             }
    }
    
    
}
