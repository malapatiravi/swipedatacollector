package com.SwipeData.generalobjects;

import java.io.File;
import android.os.Environment;

public class AndroidFileManager
{
	private String Filename = "";
	private String FolderName = "";
	private File CurrentFile;
	private File Root = new File(Environment.getExternalStorageDirectory(), "");
	private boolean Append = true;
	private FileWriterObject ThisFWO;

	AndroidFileManager() throws Exception
	{
		throw new Exception("Cannot use this constructor");
  }
	
	public AndroidFileManager(AndroidFileManager AFMObject)
	{
		Filename = AFMObject.GetFilename();
		FolderName = AFMObject.GetCurrentFolder();
		this.Append = true;
		Initialize();
  }
	public AndroidFileManager(String FL, String FN)
	{
		Filename = FN;
		FolderName = FL;
		this.Append = true;
		Initialize();
  }
	public AndroidFileManager( String FL, String FN, boolean Append)
	{
		Filename = FN;
		FolderName = FL;
    this.Append = Append;
    Initialize();
   }
	public String GetAbsoluteFilename()
	{
		return CurrentFile.getAbsolutePath();
	}
	
	public String GetFullPath()
	{
		return Root+"/"+FolderName;
	}
	public void Initialize()
	{
		CurrentFile = new File(Root, FolderName+"/"+Filename);
		ThisFWO = new FileWriterObject(CurrentFile, Append);
	}
	
	private void ReInitialize()
	{
		Close();
		Initialize();
	}
	
	public void Close()
	{
		ThisFWO.CloseWriter();
	}
	
	public String GetCurrentFolder()
	{
		return FolderName;
	}
	
	public void SetCurrentFolder(String FL)
	{
		FolderName = FL;
		ReInitialize();		
	}
	
	public String GetFilename()
	{
		return Filename;
	}
	
	public void SetFilename(String FN)
	{
		Filename = FN;
		ReInitialize();
	}
	
	public void SaveData(String Data)
	{
		ThisFWO.SaveData(Data);
	}	
	
	public void CloseWriter()
	{
		ThisFWO.CloseWriter();
	}
}
