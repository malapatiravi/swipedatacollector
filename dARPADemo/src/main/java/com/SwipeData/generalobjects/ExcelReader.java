package com.SwipeData.generalobjects;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.os.Environment;

public class ExcelReader
{
  private String DataFolder = "DataFolder";
  private File root;// = new File(Environment.getExternalStorageDirectory(), DataFolder);
	private String Filename = "Data.csv";
  private java.io.File CSVFileObject;// = "FilenameRecorder.txt";
  private BufferedReader BufferedFile;
  private int ReadMode; //if ==0 then read mode is swiping and duplicate values should be discarded
  private int IndexOfTime = 0;
	private List<String> AllDataRows = new ArrayList<String>();
	private String[][] CompleteData;
  private int TotalRows = 0;
  private int TotalColumns = 0;
  private float[][] CompleteFloatData;
  private int IndexOfActionType = 1;


  
  ExcelReader()
  {

  }
  
  public ExcelReader(String Filename1, String FileLocation, int RM, int IOT) throws Exception
  {
  	DataFolder = FileLocation;
  	Filename = Filename1;
  	IndexOfTime = IOT;
  	ReadMode = RM;
  	root = new File(Environment.getExternalStorageDirectory(), DataFolder);		
  	CSVFileObject = new File(root , Filename);
		//Log.i(TAG, "CSV file name:"+Filename+" and location="+DataFolder);
  	if (!CSVFileObject.exists())
		{
  		throw new Exception("File : "+CSVFileObject.getAbsolutePath()+" in "+DataFolder+"/"+Filename+" does not exist!");
		}
  	try
		{
			BufferedFile = new BufferedReader(new FileReader(CSVFileObject));
		}
  	catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
  	ReadAllLines();
  }
  
  private void ReadAllLines() throws IOException
  {
		int i = 0;
  	if (ReadMode==0)
  	{
    	String PreviousTime = "";
			String CurrentLine = BufferedFile.readLine();
			while (CurrentLine != null)
			{
				String[] LineColumns = CurrentLine.split(",");
//				Log.i(TAG, "Values Current = "+LineColumns[IndexOfTime]+" and previous = "+PreviousTime);
				if ((!PreviousTime.equals(LineColumns[IndexOfTime])) || LineColumns[IndexOfActionType].equals("1") || LineColumns[IndexOfActionType].equals("0"))
				{
					AllDataRows.add(CurrentLine);
					PreviousTime = LineColumns[IndexOfTime];
					TotalRows++;
				}
				else
				{
//					Log.i(TAG, "Duplicate reading!");
				}
				if (i==0)	TotalColumns = LineColumns.length;
				i++;
				CurrentLine = BufferedFile.readLine();
			}
			
  	}
  	else
  	{
			String CurrentLine = BufferedFile.readLine();
			while (CurrentLine != null)
			{
				String[] LineColumns = CurrentLine.split(",");
				AllDataRows.add(CurrentLine);
				TotalRows++;
				if (i==0)	TotalColumns = LineColumns.length;
				i++;
				CurrentLine = BufferedFile.readLine();
			}  		
  	}
  }
  
  public String[][] GetData()
  {
  	CompleteData = new String[GetTotalRows()][GetTotalColumns()];
  	String CurrentLine = "";
  	String[] CurrentDataColumns = new String[GetTotalColumns()];
  	for (int i = 0; i < AllDataRows.size(); i++)
  	{
  		CurrentLine = AllDataRows.get(i);
  		CurrentDataColumns = CurrentLine.split(",");
  		for (int j = 0; j < GetTotalColumns(); j++)
  		{
  			CompleteData[i][j] = CurrentDataColumns[j];
  		}
  	}
  	return CompleteData;
  }
  
  public long GetFileSize()
  {
  	return CSVFileObject.length();
  }
  public String GetRowAt(int Index)
  {
  	if (Index>=TotalRows)	return "Invalid";
  	return Arrays.toString(CompleteData[Index]);
  }
  public float[][] GetFloatData()
  {
  	CompleteFloatData = new float[GetTotalRows()][GetTotalColumns()];
  	String CurrentLine = "";
  	String[] CurrentDataColumns = new String[GetTotalColumns()];
  	for (int i = 0; i < AllDataRows.size(); i++)
  	{
  		CurrentLine = AllDataRows.get(i);
  		CurrentDataColumns = CurrentLine.split(",");
  		for (int j = 0; j < GetTotalColumns(); j++)
  		{
  			CompleteFloatData[i][j] = Float.parseFloat(CurrentDataColumns[j]);
  		}
  	}
  	
  	return CompleteFloatData;
  }
	public int GetTotalRows()
	{
		return TotalRows;
	}
	public int GetTotalColumns()
	{
		return TotalColumns;
	}
	public void CloseFile() throws IOException
	{
		this.BufferedFile.close();
	}
	public String GetFilePath()
	{
		return CSVFileObject.getAbsolutePath();
	}
}
