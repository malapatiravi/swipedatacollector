package com.SwipeData.generalobjects;

import java.io.File;
import java.util.Arrays;

import android.os.Environment;
import android.util.Log;

public class TemplateCreator extends FileWriterObject 
{
	private String DataLocation;
	private String GenuineUser;
	private FileWriterObject FWO;
	private String TAG = "TC";
	private String OtherUsers[];
	private int TotalAvailableUsers;
	private int GenuineSampleSize;
	private final String TrainTemplateName = "TrainingTemplate.txt";
	private final File ImposterData = new File(Environment.getExternalStorageDirectory(), "SwipeImposters/Imposters.txt");
	TemplateCreator()
	{
		try {	throw new Exception("Not enough data to create template creator!");} 
		catch (Exception e){ e.printStackTrace();}
	}
	
	public TemplateCreator(String FName, String Location, String GU)
	{
		super(FName, Location+"/Trainer", false);
		DataLocation = Location;
		GenuineUser = GU;
		GenuineSampleSize = 0;
	}
	
	public void GetFolderNames() throws Exception
	{
		File root = new File(Environment.getExternalStorageDirectory(), DataLocation);
		if (root.exists())
		{
			OtherUsers = root.list();
			if (OtherUsers==null) throw new Exception("Current directory does not contain any other directories");
			TotalAvailableUsers = OtherUsers.length;
			Log.i(TAG, Arrays.toString(OtherUsers));
		}
		else
		{
			throw new Exception("Root does not exists: "+root.getAbsoluteFile());
		}
	}
	
	public void CreateTrainingTemplate()
	{
		try
		{
			ExcelReader GenuineFile;
			GenuineFile = new ExcelReader(TrainTemplateName, DataLocation+"/"+GenuineUser, 0, 0);
			GenuineSampleSize = GenuineFile.GetTotalRows();
			for (int i =0; i<GenuineSampleSize; i++) this.SaveData(GenuineFile.GetRowAt(i)+",Genuine");
		} 
		catch (Exception e){e.printStackTrace();}
		int DataPerImposter = (int) Math.ceil(GenuineSampleSize/(TotalAvailableUsers-1));
		for (int i =0; i<TotalAvailableUsers; i++)
		{
			if (OtherUsers[i]!=GenuineUser)
			{
				try
				{
					ExcelReader ImposterFile;
					ImposterFile = new ExcelReader(TrainTemplateName, DataLocation+"/"+OtherUsers[i], 0, 0);
					int SampleSize = ImposterFile.GetTotalRows();
					for (int j =0; j<((SampleSize>DataPerImposter)?DataPerImposter:SampleSize); j++) this.SaveData(ImposterFile.GetRowAt(j)+",Imposter");
				}				
				catch (Exception e){e.printStackTrace();}
			}
		}
		
	}
	
}
