package com.SwipeData.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.SwipeData.authentication.SensorFileManager;
import com.SwipeData.authentication.SensorsTemplateManager;
import com.SwipeData.authentication.WekaClassifierRemoval;
import com.SwipeData.generalobjects.AndroidFileManager;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class SensorsAuthService extends Service
{
	private String TAG="SensorServ";
	private String Username;
	private String DemoFor;
	private String Filename;
	private String Folder;
	private String AuthorizedUser;
	private String TemplateFilename;
	private AndroidFileManager AFMObject;
	private File DataFile;
	private BufferedReader DataBuffer;
	private boolean FileOpened = false;
	//private int FILE_SIZE_THRESHOLD = 4000;
	private int NumberOfRequiredLines=500;
	private boolean KeepLooping = true;
	private long LastFileSize = 0;
	private boolean IsAuthenticated = false;
	private boolean FileLocked = false;
	private String[] Message;
  private File root;// = new File(Environment.getExternalStorageDirectory(), DataFolder);
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.i(TAG, "Service initiated!");
		Message = new String[10];
		Toast.makeText(getApplicationContext(), "Gait authentication started", Toast.LENGTH_SHORT).show();
		speedExceedMessageToActivity();
		
		Username = intent.getExtras().getString("Username");
		DemoFor = intent.getExtras().getString("DemoFor");
		Filename = intent.getExtras().getString("Filename");
		Folder = intent.getExtras().getString("Folder");
		AuthorizedUser = intent.getExtras().getString("AuthorizedUser");
		TemplateFilename = intent.getExtras().getString("TemplateFilename");
		root =  new File(Environment.getExternalStorageDirectory(), Folder);
		AFMObject= new AndroidFileManager(Folder+"/"+DemoFor+"/"+Username,"Testing.txt");
			
		Log.i(TAG, "Username="+Username+", DemoFor="+DemoFor+", Filename="+Filename+", Folder="+Folder+", AuthorizedUser="+AuthorizedUser+", TemplateFilename="+TemplateFilename);
		if ((Username=="") || (DemoFor=="") || (Filename=="") ||(Folder=="") || (AuthorizedUser=="") || (TemplateFilename==""))
		{
			Toast.makeText(getApplicationContext(), "Some value is missing here!", Toast.LENGTH_SHORT).show();					
		}
		else
		{
			GetFeatures();
			//TryAuthentication();
		}
		return startId;
	}
	@Override
	public IBinder onBind(Intent arg0)
	{
		return null;
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		KeepLooping = false;
		try
		{
			DataBuffer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    Toast.makeText(getApplicationContext(), "Sensor authentication stopped", Toast.LENGTH_SHORT).show();
	}
	
	private void speedExceedMessageToActivity() 
	{
    Intent intent = new Intent("AuthenticationBroadcast");
    sendLocationBroadcast(intent);
	}
 
	private void sendLocationBroadcast(Intent intent)
	{
    intent.putExtra("IsSensorAuthenticated", IsAuthenticated);
    intent.putExtra("UpdateMessage", Message[0]);
    intent.putExtra("TotalLines", Message[1]);
    intent.putExtra("FinalScore is", Message[2]);
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
	
//	private void TryAuthentication()
//	{
//		final String TestArff = "Testing.arff";
//		final File TestedRoot =  new File(Environment.getExternalStorageDirectory(), ((Folder+"/"+DemoFor+"/"+Username)));
//		
//		Thread thread = new Thread()
//		{
//			@Override
//			public void run()
//			{
//				File TestingDataFile = new File(TestedRoot, TestArff);
//				//Log.i(TAG, "Starting to try authentiction");
//				while(KeepLooping)
//				{
//					if (TestingDataFile.exists())
//					{
//					//	Log.i(TAG, "Testing data file exists! - "+TestingDataFile.getAbsolutePath().toString());
//						Message[1] = "Classifying...";
//						speedExceedMessageToActivity();
//						try
//						{
//						//	Log.i(TAG, "File lock status: "+FileLocked);
//							if (!FileLocked) 
//							{
//								Classify();  
//								
//					    }
//						} catch (Exception e)
//						{
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					}
//					else
//					{
//						Log.i(TAG, "Testing data file does not exists! - "+TestingDataFile.getAbsolutePath().toString());						
//					}
//				  try{ Thread.sleep(10000);} catch (InterruptedException e)	{	e.printStackTrace();}
//				}
//			}
//		};
//		thread.start();
//	}
//	
	private void GetFeatures()
	{
		final File root =  new File(Environment.getExternalStorageDirectory(), ((Folder+"/"+DemoFor+"/"+Username)));
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				Log.i(TAG, "Starting out the feature extraction");
				DataFile = new File(root, Filename);
				while(KeepLooping)
				{
					if (LastFileSize<DataFile.length())
					{
						Log.i(TAG, "Filename="+Filename+" and folder="+(Folder));
						if (DataFile.exists())
						{
							Log.i(TAG, "File exists and file");
							if (!FileOpened)
							{
								try
								{
									DataBuffer = new BufferedReader(new FileReader(DataFile));
								} catch (FileNotFoundException e){e.printStackTrace();}
								Log.i(TAG, "File has been opened");
								FileOpened=true;
							}
							SensorFileManager TestingDataFile = new SensorFileManager(DataFile,root.getAbsolutePath());
							int NOLInTestFile=0;
							try
							{
								NOLInTestFile=TestingDataFile.getNumberOfTotalSamples();
								Log.i(TAG,+NOLInTestFile+"!!!!!!!! Number Of Lines in the File...........");
							} catch (FileNotFoundException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							Message[1] = ""+NOLInTestFile;
							speedExceedMessageToActivity();
							//Make Template here...
					//		if (DataFile.length()>=FILE_SIZE_THRESHOLD)
							if(NOLInTestFile>NumberOfRequiredLines) //Checking the number of lines
							{
								Log.i(TAG,"Sufficient Data For Classification...");
								//Log.i(TAG, DataFile.length()+" is the size of file  and is enough for classification ");
								try
								{
									FileLocked = true;
									Thread.sleep(10000);
									DoTesting();
									Classify();
									
									
								} catch (Exception e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
//														Log.i(TAG, DataFile.length()+" is the size of file, which is above threshold");
						  }
							else 
								//Log.i(TAG, DataFile.length()+" is the size of file, which is not enough to test");
								Log.i(TAG, NOLInTestFile+" Lines are in file but NOT Enough to test");

						}
						else
						{
							Log.i(TAG, "Filename="+Filename+" and folder="+((Folder+"/"+DemoFor+"/"+Username))+" does not exists yet");
						}
						LastFileSize = DataFile.length();
					}
				//	try{	Thread.sleep(10000); } catch (InterruptedException e)	{	e.printStackTrace();	}
				}				
			}			
		};
		thread.start();
	}
	
	private void Classify() throws Exception
	{
		
		Log.i(TAG, "Inside Classify Function....."); 
	  //Getting the Training and Testing.arff files to pass to Weka Clssifier
		//int FeatureIndexesToBeRemoved[]={4,18,32,46,5,19,33,47,6,20,34,48,7,21,35,49,12,26,40,53,13,27,41,54};
		int FeatureIndexesToBeRemoved[]={4,18,32,46};
	//	int FeatureIndexesToBeRemoved[]={};
    //File TrainingArff = new File(root,"Sensors/"+Username+"/Training.arff");
		//File TestingArff = new File(root,"Sensors/"+Username+"/Testing.arff");
		
//		File TrainingArff = new File(root,"Sensors/"+Username+"/weatherTraining.arff");
//		File TestingArff = new File(root,"Sensors/"+Username+"/weatherTesting.arff");
//		
	  	//Log.i(TAG, "TrainingArff : "+TrainingArff.getAbsolutePath()+" Sending.... to Weka ");
		 //Log.i(TAG, "TestingArff : "+TestingArff.getAbsolutePath()+" Sending.... to Weka ");
		 
		// Calling the Weka Classifier To get the decision.
		WekaClassifierRemoval DecisionTemp = new WekaClassifierRemoval(AFMObject);
	  // Getting the decision from Naive Bayes----
		// We have different options to get the decision-SVM, Logistic Regression, and Random Forest Classifiers
	//	boolean Decision=DecisionTemp.getTheDecisionUsingNaiveBayesForSensors();
		DecisionTemp.setFeatureIndexesToBeRemoved(FeatureIndexesToBeRemoved);
		DecisionTemp.setThreshold(0.80);
		
		//boolean Decision=DecisionTemp.getTheDecisionUsingNaiveBayesForSensors();
		//boolean Decision=DecisionTemp.getTheDecisionUsingLogisticRegressionForSensors();
	//	boolean Decision=DecisionTemp.getTheCombinedDecision();
		boolean Decision=DecisionTemp.getFinalDecision();
   //  Message[2] = ""+DecisionTemp.getClassificationScore();
		Message[2] = ""+Decision;
     
		if(!Decision)
		{
			IsAuthenticated = false;
			Message[0] = "IMPOSTER";
			speedExceedMessageToActivity();
		     Log.i(TAG,"Imposter");
		}
		 else
		 {
				IsAuthenticated = true;
				Message[0] = "GENUINE";
				speedExceedMessageToActivity();
		     Log.i(TAG,"Genuine");
		 }
	}
	
	private void DoTesting() throws Exception
	{
			FileLocked = true;
			Log.i(TAG, "Inside DoTesting-----:");
			SensorsTemplateManager TestingObject= new SensorsTemplateManager(AFMObject,false);//false for TESTING 
			TestingObject.createArffFileForTesting();
     FileLocked = false;
	}

}
