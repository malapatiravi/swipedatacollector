package com.SwipeData.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import com.SwipeData.authentication.SwipeClassifier;
import com.SwipeData.authentication.SwipeFeatureExtractor;
import com.SwipeData.generalobjects.AndroidFileManager;
import com.SwipeData.generalobjects.ArffCreator;
import com.SwipeData.generalobjects.DataSender;
import com.SwipeData.generalobjects.ExcelReader;
import com.SwipeData.generalobjects.FileWriterObject;






import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
public class NAOAuthenticationService extends Service
{
	private String TAG="AService";
	private SwipeFeatureExtractor TestingFE;
	private String Username;
	private String DemoFor;
	private String Filename;
	private String Folder;
	private String AuthorizedUser;
	private String TemplateFilename;
	private File DataFile;
	private BufferedReader DataBuffer;
	private boolean FileOpened = false;
	private int FILE_SIZE_THRESHOLD = 10000;
	private boolean KeepLooping = true;
	private int NumberOfFeatures;
	private long LastFileSize = 0;
	private int Z_Total_Threshold = 13;
	private File TrainingArff;
	private File TestingArff;
	private AndroidFileManager GlobalAFM;
	private boolean TestingFileLocked = true;
	private String ServerURL = "http://10.79.54.131/DEMO/StoreClassificationValue.php";
	private DataSender DS = new DataSender(ServerURL);
	private String MsgToServer = ""; 
	private boolean IsAuthenticated = false;
	private int WindowSize = 20;
  @Override
	public IBinder onBind(Intent arg0) {return null;}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.i(TAG, "Service initiated!");
		Toast.makeText(getApplicationContext(), "NAO authentication started", Toast.LENGTH_SHORT).show();
		speedExceedMessageToActivity();
		Username = intent.getExtras().getString("Username");
		DemoFor = intent.getExtras().getString("DemoFor");
		Filename = intent.getExtras().getString("Filename");
		Folder = intent.getExtras().getString("Folder");
		AuthorizedUser = intent.getExtras().getString("AuthorizedUser");
		GlobalAFM = new AndroidFileManager(Folder+"/"+DemoFor+"/"+Username, "AuthenticationFile.txt");
		
		TemplateFilename = intent.getExtras().getString("TemplateFilename");
		NumberOfFeatures = intent.getExtras().getInt("NumberOfFeatures");
		Log.i(TAG, "Username="+Username+", DemoFor="+DemoFor+", Filename="+Filename+", Folder="+Folder+", AuthorizedUser="+AuthorizedUser+", TemplateFilename="+TemplateFilename);
		if ((Username=="") || (DemoFor=="") || (Filename=="") ||(Folder=="") || (AuthorizedUser=="") || (TemplateFilename==""))
		{
			Toast.makeText(getApplicationContext(), "Some value is missing here!", Toast.LENGTH_SHORT).show();					
		}
		else
		{
			PrepareForClassification();
			GetFeatures();
			TryWekaAuthentication();
			//TryAuthentication();
			//RunDataEvaluation();
			

		}
		try
		{
//			source = new DataSource("/some/where/data.arff");
		} catch (Exception e)
		{
			
			e.printStackTrace();
		}

		
		return startId;
	}

	private void RunDataEvaluation()
	{
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				SplitFiles();
				MakeTrainingTemplates();
				MakeTestingTemplates();
				RunTests();
			}
		};
		thread.start();
	}
	
	private void RunTests()
	{
		String DataLocation = "DataFolder/Separated";
		String[] OtherFolders = GetFolderNames(DataLocation);
		int TotalOtherFolders = OtherFolders.length;
		String TestingTemplate = "TestingTemplate.txt";
		String TrainingTemplate = "TrainingTemplate.txt";
		String FileContent = "";
		FileWriterObject FinalDataOut = new FileWriterObject("FinalOut.txt", DataLocation);
		for (int i=0; i<TotalOtherFolders; i++)
		{
			String TrainerFolder = DataLocation+"/"+OtherFolders[i];
			for (int j=0; j<TotalOtherFolders; j++)
			{
				String TesterFolder = DataLocation+"/"+OtherFolders[j];
				SwipeClassifier IndvClassifier = new SwipeClassifier(TrainerFolder, TrainingTemplate, TesterFolder, TestingTemplate);
				
				int ZScore = IndvClassifier.GetZScore();
				float[] Trio = IndvClassifier.GetTrio();
				FileContent = i+","+j+","+ZScore+","+Trio[0]+","+Trio[1]+","+Trio[2];
				FinalDataOut.SaveData(FileContent);
				Log.i(TAG, "i = "+i+", j = "+j);
			}
		}
		FinalDataOut.CloseWriter();
	}
	private void SplitFiles()
	{
		String DataLocation = "DataFolder/Separated";
		String SwipeFilename = "SwipeFile.txt";
		String TrainingFile = "Training.txt";
		String TestingFile = "Testing.txt";
		String[] OtherFolders = GetFolderNames(DataLocation);
		int TotalOtherFolders = OtherFolders.length;
		ExcelReader EFR = null;
		for (int i=0; i<TotalOtherFolders; i++)
		{
			String CurrentFolder = DataLocation+"/"+OtherFolders[i];

			try
			{
				EFR = new ExcelReader(SwipeFilename, CurrentFolder, 1, 0);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			String[][] FileData = EFR.GetData();
			int TotalLines = FileData.length;
			FileWriterObject CFWO = new FileWriterObject(TrainingFile, CurrentFolder);				
			for (int j = 0; j < TotalLines/2; j++)
			{
				String CurrentLine = "";
				for (int k = 0; k < FileData[j].length; k++) CurrentLine+= ((k==0)?"":",")+FileData[j][k];
				CFWO.SaveData(CurrentLine);
			}
			CFWO.CloseWriter();
			Log.i(TAG,"Training file creation complete = "+i);
			FileWriterObject CFWOTe = new FileWriterObject(TestingFile, CurrentFolder);				
			for (int j =TotalLines/2; j < TotalLines; j++)
			{
				String CurrentLine = "";
				for (int k = 0; k < FileData[j].length; k++) CurrentLine+= ((k==0)?"":",")+FileData[j][k];
				CFWOTe.SaveData(CurrentLine);				
			}
			CFWOTe.CloseWriter();
			Log.i(TAG,"Testing file creation complete = "+i);
		}
	}
	private void MakeTrainingTemplates()
	{
		String DataLocation = "DataFolder/Separated";
		String SwipeFilename = "Training.txt";
		String[] OtherFolders = GetFolderNames(DataLocation);
		int TotalOtherFolders = OtherFolders.length;
		for (int i=0; i<TotalOtherFolders; i++)
		{
			String CurrentFolder = DataLocation+"/"+OtherFolders[i];
			SwipeFeatureExtractor CurrentFE = new SwipeFeatureExtractor(NumberOfFeatures, SwipeFilename, CurrentFolder);
			MakeTemplate(CurrentFE, CurrentFolder, "TrainingTemplate.txt");
			Log.i(TAG, "Finished trainign file with user: "+(i+1));
		}
	}
	
	private void MakeTestingTemplates()
	{
		String DataLocation = "DataFolder/Separated";
		String SwipeFilename = "Testing.txt";
		String[] OtherFolders = GetFolderNames(DataLocation);
		int TotalOtherFolders = OtherFolders.length;
		for (int i=0; i<TotalOtherFolders; i++)
		{
			String CurrentFolder = DataLocation+"/"+OtherFolders[i];
			SwipeFeatureExtractor CurrentFE = new SwipeFeatureExtractor(NumberOfFeatures, SwipeFilename, CurrentFolder);
			MakeTemplate(CurrentFE, CurrentFolder, "TestingTemplate.txt");
			Log.i(TAG, "Finished testing file with user: "+(i+1));
		}
	}
	private void MakeTemplate(SwipeFeatureExtractor CurrentFE, String CurrentFolder, String TemplateFilename)
	{
		//String TemplateFilename = "FeatureFile.txt";
		String FeatureData = "";
		FileWriterObject TrainedTemplate = new FileWriterObject(TemplateFilename, CurrentFolder, false);
			for (int j=0; j<CurrentFE.GetFeatureVar().GetNumberOfStrokes(); j++)
			{
				for (int k=0; k<NumberOfFeatures; k++)
				{
					FeatureData = FeatureData+(k==0?"":",")+CurrentFE.GetFeatureVar().GetValueAt(j, k);
//					Log.i(TAG,"i="+i+", j="+j+", k="+k+", TrainingFE.GetFeatureVar().GetValueAt(i, j, k)="+TrainingFE.GetFeatureVar().GetValueAt(i, j, k));
				}
				TrainedTemplate.SaveData(FeatureData);
				FeatureData="";
			}
		TrainedTemplate.CloseWriter();
		Log.i(TAG,"Finished creating template file");
	}
	
	public String[] GetFolderNames(String DataLocation)
	{
		File root = new File(Environment.getExternalStorageDirectory(), DataLocation);
		String OtherUsers[] = root.list();
		if (root.exists())
		{
			if (OtherUsers==null)
				try
				{
					throw new Exception("Current directory does not contain any other directories");
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			Log.i(TAG, Arrays.toString(OtherUsers));
		}
		else
		{
			try
			{
				throw new Exception("Root does not exists: "+root.getAbsoluteFile());
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return OtherUsers;
	}
	private void PrepareForClassification()
	{
		String TrainingDataDir = GlobalAFM.GetCurrentFolder();
		String TrainingFilename = "TrainingTemplate.txt";
		String DataToSave[] = null;
		try
		{
			ExcelReader GenuineFile = new ExcelReader(TrainingFilename, TrainingDataDir, 0, 0);
			//Log.i(TAG, "Genuine file at: "+GenuineFile.GetFilePath());
			ExcelReader ImposterFile = new ExcelReader("Imposters.txt", "SwipeImposters", 0, 0);
			//Log.i(TAG, "Genuine file at: "+ImposterFile.GetFilePath());
			int GenuineSampleSize = GenuineFile.GetTotalRows();
			int ImposterSampleSize = ImposterFile.GetTotalRows();
			int MaxImposters = (ImposterSampleSize>GenuineSampleSize)?GenuineSampleSize:ImposterSampleSize;
			DataToSave = new String[(MaxImposters*2)];
			for (int i =0; i < DataToSave.length; i++)
				DataToSave[i] = "0.0";
			String[][] GenuineData = NormalizeData(GenuineFile.GetData());
			String[][] ImposterData = ImposterFile.GetData();
			int Counter = 0;
			for (int i =0; i<MaxImposters; i++) 
			{
				String CurrLine = "";
				for (int j = 0; j < GenuineData[i].length; j++) CurrLine += ((j==0)?"":",")+GenuineData[i][j];
				CurrLine = CurrLine+",Genuine";
				//Log.i(TAG, "Genuine i = "+i+", Data: "+CurrLine+" & DataToSave = "+Arrays.toString(DataToSave));
				DataToSave[Counter] = CurrLine;
				Counter++;
			}
			for (int i =0; i<MaxImposters; i++)
			{
				String CurrLine = "";
				for (int j = 0; j < ImposterData[i].length; j++) CurrLine += ((j==0)?"":",")+ImposterData[i][j];
				//Log.i(TAG, "Imposter i = "+i+", Data: "+CurrLine);
				DataToSave[Counter] = CurrLine;
				Counter++;
			}
		} 
		catch (Exception e){e.printStackTrace();}
		ArffCreator TrArffFile = new ArffCreator("Training.arff", TrainingDataDir, false);
		TrArffFile.SetRelation("TrainingFile");
		TrArffFile.SetAttributes(GetFeatureAndTypeRelation());
		TrArffFile.SetData(DataToSave);
		TrArffFile.CloseWriter();
		TrainingArff = new File(GlobalAFM.GetCurrentFolder(), "Training.arff");
		Log.i(TAG, "Prepared for classification");
	}
	
	public void normalizeFeatures(double FeatureMatrix[][]) throws IOException
  {
      int NumberOfRows;
      NumberOfRows=FeatureMatrix.length;
      double TempArray[]= new double[NumberOfRows];
      double Min, Max, Differences;
      int NumberOfFeaturesToBeNormalized=23;
     for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
      {
        for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
        {
          TempArray[VectorItr]=FeatureMatrix[VectorItr][FeatureItr];
        }
          Arrays.sort(TempArray);
          Min=TempArray[0];Max=TempArray[TempArray.length-1];
          Differences=Max-Min;
         
              
        for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
        {
          
            if(Differences==0)
              FeatureMatrix[VectorItr][FeatureItr]= FeatureMatrix[VectorItr][FeatureItr]-Min;
            else 
              FeatureMatrix[VectorItr][FeatureItr]= (FeatureMatrix[VectorItr][FeatureItr]-Min)/Differences;
        }
                    
      }
     
     for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
     {
    	 for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
       {
    		  if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]) || FeatureMatrix[VectorItr][FeatureItr]==0.0)
    		// if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]))
    		  	{
    		  	   FeatureMatrix[VectorItr][FeatureItr]=0.0025;
    		  	   Log.i(TAG, "Found NaN or 0.0 here And replacing with 0.0025 : "+FeatureMatrix[VectorItr][FeatureItr]);
    		  	}
    		  
    		 
       }
     }
   
} 
	private String[][] NormalizeData(String[][] Data)
	{
			try
			{
				if (Data.length <= 0)
					throw new Exception ("There is no data to normalize");
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		int TotalRows = Data.length;
		int TotalColumns = Data[0].length;
		String[][] ReturnVal = new String[TotalRows][TotalColumns];
		for(int i=0; i < TotalColumns; i++)
		{
			float[] CurrentColumn = new float[TotalRows];
			for (int j=0; j<TotalRows; j++)	{	CurrentColumn[j] = Float.parseFloat(Data[j][i]);	}
			float Min = GetMin(CurrentColumn);
			float Max = GetMax(CurrentColumn);
			if (Min == Max) Max++;
			for (int j=0; j<TotalRows; j++)	{	ReturnVal[j][i] = Float.toString((CurrentColumn[j]-Min)/(Max-Min));	}
		}
		return ReturnVal;
	}
	
	private float GetMin(float[] Arr)
	{
		Arrays.sort(Arr);
		return Arr[0];
	}
	private float GetMax(float[] Arr)
	{
		Arrays.sort(Arr);
		return Arr[Arr.length-1];
	}
	private String[][] GetFeatureAndTypeRelation()
	{
		String[][] Attribs  = new String[NumberOfFeatures+1][2];
		Attribs[0][0]  = "BeginX"; 							Attribs[0][1]  = "numeric";
		Attribs[1][0]  = "EndX"; 								Attribs[1][1]  = "numeric";
		Attribs[2][0]  = "BeginY"; 							Attribs[2][1]  = "numeric";
		Attribs[3][0]  = "EndY"; 								Attribs[3][1]  = "numeric";
		Attribs[4][0]  = "Distance"; 						Attribs[4][1]  = "numeric";
		Attribs[5][0]  = "AvgDist";							Attribs[5][1]  = "numeric";
		Attribs[6][0]  = "Angle1"; 							Attribs[6][1]  = "numeric";
		Attribs[7][0]  = "Time"; 								Attribs[7][1]  = "numeric";
		Attribs[8][0]  = "MeanEV"; 							Attribs[8][1]  = "numeric";
		Attribs[9][0]  = "StdDevEV"; 						Attribs[9][1]  = "numeric";
		Attribs[10][0] = "QuantileEV25"; 				Attribs[10][1] = "numeric";
		Attribs[11][0] = "QuantileEV50"; 				Attribs[11][1] = "numeric";
		Attribs[12][0] = "QuantileEV75"; 				Attribs[12][1] = "numeric";
		Attribs[13][0] = "MeanEA"; 							Attribs[13][1] = "numeric";
		Attribs[14][0] = "StdDevEA"; 						Attribs[14][1] = "numeric";
		Attribs[15][0] = "QuantileEA25"; 				Attribs[15][1] = "numeric";
		Attribs[16][0] = "QuantileEA50"; 				Attribs[16][1] = "numeric";
		Attribs[17][0] = "QuantileEA75"; 				Attribs[17][1] = "numeric";
		Attribs[18][0] = "MeanPressure"; 				Attribs[18][1] = "numeric";
		Attribs[19][0] = "PressureStdDev"; 			Attribs[19][1] = "numeric";
		Attribs[20][0] = "PressureQuantile25"; 	Attribs[20][1] = "numeric";
		Attribs[21][0] = "PressureQuantile50"; 	Attribs[21][1] = "numeric";
		Attribs[22][0] = "PressureQuantile75"; 	Attribs[22][1] = "numeric";
		Attribs[23][0] = "class"; 							Attribs[23][1] = "{Genuine,Imposter}";
		return Attribs;
	}
	
	private void TryWekaAuthentication()
	{
		//final SwipeClassifier SCF 				= new SwipeClassifier(TrainingArff, TestingArff);
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				while(KeepLooping)
				{
					if (!TestingFileLocked)
					{
						SwipeClassifier SCF 				= new SwipeClassifier(GlobalAFM);
						boolean IsGenuine = false;
						Log.i(TAG, "Calculating ZScore!");
						if (SCF.TesterFileExists())
						{
							SCF.LoadAFMToWeka();
							double[] AllScores = new double[4];//SCF.GetAllScores();
							
							IsGenuine = SCF.GetTheDecisionUsingLogisticRegression();
							AllScores[0] = SCF.GetScoreFromWeka();
							SCF 				= new SwipeClassifier(GlobalAFM);
							IsGenuine = SCF.GetTheDecisionUsingNaiveBayes();//.GetTheDecisionUsingLogisticRegression();
							AllScores[1] = SCF.GetScoreFromWeka();
							SCF 				= new SwipeClassifier(GlobalAFM);
							IsGenuine = SCF.GetTheDecisionUsingRandomForest();//GetTheDecisionUsingNaiveBayes();//.GetTheDecisionUsingLogisticRegression();
							AllScores[2] = SCF.GetScoreFromWeka();
							SCF 				= new SwipeClassifier(GlobalAFM);
							IsGenuine = SCF.GetTheDecisionUsingSVM();//GetTheDecisionUsingNaiveBayes();//.GetTheDecisionUsingLogisticRegression();
							AllScores[3] = SCF.GetScoreFromWeka();
							Log.i(TAG, "All scores: "+Arrays.toString(AllScores));
							//double Score = SCF.GetWekaClassificationScore();//GetMajority();	
							IsAuthenticated = IsGenuine;
							MsgToServer = DemoFor+":-:"+Arrays.toString(AllScores)+":-:"+(IsAuthenticated?"1":"0");
							DS.SendData(MsgToServer);
							speedExceedMessageToActivity();
							//Log.i(TAG, "Score = "+Score+" and hence "+(IsAuthenticated?" AUTHENTICATED":"REJECTED"));
						}
						else
						{
							Log.i(TAG, "Testing data file does not exists!");
						}
						try	{ Thread.sleep(800);} 
						catch (InterruptedException e){	e.printStackTrace(); }						
					}

				}				
			}
		};

		thread.start();
	}
	private void TryAuthentication()
	{
		final String TesterPath 					= (Folder+"/"+DemoFor+"/"+Username);
		final String TrainerPath 					= (Folder+"/"+DemoFor+"/"+AuthorizedUser);
		final String TesterFilename 			= "TestingTemplate.txt";
		final String TrainerFilename 			= "TrainingTemplate.txt";

		final FileWriterObject SaveAuthData = new FileWriterObject("AuthDataVals.txt", (Folder+"/"+DemoFor+"/"+Username), true);
		final SwipeClassifier SCF 				= new SwipeClassifier(TrainerPath, TrainerFilename, TesterPath, TesterFilename);
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				while(KeepLooping)
				{
					Log.i(TAG, "Calculating ZScore!");
					if (SCF.TesterFileExists())
					{
						float[] Trio = SCF.GetTrio();
						int Score = SCF.GetZScore();//GetMajority();
						SaveAuthData.SaveData(Arrays.toString(Trio));
						
						if (Score>=Z_Total_Threshold)		IsAuthenticated = true;
						else														IsAuthenticated = false;
						speedExceedMessageToActivity();
						Log.i(TAG, "Z-Score = "+Score+" and hence "+(IsAuthenticated?" AUTHENTICATED":"REJECTED"));
					}
					else
					{
						Log.i(TAG, "Testing data file does not exists! - "+TesterPath+"/"+TesterFilename);
					}
					try	{ Thread.sleep(1200);} 
					catch (InterruptedException e){	e.printStackTrace(); }
				}				
			}
		};
		//SaveAuthData.CloseWriter();

		/*		
		 final String TestingTemplateName 	= "TestingTemplate.txt";
		 final File TestedRoot =  new File(Environment.getExternalStorageDirectory(), TesterPath);
		
		Thread thread = new Thread()
		{
			public void run()
			{
				File TestingDataFile = new File(TestedRoot, TestingTemplateName);
				ExcelReader TrainedTemplate;
				ExcelReader TestingTemplate;
				//Log.i(TAG, "Starting to try authentiction");
				while(KeepLooping)
				{
					if (TestingDataFile.exists())
					{
						//Log.i(TAG, "Testing data file exists!! hurray!");
						try
						{
							TrainedTemplate = new ExcelReader(TemplateFilename, (Folder+"/"+DemoFor+"/"+AuthorizedUser), 0, 0);
							TestingTemplate = new ExcelReader(TestingTemplateName, (Folder+"/"+DemoFor+"/"+Username), 0, 0);
							
							
							int TRTRows = TrainedTemplate.GetTotalRows();
							int TRTColumns = TrainedTemplate.GetTotalColumns();
							int TETRows = TestingTemplate.GetTotalRows();
							int TETColumns = TestingTemplate.GetTotalColumns();
							
							float TrainedData[][] = new float[TRTRows][TRTColumns];
							float TestingData[][] = new float[TETRows][TETColumns];

							TrainedData = TrainedTemplate.GetFloatData();
							TestingData = TestingTemplate.GetFloatData();
							float TrMeanVals[] = new float[TRTColumns];
							float TeMeanVals[] = new float[TRTColumns];
							float TrStdDev[] = new float[TRTColumns];
							float TeStdDev[] = new float[TRTColumns];
							float[] Z_Value = new float[TRTColumns];
							
							int Counter = 0;
							for (int i = 0; i<TRTColumns; i++)
							{
								
								TrMeanVals[i] = GetColumnMean(TrainedData, i, TRTRows);
								TeMeanVals[i] = GetColumnMean(TestingData, i, TETRows);
								TrStdDev[i] = GetColumnStdDev(TrainedData, i, TRTRows, TrMeanVals[i]);
								TeStdDev[i] = GetColumnStdDev(TestingData, i, TETRows, TeMeanVals[i]);
								float Sigma1ByN1 = (float) ((Math.pow(TrStdDev[i], 2))/TRTRows);
								float Sigma2ByN2 = (float) ((Math.pow(TeStdDev[i], 2))/TETRows);
								Z_Value[i] = (float) ((TrMeanVals[i]-TeMeanVals[i]) / (Math.pow((Sigma1ByN1+Sigma2ByN2), 0.5)));
								if (Math.abs(Z_Value[i])<Z_THRESHOLD)
									Counter++;
							Log.i(TAG, "Z value at "+i+" = "+Z_Value[i]);
							}
							if (Counter>=Z_Total_Threshold)
								IsAuthenticated = true;
							else
								IsAuthenticated = false;
							speedExceedMessageToActivity();
							Log.i(TAG, "Z-Score = "+Counter+" and hence "+(IsAuthenticated?" AUTHENTICATED":"REJECTED"));
//							Log.i(TAG, "TrStdDev="+Arrays.toString(TrStdDev)+"---------TeStdDev="+Arrays.toString(TeStdDev));
							
							
							
							
//							Log.i(TAG, Arrays.deepToString(TrainedData) + "--------------------- and -----------------"+Arrays.deepToString(TestingData));
						} catch (Exception e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						Log.i(TAG, "Testing data file does not exists! - "+TestingDataFile.getAbsolutePath().toString());						
					}
					try
					{
						Thread.sleep(2000);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		};
		*/
		thread.start();
	}
	
	private void GetFeatures()
	{
		final File root =  new File(Environment.getExternalStorageDirectory(), ((Folder+"/"+DemoFor+"/"+Username)));
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				Log.i(TAG, "Starting out the feature extraction");
				DataFile = new File(root, Filename);
				while(KeepLooping)
				{
					if (LastFileSize<DataFile.length())
					{
	//					Log.i(TAG, "Filename="+Filename+" and folder="+(Folder));
						if (DataFile.exists())
						{
	//						Log.i(TAG, "File exists");
							if (!FileOpened)
							{
								try
								{
									DataBuffer = new BufferedReader(new FileReader(DataFile));
								} catch (FileNotFoundException e){e.printStackTrace();}
	//							Log.i(TAG, "File has been opened");
								FileOpened=true;
							}
							Log.i(TAG, DataFile.length()+" is the size of file");
							if (DataFile.length()>=FILE_SIZE_THRESHOLD)
							{
								TestingFE = new SwipeFeatureExtractor(NumberOfFeatures, Filename, (Folder+"/"+DemoFor+"/"+Username));
								MakeTemplate();
								MakeTestArff();
					
						  }
						}
						else
						{
							Log.i(TAG, "Filename="+Filename+" and folder="+((Folder+"/"+DemoFor+"/"+Username))+" does not exists yet");
						}
						LastFileSize = DataFile.length();
					}

				}				
			}			
		};
		thread.start();
	}

	private void MakeTemplate()
	{
		String TemplateFilename = "TestingTemplate.txt";
		String FeatureData = "";
		FileWriterObject TrainedTemplate = new FileWriterObject(TemplateFilename, (Folder+"/"+DemoFor+"/"+Username), false);
			for (int j=0; j<TestingFE.GetFeatureVar().GetNumberOfStrokes(); j++)
			{
				for (int k=0; k<NumberOfFeatures; k++)
				{
					FeatureData = FeatureData+(k==0?"":",")+TestingFE.GetFeatureVar().GetValueAt(j, k);
//					Log.i(TAG,"i="+i+", j="+j+", k="+k+", TrainingFE.GetFeatureVar().GetValueAt(i, j, k)="+TrainingFE.GetFeatureVar().GetValueAt(i, j, k));
				}
				TrainedTemplate.SaveData(FeatureData);
				FeatureData="";
			}
		TrainedTemplate.CloseWriter();
		Log.i(TAG,"Finished creating template file");
	}

	private void MakeTestArff()
	{
		try
		{
			TestingFileLocked = true;
			ExcelReader TestFeatureFile = new ExcelReader("TestingTemplate.txt", (Folder+"/"+DemoFor+"/"+Username), 0, 0);
			String[][] FeatureData1 = TestFeatureFile.GetData();
			int TotalFeatureData = (FeatureData1.length>WindowSize)?WindowSize:FeatureData1.length;
			String[][] FeatureData = new String[TotalFeatureData][FeatureData1[0].length+1];
			String[][] IntermediateFeatureData  = new String[TotalFeatureData][FeatureData1[0].length+1];
			//FeatureData = FeatureData1;
			IntermediateFeatureData = NormalizeData(FeatureData1);
			int InitialFeatureDataIndex = IntermediateFeatureData.length-TotalFeatureData;
			for (int i = InitialFeatureDataIndex; i < IntermediateFeatureData.length; i++) 
			{
				for (int j = 0; j < IntermediateFeatureData[0].length; j++)
				{
					FeatureData[i-InitialFeatureDataIndex][j] = IntermediateFeatureData[i][j];					
				}
				FeatureData[i-InitialFeatureDataIndex][IntermediateFeatureData[i].length] = "Genuine";					
			}
		//	FinalFeatureData = NormalizeData(FeatureData);
			ArffCreator TeArffFile = new ArffCreator("Testing.arff", (Folder+"/"+DemoFor+"/"+Username), false);
			TeArffFile.SetRelation("TestingFile");
			TeArffFile.SetAttributes(GetFeatureAndTypeRelation());
			TeArffFile.SetData(FeatureData);
			TeArffFile.CloseWriter();
			TestingFileLocked = false;
		} catch (Exception e)
		{

			e.printStackTrace();
		}
		TestingArff = new File(GlobalAFM.GetCurrentFolder(), "Testing.arff");
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		KeepLooping = false;
		try
		{
			DataBuffer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    Toast.makeText(getApplicationContext(), "NAO logging stopped", Toast.LENGTH_SHORT).show();
	}
	private void speedExceedMessageToActivity() 
	{
    Intent intent = new Intent("NAOAS");
    sendLocationBroadcast(intent);
	}
 
	private void sendLocationBroadcast(Intent intent)
	{
	    intent.putExtra("IsAuthenticated", IsAuthenticated);
	    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
}
