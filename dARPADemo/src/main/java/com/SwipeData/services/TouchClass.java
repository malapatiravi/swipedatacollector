package com.SwipeData.services;

import com.SwipeData.generalobjects.PointerLocation;

import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.LinearLayout.LayoutParams;

public class TouchClass extends Service
{
	
	private String TAG = this.getClass().getSimpleName();
	// window manager
	private WindowManager mWindowManager;
	// linear layout will use to detect touch event
	private PointerLocation touchLayout;
	private LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	private String[] DataPassed;
	@Override
	public IBinder onBind(Intent arg0) 
	{
		return null;
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
//		super.onCreate();
		Log.i(TAG, "Demo mode = "+intent.getExtras().getString("DemoMode")+" and Demo for = "+intent.getExtras().getString("DemoFor"));
		DataPassed = new String[10];
		DataPassed[0] = intent.getExtras().getString("DemoMode");
		DataPassed[1] = intent.getExtras().getString("DemoFor");
		DataPassed[2] = intent.getExtras().getString("CurrentURL");
		DataPassed[3] = intent.getExtras().getString("UserID");
		touchLayout = new PointerLocation(this, DataPassed);
		touchLayout.setLayoutParams(lp);
		touchLayout.setBackgroundColor(Color.parseColor("#c0000000"));
		touchLayout.setClickable(true);
		mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
		WindowManager.LayoutParams mParams = new WindowManager.LayoutParams
		(
			android.view.ViewGroup.LayoutParams.MATCH_PARENT, // width is equal to full screen
			android.view.ViewGroup.LayoutParams.MATCH_PARENT, // height is equal to full screen
			WindowManager.LayoutParams.TYPE_SYSTEM_ALERT, // Type Ohone, These are non-application windows providing user interaction with the phone (in particular incoming calls).
			WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, // this window won't ever get key input focus
			PixelFormat.TRANSLUCENT
		);
    mParams.gravity = Gravity.LEFT | Gravity.TOP;
		mWindowManager.addView(touchLayout, mParams);		

		//Log.i(TAG, "add View");
		//Toast.makeText(getApplicationContext(), "Service Started", Toast.LENGTH_SHORT).show();;//, "Service Started!", Toast.LENGTH_LONG).show();
		//Log.i("MyApp", "Finished");
		return START_STICKY;
	}
	@Override
	public void onDestroy() 
	{
		if(mWindowManager != null) 
		{
			if(touchLayout != null) mWindowManager.removeView(touchLayout);
		}
		super.onDestroy();
	}
}