package com.SwipeData.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.SwipeData.authentication.SensorsClassifier;
import com.SwipeData.authentication.SensorsTemplateManager;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class OLDSensorsAuthService extends Service
{
	private String TAG="SensorServ";
	private String Username;
	private String DemoFor;
	private String Filename;
	private String Folder;
	private String AuthorizedUser;
	private String TemplateFilename;
	private File DataFile;
	private BufferedReader DataBuffer;
	private boolean FileOpened = false;
	private int FILE_SIZE_THRESHOLD = 5000;
	private boolean KeepLooping = true;
	private long LastFileSize = 0;
	private boolean IsAuthenticated = false;
	private String[] Message;
  private File root;// = new File(Environment.getExternalStorageDirectory(), DataFolder);
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.i(TAG, "Service initiated!");
		Message = new String[10];
		Toast.makeText(getApplicationContext(), "NAO authentication started", Toast.LENGTH_SHORT).show();
		speedExceedMessageToActivity(Message);
		
		Username = intent.getExtras().getString("Username");
		DemoFor = intent.getExtras().getString("DemoFor");
		Filename = intent.getExtras().getString("Filename");
		Folder = intent.getExtras().getString("Folder");
		AuthorizedUser = intent.getExtras().getString("AuthorizedUser");
		TemplateFilename = intent.getExtras().getString("TemplateFilename");
		root =  new File(Environment.getExternalStorageDirectory(), Folder);
		
		Log.i(TAG, "Username="+Username+", DemoFor="+DemoFor+", Filename="+Filename+", Folder="+Folder+", AuthorizedUser="+AuthorizedUser+", TemplateFilename="+TemplateFilename);
		if ((Username=="") || (DemoFor=="") || (Filename=="") ||(Folder=="") || (AuthorizedUser=="") || (TemplateFilename==""))
		{
			Toast.makeText(getApplicationContext(), "Some value is missing here!", Toast.LENGTH_SHORT).show();					
		}
		else
		{
			GetFeatures();
			TryAuthentication();
		}
		return startId;
	}
	@Override
	public IBinder onBind(Intent arg0)
	{
		return null;
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		KeepLooping = false;
		try
		{
			DataBuffer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    Toast.makeText(getApplicationContext(), "Sensor authentication stopped", Toast.LENGTH_SHORT).show();
	}
	
	private void speedExceedMessageToActivity(String[] Message) 
	{
    Intent intent = new Intent("speedExceeded");
    sendLocationBroadcast(intent, Message);
	}
 
	private void sendLocationBroadcast(Intent intent, String[] Message)
	{
    intent.putExtra("IsSensorAuthenticated", IsAuthenticated);
    intent.putExtra("UpdateMessage", Message[0]);
    intent.putExtra("TotalLines", Message[1]);
    intent.putExtra("FinalScore", Message[2]);
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
	
	private void TryAuthentication()
	{
		final String TestingTemplateName = "TestingTemplate.txt";
		final File TestedRoot =  new File(Environment.getExternalStorageDirectory(), ((Folder+"/"+DemoFor+"/"+Username)));
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				File TestingDataFile = new File(TestedRoot, TestingTemplateName);
				//Log.i(TAG, "Starting to try authentiction");
				while(KeepLooping)
				{
					if (TestingDataFile.exists())
					{
						Log.i(TAG, "Testing data file exists! - "+TestingDataFile.getAbsolutePath().toString());	
//						speedExceedMessageToActivity("Classifying!");
						Classify();
					}
					else
					{
						Log.i(TAG, "Testing data file does not exists! - "+TestingDataFile.getAbsolutePath().toString());						
					}
					try
					{
						Thread.sleep(2000);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		};
		thread.start();
	}
	
	private void GetFeatures()
	{
		final File root =  new File(Environment.getExternalStorageDirectory(), ((Folder+"/"+DemoFor+"/"+Username)));
		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				Log.i(TAG, "Starting out the feature extraction");
				DataFile = new File(root, Filename);
				while(KeepLooping)
				{
					if (LastFileSize<DataFile.length())
					{
						Log.i(TAG, "Filename="+Filename+" and folder="+(Folder));
						if (DataFile.exists())
						{
							Log.i(TAG, "File exists");
							if (!FileOpened)
							{
								try
								{
									DataBuffer = new BufferedReader(new FileReader(DataFile));
								} catch (FileNotFoundException e){e.printStackTrace();}
								Log.i(TAG, "File has been opened");
								FileOpened=true;
							}
							Log.i(TAG, DataFile.length()+" is the size of file");
							Message[1] = ""+DataFile.length();
							speedExceedMessageToActivity(Message);
							//Make Template here...
							if (DataFile.length()>=FILE_SIZE_THRESHOLD)
							{
								DoTraining();
//														Log.i(TAG, DataFile.length()+" is the size of file, which is above threshold");
						  }
						}
						else
						{
							Log.i(TAG, "Filename="+Filename+" and folder="+((Folder+"/"+DemoFor+"/"+Username))+" does not exists yet");
						}
						LastFileSize = DataFile.length();
					}

				}				
			}			
		};
		thread.start();
	}
	
	private void Classify()
	{
  	SensorsClassifier DecisionTemp = new SensorsClassifier(root.getAbsolutePath()+"/Sensors/"+Username+"/TestingTemplate.txt",root.getAbsolutePath()+"/Sensors/"+AuthorizedUser+"/TrainingTemplate.txt");
    //DecisionObjects.add(DecisionTemp);
     try
		{
			Message[2] = ""+DecisionTemp.GetFinalScore();
			if(!DecisionTemp.AuthenticationDecision())
			{
				IsAuthenticated = false;
				Message[0] = "CLASSIFIED AS IMPOSTER";
				speedExceedMessageToActivity(Message);
			     Log.i(TAG,"Imposter");
			}
			 else
			 {
					IsAuthenticated = true;
					Message[0] = "Genuine";
					speedExceedMessageToActivity(Message);
			     Log.i(TAG,"Genuine");
			 }
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	private void DoTraining()
	{
		ArrayList<SensorsTemplateManager> Users = new ArrayList<SensorsTemplateManager>();
    //ArrayList<Classifier> DecisionObjects = new ArrayList<>();
    root =  new File(Environment.getExternalStorageDirectory(), Folder);
    Log.i(TAG, "Current Path is: "+root.getAbsolutePath()+"-/-Sensors-/-"+Username+"-/Testing.txt");
	//	SensorsTemplateManager Temp= new SensorsTemplateManager(root.getAbsolutePath()+"/Sensors/"+Username+"/Testing.txt", "Sensors/"+Username);
	//	Users.add(Temp);
	//	try
//		{
	//		Users.get(0).createTemplate(Folder+"/Sensors/"+Username+"/TestingTemplate.txt", false);
	//	} catch (FileNotFoundException e)
//		{
//			e.printStackTrace();
//		} catch (IOException e)
//		{
//			e.printStackTrace();
//		}
	}

}
