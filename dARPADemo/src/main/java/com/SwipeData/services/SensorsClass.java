package com.SwipeData.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.SwipeData.generalobjects.DataSender;
import com.SwipeData.generalobjects.FileWriterObject;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

@SuppressLint({ "InlinedApi", "NewApi" })
public class SensorsClass extends Service implements SensorEventListener
{
	private SensorManager GlobalSensorManager;
  private Sensor LinearAccS;
  private Sensor RotationVectorS;
  private String CompleteData = "";
  private int CurrentStatus = 0;
  float[][] RMatrix = new float[3][3];
  public String LastData = "";
  private int DataCounter = 0;
  private int MAX_DATA_PACK = 20;
  private DataSender DataTransporter;// = new DataSender(ServerURL);
  private String CurrentURL = "";
  private String CurrentFor = "";
  private String CurrentMode = "";
  private String Folder = "";
  private String UserID = "";
  private FileWriterObject StoreObject;
  private String[] Messages;
  private int TotalLinesSaved = 0;
	private SimpleDateFormat CustomTimeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss-SSS");
	//private String Header = "AccX,AccY,AccZ,RVectorX,RVectorY,RVectorZ,LAccX,LAccY,LAccZ,CorrectedX,CorrectedY,CorrectedZ,GyX,GyY,GyZ,Time";
	//private Sensor AccelerometerS;
	//private Sensor GyroscopeS;
  //private boolean Notified = false;
  @Override
	public IBinder onBind(Intent arg0) {return null;}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		CurrentMode = intent.getExtras().getString("DemoMode");
		CurrentFor = intent.getExtras().getString("DemoFor");
		CurrentURL = intent.getExtras().getString("CurrentURL");
		Folder = intent.getExtras().getString("Folder");
		UserID = intent.getExtras().getString("UserID");
		DataTransporter = new DataSender(CurrentURL);
		Messages = new String[10];
		GlobalSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		LinearAccS = GlobalSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		RotationVectorS = GlobalSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
		GlobalSensorManager.registerListener(GlobalSensorListener, LinearAccS, 30000);// SensorManager.SENSOR_DELAY_FASTEST);
		GlobalSensorManager.registerListener(GlobalSensorListener, RotationVectorS, 30000);// SensorManager.SENSOR_DELAY_FASTEST);
		//AccelerometerS = GlobalSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		//GyroscopeS = GlobalSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		//GlobalSensorManager.registerListener(GlobalSensorListener, AccelerometerS , SensorManager.SENSOR_DELAY_NORMAL);
		//GlobalSensorManager.registerListener(GlobalSensorListener, GyroscopeS , SensorManager.SENSOR_DELAY_NORMAL);
		Log.i("Browser: ", "Service initiated!");
		StoreObject = new FileWriterObject(CurrentMode+".txt", Folder+"/"+CurrentFor+"/"+UserID);
		Toast.makeText(getApplicationContext(), "Sensor logging started", Toast.LENGTH_SHORT).show();
		return START_STICKY;
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		GlobalSensorManager.unregisterListener(GlobalSensorListener);
    Toast.makeText(getApplicationContext(), "Sensor logging stopped", Toast.LENGTH_SHORT).show();
	}
	private void speedExceedMessageToActivity(String[] Message) 
	{
    	Intent intent = new Intent("LoggerMessages");
    	sendLocationBroadcast(intent, Message);
	}
 
	private void sendLocationBroadcast(Intent intent, String[] Message)
	{
    intent.putExtra("TotalTrainerLines", Message[0]);
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
	
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {}
	private SensorEventListener GlobalSensorListener = new SensorEventListener()
	{
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {}
		@Override
		public void onSensorChanged(SensorEvent event) 
		{
			Sensor TakenSensor = event.sensor;
//			String TempData = event.values[0]+","+event.values[1]+","+event.values[2]+",";
			if (TakenSensor.getType()==Sensor.TYPE_ROTATION_VECTOR && CurrentStatus==0)
			{
				CurrentStatus = 1;
				float[] RVector = new float[3];
				RVector[0] = event.values[0];
				RVector[1] = event.values[1];
				RVector[2] = event.values[2];
				float[] TMatrix = new float[9];
				SensorManager.getRotationMatrixFromVector(TMatrix, RVector);
				try { RMatrix = ConvertTo2D(TMatrix, 9, 3, 3);} 
				catch (Exception e) {e.printStackTrace();}
			}
			else if (TakenSensor.getType()==Sensor.TYPE_LINEAR_ACCELERATION && CurrentStatus==1)
			{
				CurrentStatus = 0;
				float[] LVector = new float[3];
				LVector[0] = event.values[0];
				LVector[1] = event.values[1];
				LVector[2] = event.values[2];
				float[] CorrectedV = new float[3];
				try { CorrectedV = RotateVectorByMatrix(RMatrix, 3, 3, LVector, 3); } 
				catch (Exception e) {e.printStackTrace();}
				
				String TempData2 = CorrectedV[0]+","+CorrectedV[1]+","+CorrectedV[2];
				
				CompleteData=CompleteData+"::"+TempData2+","+CustomTimeStamp.format(new Date());
        DataCounter++;
        if (DataCounter>MAX_DATA_PACK)
        {
//        	Log.i("SendingData:",CompleteData);
        	DataCounter=0;
          DataTransporter.SendData(CompleteData);        	
          CompleteData = CurrentMode+":-:"+CurrentFor+":-:"+UserID+":-:";
        }
//				CompleteData+=","+GetScreenStatus();
				StoreObject.SaveData(TempData2);
				TotalLinesSaved++;
				if ((TotalLinesSaved%20)==0)
				{
					Messages[0] = TotalLinesSaved+"";
					speedExceedMessageToActivity(Messages);					
				}
			}
/*	Uncomment this for complete log
 **********************************************************************************************	
  		if (TakenSensor.getType()==Sensor.TYPE_ACCELEROMETER && CurrentStatus==0)
			{
				CompleteData+=TempData;
				CurrentStatus = 1;
			}
			else if (TakenSensor.getType()==Sensor.TYPE_ROTATION_VECTOR && CurrentStatus==1)
			{
				CompleteData+=TempData;
				CurrentStatus = 2;
				float[] RVector = new float[3];
				RVector[0] = event.values[0];
				RVector[1] = event.values[1];
				RVector[2] = event.values[2];
				float[] TMatrix = new float[9];
				SensorManager.getRotationMatrixFromVector(TMatrix, RVector);
				try { RMatrix = ConvertTo2D(TMatrix, 9, 3, 3);} 
				catch (Exception e) {e.printStackTrace();}
			}
			else if (TakenSensor.getType()==Sensor.TYPE_LINEAR_ACCELERATION && CurrentStatus==2)
			{
				float[] LVector = new float[3];
				LVector[0] = event.values[0];
				LVector[1] = event.values[1];
				LVector[2] = event.values[2];
				float[] CorrectedV = new float[3];
				try { CorrectedV = RotateVectorByMatrix(RMatrix, 3, 3, LVector, 3); } 
				catch (Exception e) {e.printStackTrace();}
				
				String TempData2 = CorrectedV[0]+","+CorrectedV[1]+","+CorrectedV[2]+",";
				Log.i("Data: ", TempData+" ===and=== "+TempData2);
				CompleteData+=TempData+TempData2;
				CurrentStatus = 3;
			}
			else if (TakenSensor.getType()==Sensor.TYPE_GYROSCOPE && CurrentStatus==3)
			{
				CompleteData+=TempData;
				CurrentStatus = 0;
				StoreObject.SaveData(CompleteData);
				CompleteData = "";
			}
*****************************************************************************************************/
		}
	};
	
	int GetScreenStatus()
	{
		int ReturnVal = 0;
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		boolean isScreenOn = pm.isScreenOn();
		if((isScreenOn))
		{
			ReturnVal = 1;
		}
		else
		{
			ReturnVal = 0;
		}
		return ReturnVal;
		
	}
	float[][] ConvertTo2D(float[] Arr, int ArrSize, int Cols, int Rows) throws Exception
	{
		float[][] ReturnVal = new float[Cols][Rows];
		if (Cols*Rows!=ArrSize)
			throw new Exception("Invalid Operation: Total Array size != Cols*Rows");
		for (int i = 0; i < Rows; i++)
		{
			for (int j = 0; j < Cols; j++)
			{
				ReturnVal[i][j] = Arr[(i*Cols)+j];
			}
		}
		return ReturnVal;
	}
	
	float[] RotateVectorByMatrix(float[][] RotationMatrix, int RMatrixCols, int RMatrixRows, float[] RVector, int RVectorSize) throws Exception
	{
		float[] ReturnVector = new float[RVectorSize];
		if (RMatrixCols!=RVectorSize)
			throw new Exception("Invalid Matrix rotation operation: Vector Size != Matrix Column");
		for (int i = 0; i < RMatrixRows; i++)
		{
			ReturnVector[i] = 0;
			for (int j = 0; j < RMatrixCols; j++)
			{
				ReturnVector[i] += RotationMatrix[i][j]*RVector[j];
			}
		}
		return ReturnVector;
	}
	@Override
	public void onSensorChanged(SensorEvent event) {}

}